import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Usuario } from '../models/usuario';
import { handleError, extractData, getHeaders } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class UsuariosService {
    private API_URI = environment.URI;

    constructor(private http: HttpClient, public router: Router) {}

    /**
     * Funcion encargada de obtener todos los usuarios desde el Servidor.
     */
    getAllUsuarios(): Observable<Usuario[]> {
        let URL = this.API_URI + 'usuarios';

        return this.http.get<Usuario[]>(URL, { headers: getHeaders() })
        //   .map(extractData)
        //   .catch(handleError);
    }
    /**
     * Agrega un usuario enviandolo al Servidor.
     * @param usuario
     */
    addUsuario(usuario: Usuario): Observable<any> {
        let URL = this.API_URI + 'usuarios';

        let body = {
            username: usuario.username,
            nombre: usuario.nombre,
            apellidos: usuario.apellidos,
            email: usuario.email,
            id_rol: usuario.id_rol,
            password: usuario.password,
            eliminado: 0,
            activo: usuario.activo,
            id_empresa: usuario.id_empresa
        };
        
        return this.http.post(URL, JSON.stringify(body), { headers: getHeaders() });
    }

    /**
     * Actualiza un usuario enviandolo al servidor.
     * @param usuario
     */
    updateUsuario(usuario: Usuario): Observable<any> {
        let URL = this.API_URI + 'usuarios/' + usuario.id;

        let body = {
            username: usuario.username,
            nombre: usuario.nombre,
            apellidos: usuario.apellidos,
            email: usuario.email,
            id_rol: usuario.id_rol,
            activo: usuario.activo,
            id_empresa: usuario.id_empresa
        }
        return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() });
        // .map(extractData)
        // .catch(handleError);
    }

    /**
     * Elimina un usuario dado su Id.
     * @param id Usuario
     */
    deleteUsuario(id: Number): Observable<Response> {
        let URL = this.API_URI + 'usuarios/' + id;

        return this.http.delete(URL, { headers: getHeaders() })
        // .map(extractData)
        .catch(handleError);
    }
}
