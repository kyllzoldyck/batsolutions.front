import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { UsuariosComponent } from "./usuarios.component";
import { SharedModule } from '../../shared/shared.module';
import { UsuariosRoutingModule } from "./usuarios-routing.module";
import { UsuariosService } from "../../services/usuarios.service";
import { RolesService } from "../../services/roles.service";
import { EmpresaService } from "../../services/empresas.service";
import { MatPaginatorModule, MatTableModule, MatProgressSpinnerModule, MatPaginatorIntl } from "@angular/material";
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule,
        UsuariosRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule
    ],
    declarations: [ 
        UsuariosComponent 
    ],
    providers: [ 
        UsuariosService, 
        RolesService, 
        EmpresaService,
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class UsuariosModule {}