import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FacturacionComponent } from "./facturacion.component";
import { FacturaCompraComponent } from "./facturaCompra/factura-compra.component";
import { FacturaVentaComponent } from "./facturaVenta/factura-venta.component";
import { NotaCreditoComponent } from "./notaCredito/nota-credito.component";

const routes: Routes = [
    {
        path: '',
        component: FacturacionComponent,
        data: {
            breadcrumb: 'Facturación',
            icon: '',
            status: false
        },
        children: [
            {
                path: '',
                redirectTo: 'compra',
                pathMatch: 'full'
            },
            {
                path: 'compra',
                component: FacturaCompraComponent,
                data: {
                    breadcrumb: 'Factura Compra',
                    icon: '',
                    status: false
                },
            },
            {
                path: 'venta',
                component: FacturaVentaComponent,
                data: {
                    breadcrumb: 'Factura Venta',
                    icon: '',
                    status: false
                },
            },
            {
                path: 'nota-credito',
                component: NotaCreditoComponent,
                data: {
                    breadcrumb: 'Nota(s) de Crédito',
                    icon: '',
                    status: false
                },
            }
        ],
    }
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class FacturacionRoutingModule { }
