import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FacturacionRoutingModule } from "./facturacion-routing.module";
import { FacturacionComponent } from "./facturacion.component";
import { FacturasService } from "../../services/facturas.service";
import { FacturaCompraComponent } from "./facturaCompra/factura-compra.component";
import { FacturaVentaComponent } from "./facturaVenta/factura-venta.component";
import { NotaCreditoComponent } from "./notaCredito/nota-credito.component";
import { EmpresaService } from "../../services/empresas.service";
import { ProveedoresService } from "../../services/proveedores.service";
import { ClientesService } from "../../services/clientes.service";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { ModalArticulosComponent } from "./shared/modal-articulos/modal-articulos.component";
import { ArticulosService } from "../../services/articulos.service";
import { ImpuestosService } from "../../services/impuestos.services";
import { DecimalsPipe } from "../../shared/pipes/decimals.pipe";
import { NotasDeCreditoService } from "../../services/notaCredito.service";
import { ThousandsSeparatorPipe } from "../../shared/pipes/thousands.separator.pipe";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InputUnidadesComponent } from "./shared/input-unidades/input-unidades.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatTableModule, MatButtonModule, MatCheckboxModule, MatPaginatorModule, MatPaginatorIntl, MatProgressSpinnerModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";
import { ThousandsPipeModule } from "../../shared/pipes/thousands.module";
import { DecimalsPipeModule } from "../../shared/pipes/decimals.module";
import { ModalArticulosModule } from "./shared/modal-articulos/modal-articulos.module";

@NgModule({
    imports: [ 
        CommonModule, 
        ReactiveFormsModule,
        SharedModule,
        FacturacionRoutingModule,
        NgbModule,
        MatTableModule,
        MatButtonModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        ThousandsPipeModule,
        DecimalsPipeModule,
        ModalArticulosModule
    ],
    declarations: [ 
        FacturacionComponent,
        FacturaCompraComponent,
        FacturaVentaComponent,
        NotaCreditoComponent
    ],
    providers: [ 
        FacturasService,
        EmpresaService,
        ProveedoresService,
        ClientesService,
        ArticulosService,
        ImpuestosService,
        NotasDeCreditoService,
        DecimalsPipe,
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class FacturacionModule { }