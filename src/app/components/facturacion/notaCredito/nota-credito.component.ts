import { Component, ViewChild, AfterViewInit, OnInit, ElementRef } from '@angular/core';
import { NotaDeCredito } from "../../../models/notaCredito";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotasDeCreditoService } from "../../../services/notaCredito.service";
import { FacturasService } from "../../../services/facturas.service";
import { ClientesService } from "../../../services/clientes.service";
import { ProveedoresService } from "../../../services/proveedores.service";
import { TipoFactura } from '../../../shared/enum/TipoFactura';
import { DatePipe } from '@angular/common';
import { Promise } from 'q';
import { getErrorsForm } from '../../../shared/utils';
import { AuthService } from '../../../services/auth.service';
import { Usuario } from '../../../models/usuario';
import { EmpresaService } from '../../../services/empresas.service';
import { Empresa } from '../../../models/empresa';
import { Router } from '@angular/router';
import swal from "sweetalert2";
import { MatTable, MatPaginator, MatTableDataSource } from '@angular/material';
import { ArticulosService } from '../../../services/articulos.service';

@Component({
  moduleId: module.id,
  providers: [DatePipe],
  selector: 'nota-credito',
  templateUrl: 'nota-credito.html'
})

export class NotaCreditoComponent implements OnInit, AfterViewInit {

  notaDeCredito = new NotaDeCredito();
  showFormState: boolean = false;
  isUpdate: boolean = false;
  isLoading: boolean = true;
  isLoadingSearch: boolean = true;
  txtBuscar: string;
  facturasEncontradas = [];
  facturasAnuladas = [];
  clientes = [];
  proveedores = [];
  notasEncontradasPorNumero = [];
  txtFactura: number;
  clienteName: string;
  proveedorName: string;
  usuario: Usuario;
  showNuevaNota = false;
  empresas: Empresa[];

  @ViewChild('buttonSubmitUpdate') buttonSubmitUpdate: ElementRef;
  @ViewChild('buttonSubmit') buttonSubmit: ElementRef;
  @ViewChild('paginatorNotaCredito') paginatorNotaCredito: MatPaginator;

  columnsNotasCredito = ['numero', 'fecha', 'descripcion', 'empresa', 'factura', 'acciones'];
  dataSourceNotasCredito = new MatTableDataSource();

  constructor(
    private articuloService: ArticulosService,
    private empresaService: EmpresaService,
    private authService: AuthService,
    private notasCreditoService: NotasDeCreditoService,
    private fb: FormBuilder,
    private facturasService: FacturasService,
    private clientesService: ClientesService,
    private proveedoresService: ProveedoresService,
    private datePipe: DatePipe,
    public router: Router
  ) {
    this.usuario = new Usuario();
    this.empresas = [];
  }

  ngOnInit() {
    this.getEmpresas();
    this.getAuthenticatedUser();
    this.LoadAllFacturas();
    this.LoadAllProveedores();
    this.LoadAllClientes();
    this.buildForm();
    this.showAll();

    this.dataSourceNotasCredito.filterPredicate = (data: NotaDeCredito, filter) => {
      const dataStr = data.numero + data.factura.empresa.razon_social.toLowerCase() + data.factura.razon_social.toLowerCase();
      return dataStr.indexOf(filter) != -1;
    };
  }

  ngAfterViewInit() {
    this.dataSourceNotasCredito.paginator = this.paginatorNotaCredito;
  }

  notasForm: FormGroup;

  formErrors = {
    'numero': '',
    'fecha': '',
    'descripcion': '',
    'numeroFactura': ''
  };

  validationMessages = {
    'numero': {
      'required': 'Debe ingresar el Número de Factura.',
    },
    'fecha': {
      'required': 'Debe ingresar una Fecha.',
    },
    'descripcion': {
      'required': 'Debe ingresar una Descripción.',
    },
    'numeroFactura': {
      'required': 'Debe ingresar el número de factura.',
      'min': 'Debe ingresar un número de factura mayor a cero.'
    }
  };
  /**
   * Construye el formulario y sus validaciones
   */
  buildForm() {
    this.notasForm = this.fb.group({
      numero: [this.notaDeCredito.numero, Validators.compose([
        Validators.required,
      ])],
      fecha: [this.notaDeCredito.fecha, Validators.compose([
        Validators.required,
      ])],
      descripcion: [this.notaDeCredito.descripcion, Validators.compose([
        Validators.required,
      ])],
      numeroFactura: [this.txtFactura, Validators.compose([
        Validators.required,
        Validators.min(1)
      ])],
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.notasForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.notasForm) {
      return;
    }

    const form = this.notasForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.usuario = data;
        if (this.usuario.id_rol != 3) {
          // this.settings.actions.edit = true;
          // this.settings.actions.delete = true;
          this.showNuevaNota = true;
        }
        if (this.usuario.id_rol == 1) {
          // this.settings.columns.id_empresa;
        } else {
          // delete this.settings.columns.id_empresa;
        }

      }, err => {
        localStorage.removeItem('bat');
        location.reload();
      });
  }

  searchNotaByNumber() {
    this.notasCreditoService.getNotasByNumero(+this.txtBuscar).subscribe(
      res => {
        console.log('searchNotaByNumber', res);
        this.notasEncontradasPorNumero = res;
        if (this.notasEncontradasPorNumero.length > 0) {
          this.notasEncontradasPorNumero.forEach(nc => {
            let factura = this.facturasAnuladas.find(f => f.id == nc.id_factura);
            let nombreClienteProveedor;

            let cliente = this.clientes.find(c => c.rut == factura.rut);
            if (cliente != undefined) {
              nombreClienteProveedor = cliente.razon_social;
            }
            let proveedor = this.proveedores.find(p => p.rut == factura.rut);
            if (proveedor != undefined) {
              nombreClienteProveedor = proveedor.razon_social;
            }

            nc.tipoFactura = factura.tipo_factura == TipoFactura.Venta ? 'Venta' : 'Compra';
            nc.nombre = nombreClienteProveedor;
            nc.numFactura = factura.numero;
          });
        } else {
          swal({
            title: '¡Notas no encontradas!',
            text: `No existen NC con el número ${this.txtBuscar}.`,
            type: 'info'
          });
        }
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        });
      }
    )
  }

  searchNotaById(factura) {
    this.notasCreditoService.getNotasById(factura.id).subscribe(
      res => {
        this.fillForm(res);
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          text: err.message,
          type: 'error'
        })
      }
    )
  }
  /**
   * Se encarga de pasar una nota al metodo que completa el formulario.
   * @param nota
   */
  selectNotaToView(nota) {
    this.fillForm(nota);
    this.notasEncontradasPorNumero = [];
  }

  fillForm(nota) {
    this.showFormState = true;
    this.isUpdate = true;
    this.isLoadingSearch = true;
    this.txtFactura = 1; //Solo para efectos de validar el formulario;
    this.notaDeCredito.descripcion = nota.descripcion;
    this.notaDeCredito.fecha = nota.fecha.split(" ")[0];
    console.log(this.notaDeCredito.fecha);

    this.notaDeCredito.numero = nota.numero;
    this.notaDeCredito.id = nota.id;
  }

  closeSearchBox() {
    this.notasEncontradasPorNumero = [];
  }
  /**
  * Reinicia el fomulario y limpia
  */
  cancel() {
    this.notasForm.reset();
    this.txtBuscar = "";
    this.txtFactura = +"";
    this.showFormState = true;
    this.isUpdate = false;
    this.facturasEncontradas = [];
    this.notaDeCredito = new NotaDeCredito();
  }

  cancelFromButton() {
    this.notasForm.reset();
    this.txtBuscar = "";
    this.txtFactura = +"";
    this.showFormState = false;
    this.isUpdate = false;
    this.facturasEncontradas = [];
    this.notaDeCredito = new NotaDeCredito();
    this.showAll();
  }

  showAll() {
    this.txtBuscar = "";
    this.txtFactura = +"";
    this.showFormState = false;
    this.isLoading = true;
    this.LoadAllNotasDeCredito();
  }
  /**
   * Retorna la cantidad de Notas
  */
  getCountNotas(): number {
    return this.dataSourceNotasCredito.data.length;
  }

  getCountFactura(): number {
    return this.facturasEncontradas.length;
  }
  /**
   * Carga la grilla de facturas.
   */
  LoadAllNotasDeCredito() {
    this.notasCreditoService.getAllNotas().subscribe(
      data => {
        setTimeout(() => {
          this.dataSourceNotasCredito.data = data;
          this.isLoading = false;
        }, 500);
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        }).then(() => {
          this.cancel()
        })
      }
    )
  }

  LoadAllFacturasByNumber() {
    this.facturasEncontradas = [];
    this.isLoadingSearch = true;
    this.facturasService.getAllFacturasByNumero(this.txtFactura).subscribe(
      data => {
        if (Object.keys(data).length > 0) {
          data.forEach(fact => {
            // Venta
            if (fact.tipo_factura == 1) {
              fact.razon_social = this.getInfoCliente(fact.rut);
            } else if (fact.tipo_factura == 2) {
              fact.razon_social = this.getInfoProveedor(fact.rut);
            }
            this.facturasEncontradas.push(fact);
          })
        } else {
          swal({
            title: '¡Facturas no encontradas!',
            text: `No existen facturas con el número ${this.txtFactura}.`,
            type: 'info'
          })
        }
        setTimeout(() => this.isLoadingSearch = false, 500)
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        })
      },
    )
  }

  LoadAllFacturas() {
    this.facturasService.getAllFacturas().subscribe(
      data => {
        this.facturasAnuladas = data;
      },
      err => {
        console.log('error LoadAllFacturas', err);
      }
    )
  }
  //TODO: Mejorar la obtencion de clientes, para que sea por id!
  LoadAllClientes() {
    this.clientesService.getAllClientes().subscribe(
      data => {
        this.clientes = data;
      },
      err => {
        console.log('error LoadAllClientes', err);
      }
    )
  }
  //TODO: Mejorar la obtencion de proveedores, para que sea por id!
  LoadAllProveedores() {
    this.proveedoresService.getAllProveedores().subscribe(
      data => {
        this.proveedores = data;
      },
      err => {
        console.log('error LoadAllProveedores', err);

      }
    )
  }

  getInfoCliente(rut) {
    var cliente = this.clientes.find(c => c.rut == rut);
    return cliente ? cliente.razon_social : 'No Encontrado';
  }

  getInfoProveedor(rut) {
    var proveedor = this.proveedores.find(p => p.rut == rut);
    return proveedor ? proveedor.razon_social : 'No Encontrado';
  }

  sendAnularFactura(e, idFactura) {
    if (e.target.checked) {
      this.notaDeCredito.id_factura = idFactura;
    }
  }

  onSubmit() {
    this.buttonSubmit.nativeElement.disabled = true;

    this.notasCreditoService.addNotas(this.notaDeCredito).subscribe(
      res => {
        this.buttonSubmit.nativeElement.disabled = false;

        swal({
          title: 'Nota de Crédito Creada!',
          text: 'La Nota de Crédito ha sido creada correctamente',
          type: 'success'
        }).then(() => this.cancelFromButton());
      },
      err => {
        this.buttonSubmit.nativeElement.disabled = false;

        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        })
      }
    )
  }

  deleteNotaCredito(event) {
    swal({
      title: `¿Estás seguro?`,
      text: `Se eliminará la Nota de Crédito N°: ${event.numero}.
            La factura que fue anulada por esta Nota de Crédito seguirá estando nula.`,
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar'
    })
      .then(result => {
        if (result.value)
        {
          this.notasCreditoService.deleteNotaById(event.id).subscribe(
            res => {
              swal(
                'Nota de Crédito Eliminada!',
                'La Nota de Crédito ha sido eliminado correctamente!',
                'success'
              ).then(() => {
                this.isLoading = true;
                this.LoadAllNotasDeCredito()
              });
            },
            err => {
              swal({
                title: 'Ups! Tenemos un problema',
                html: getErrorsForm(err.error.errors),
                type: 'error'
              });
            }
          );
        }
      })
  }

  /**
  * Actualiza una nota de credito
  * @param event
  */
  onUpdate(event) {
    swal({
      title: `Se actualizará la Nota de Crédito N°` + this.notaDeCredito.numero,
      text: "¿Estás seguro de actualizar?",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar'
    }).then(result => {
      if (result.value)
      {
        this.buttonSubmitUpdate.nativeElement.disabled = true;

        this.notasCreditoService.updateNota(this.notaDeCredito).subscribe(
          res => {
            this.buttonSubmitUpdate.nativeElement.disabled = false;

            swal(
              '¡Nota de Crédito Actualizada!',
              'La Nota de Crédito ha sido actualizado correctamente!',
              'success'
            ).then(() => this.cancelFromButton());
          },
          err => {
            this.buttonSubmitUpdate.nativeElement.disabled = false;

            swal({
              title: 'Ups! Tenemos un problema',
              text: err.error || getErrorsForm(err.errors),
              type: 'error'
            });
          }
        );
      }
    });
  }

  getEmpresas() {
    this.empresaService.getAllEmpresas().subscribe(data => {
      this.empresas = data;
      console.log('empresas', data);
    }, error => {
      swal(
        'Ups! Tenemos un problema.',
        'Lo sentimos, no se han podido cargar las empresas',
        'error'
      ).then(() => this.router.navigate(['/home']));
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSourceNotasCredito.filter = filterValue;
  }


}
