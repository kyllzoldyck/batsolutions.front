import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ArticulosComponent } from "./articulos.component";

const routes: Routes = [
    {
        path: '',
        component: ArticulosComponent,
        data: {
            breadcrumb: 'Artículos',
            icon: '',
            status: false
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ArticuloRoutingModule { }