import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Rol } from '../models/rol';
import { handleError, extractData, getHeaders } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class RolesService {
  private API_URI = environment.URI;

  constructor(private http: HttpClient, public router: Router) { }

  /**
   * Funcion encargada de obtener todos los roles desde el Servidor.
   */
  getAllRoles(): Observable<Rol[]> {
    let URL = this.API_URI + 'roles';

    return this.http
      .get(URL, { headers: getHeaders() })
      // .map(extractData)
      .catch(handleError);
  }
}
