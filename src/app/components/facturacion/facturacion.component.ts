import { Component, OnInit } from '@angular/core';
import { fadeInOutTranslate } from '../../shared/elements/animation';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({ 
    moduleId: module.id,
    selector: 'facturacion',
    templateUrl: 'facturacion.component.html',
    styleUrls: ['facturacion.sass'],
    animations: [ fadeInOutTranslate ],
    providers: [ NgbTabsetConfig ]
})

export class FacturacionComponent implements OnInit {

    public tabActive: string;

    constructor(private config: NgbTabsetConfig, private router: Router) 
    {
        config.justify = 'justified';
    }

    ngOnInit() {
        if (this.router.url == '/facturacion/compra')
            this.tabActive = 'tab-compra';
        else if (this.router.url == '/facturacion/venta')
            this.tabActive = 'tab-venta';
        else if (this.router.url == '/facturacion/nota-credito')
            this.tabActive = 'tab-nota-credito';
        else
            this.tabActive = 'tab-compra';
    }
}