export class Impuesto {

  public id: number;
  public nombre: string;
  public valor: number;
  public date: Date;
  public state: number;
  public created_at: Date;
  public updated_at: Date;

  constructor(data: any = {}) {
    this.id = data.id || null;
    this.nombre = data.nombre || null;
    this.valor = data.valor || null;
    this.date = data.date || null;
    this.state = data.state || null;
    this.created_at = data.created_at || null;
    this.updated_at = data.updated_at || null;
  }

}
