import { Factura } from "./factura";

export class NotaDeCredito {

    public id: number;
    public id_factura: number;
    public numero: number;
    public eliminado: number;
    public descripcion: string;
    public fecha: Date;
    public fecha_de_creacion: Date;
    public factura: Factura;

    public constructor( data: any = {}) {
        this.id = data.id || null;
        this.id_factura = data.id_factura || null;
        this.numero = data.numero || null;
        this.descripcion = data.descripcion || null;
        this.fecha = data.fecha || null;
        this.fecha_de_creacion = data.fecha_de_creacion || null;
        this.factura = data.factura || null;
    }
}
