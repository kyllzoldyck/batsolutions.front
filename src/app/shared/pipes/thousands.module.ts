import { NgModule } from "@angular/core";
import { ThousandsSeparatorPipe } from "./thousands.separator.pipe";

@NgModule({
    declarations: [
        ThousandsSeparatorPipe
    ],
    exports: [
        ThousandsSeparatorPipe
    ]
})
export class ThousandsPipeModule { }