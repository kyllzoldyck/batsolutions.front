import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { getHeaders } from '../utils';
import { Access } from '../../models/access';

export interface BadgeItem {
    type: string;
    value: string;
}

export interface ChildrenItems {
    state: string;
    target?: boolean;
    name: string;
    type?: string;
    children?: ChildrenItems[];
}

export interface MainMenuItems {
    state: string;
    main_state?: string;
    target?: boolean;
    name: string;
    type: string;
    icon: string;
    badge?: BadgeItem[];
    children?: ChildrenItems[];
}

export interface Menu {
    label: string;
    main: MainMenuItems[];
}

const httpOptions = {
    headers: new HttpHeaders({})
};

@Injectable()
export class MenuItems {

    private menu: any;

    constructor(private http: HttpClient) { }

    getAll(rol: number, entity: number): Observable<Access[]> {

        let URI = environment.URI;

        httpOptions.headers.set('Accept', 'application/json');

        return this.http.get<Access[]>(URI + 'accesses/roles/entities?id_rol=' + rol + '&id_entity=' + entity, { headers: getHeaders() });
    }

    getAllWithRolAndEntity(rol: number, entity: number): Observable<Access[]> {

        let URI = environment.URI;

        httpOptions.headers.set('Accept', 'application/json');

        return this.http.get<Access[]>(URI + 'accesses?id_rol=' + rol + '&id_entity=' + entity, { headers: getHeaders() });
    }

    /*add(menu: Menu) {
      MENUITEMS.push(menu);
    }*/
}
