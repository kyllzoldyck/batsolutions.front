import { Component, ElementRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Proveedor } from '../../models/proveedor';
import { Usuario } from '../../models/usuario';
import { Empresa } from '../../models/empresa';
import { AuthService } from '../../services/auth.service';
import { ProveedoresService } from '../../services/proveedores.service';
import { EmpresaService } from '../../services/empresas.service';
import { EmailValidator } from '@angular/forms/src/directives/validators';
import { isValidRut } from '../../shared/rut.validation';
import { isValidEmail } from '../../shared/email.validation';
import { RutPipe } from 'ng2-rut';
import { Router } from '@angular/router';
import { getErrorsForm } from '../../shared/utils';
import swal from 'sweetalert2';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ModalBasicComponent } from '../../shared/modal-basic/modal-basic.component';

@Component({
    moduleId: module.id,
    selector: 'proveedores',
    templateUrl: 'proveedores.component.html',
    providers: [RutPipe]
})
export class ProveedoresComponent implements OnInit, AfterViewInit {

    proveedor: Proveedor;
    isLoading = true;
    showFormState = false;
    showFormTitle = false;
    isUpdate = false;
    usuario: Usuario;
    empresas: Empresa[];
    columnsProveedores = ['rut', 'razon_social', 'nombre_contacto', 'email', 'empresa', 'acciones'];
    dataSourceProveedores = new MatTableDataSource<Proveedor>();
    havePermission: boolean = false;

    @ViewChild('buttonSubmit') buttonSubmit: ElementRef;
    @ViewChild('paginatorProveedores') paginatorProveedores: MatPaginator;
    @ViewChild('modal') modal: ModalBasicComponent;

    formErrors = {
        'rut': '',
        'razon_social': '',
        'contacto': '',
        'direccion': '',
        'telefono': '',
        'email': '',
        'id_empresa': '',
        'giro': ''
    };

    validationMessages = {
        'rut': {
            'required': 'Debe ingresar un Rut.',
            'maxlength': 'El Rut no debe superar los 10 caracteres.',
            'minlength': 'El Rut debe tener al menos 8 caracteres.',
            'isValidRut': 'El Rut no es válido.'
        },
        'razon_social': {
            'required': 'Debe ingresar una Razón Social.',
            'maxlength': 'La  Razón Social no debe superar los 300 caracteres.',
            'minlength': 'La  Razón Social debe tener al menos 3 caracteres.'
        },
        'contacto': {
            'maxlength': 'El Nombre del Contacto no debe superar los 200 caracteres.',
        },
        'direccion': {
            'required': 'Debe ingresar una Dirección.',
            'maxlength': 'La Dirección no deben superar los 200 caracteres.',
            'minlength': 'La Dirección deben tener como mínimo 3 caracteres.'
        },
        'telefono': {
            'required': 'Debe ingresar un Teléfono.',
        },
        'email': {
            'required': 'Debe ingresar un Email',
            'maxlength': 'El Email no debe superar los 50 caracteres.',
            'minlength': 'El Email debe tener como mínimo 3 caracteres.',
            'isValidEmail': 'Debe ingresar un Email válido.'
        },
        'giro': {
          'required': "Debe ingresar un giro",
          'maxlength': "El Giro no debe superar los 300 caracteres.",
          'minlength': "El Giro debe tener como mínimo 3 caracteres"
        },
        'id_empresa': {
            'required': 'La empresa es obligatoria.'
        }
    };

    proveedoresForm: FormGroup;

    constructor(
        public fb: FormBuilder,
        public proveedoresService: ProveedoresService,
        public authService: AuthService,
        public rutPipe: RutPipe,
        public empresaService: EmpresaService,
        public router: Router) {
        this.usuario = new Usuario();
        this.proveedor = new Proveedor();
    }

    ngOnInit(): void {
        this.columnsProveedores.pop();
        
        this.getEmpresas();
        this.LoadAllProveedores();
        this.getAuthenticatedUser();
        this.buildForm();
    }

    ngAfterViewInit(): void {
        this.dataSourceProveedores.paginator = this.paginatorProveedores;
    }

    getAuthenticatedUser() {
        this.authService.getAuthenticatedUser()
            .subscribe(data => {
                this.usuario = data;

                if (this.usuario.id_rol != 3) {
                    
                    this.havePermission = true;
                    this.columnsProveedores.push('acciones');
                }

                this.showFormTitle = true;
            }, err => {
                localStorage.removeItem('bat');
                location.reload();
            });
    }

    onChangeEmpresa(empresa: number) {
        this.proveedor.id_empresa = empresa;
    }

    /**
    * Construye el formulario y sus validaciones
    */
    buildForm() {

        let id_empresa = [this.proveedor.id_empresa, Validators.compose([
            Validators.required
        ])];

        if (this.usuario.id_rol != 1) {
            id_empresa = [];
        }

        this.proveedoresForm = this.fb.group({

            rut: [
                this.proveedor.rut,
                Validators.compose([
                    Validators.required,
                    Validators.maxLength(10),
                    Validators.minLength(8),
                    isValidRut()
                ])
            ],
            razon_social: [
                this.proveedor.razon_social,
                Validators.compose([
                    Validators.required,
                    Validators.maxLength(300),
                    Validators.minLength(3)
                ])
            ],
            contacto: [
                this.proveedor.nombre_contacto,
                Validators.compose([
                    Validators.maxLength(200)
                ])
            ],
            direccion: [
                this.proveedor.direccion,
                Validators.compose([
                    Validators.required,
                    Validators.maxLength(200),
                    Validators.minLength(3)
                ])
            ],
            telefono: [
                this.proveedor.telefono,
                Validators.compose([
                    Validators.required
                ])
            ],
            email: [
                this.proveedor.email,
                Validators.compose([
                    Validators.required,
                    Validators.maxLength(50),
                    Validators.minLength(3),
                    isValidEmail()
                ])
            ],
            giro: [
                this.proveedor.giro,
                Validators.compose([
                  Validators.required,
                  Validators.maxLength(300),
                  Validators.minLength(3)
                ])
            ],
            id_empresa: id_empresa
        });

        // Cada vez que hay un cambio en el form llama a onValueChanged
        this.proveedoresForm.valueChanges.subscribe(
            data => this.onValueChanged(data)
        );

        this.onValueChanged();
    }

    // Cada vez que los valores cambian en el Form, se validan.
    onValueChanged(data?: any) {

        if (!this.proveedoresForm) {
            return;
        }

        let form = this.proveedoresForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    LoadAllProveedores() {
        this.dataSourceProveedores.data = [];

        this.proveedoresService.getAllProveedores().subscribe(
            data => {
                setTimeout(() => {
                    this.isLoading = false;
                    this.dataSourceProveedores.data = data;
                }, 500);
            },
            err => {
                swal({
                    title: 'Ups! Tenemos un problema',
                    text: err.error,
                    type: 'error'
                }).then(() => {
                    this.cleanFormAndReload();
                });
            }
        )
    }

    getEmpresas() {
        this.empresaService.getAllEmpresas().subscribe(
            data => {
                this.empresas = data;
            },
            err => {
                swal(
                    'Ups! Tenemos un problema.',
                    'Lo sentimos, no se han podido cargar las empresas',
                    'error'
                ).then(() => this.router.navigate(['/home']));
            }
        );
    }

    /**
    * Limpia el formulario y carga nuevamente los proveedores.
    */
    cleanFormAndReload() {
        this.isLoading = true;
        this.isUpdate = false;
        this.showFormState = false;
        this.LoadAllProveedores();
        this.proveedoresForm.reset();
        this.hide();
    }

    /**
    * Se encarga de mostrar/esconder el formulario.
    */
    show() {
        this.modal.show();
        if (this.isUpdate)
            this.cleanForm();
    }

    hide() {
        this.modal.hide();
    }

    /**
    * Se encarga de llenar el formulario con los datos para actualizar el proveedor.
    * @param event
    */
    fillFormForEdit(value) {

        let proveedor = this.dataSourceProveedores.data.find(r => r.id == value);

        this.proveedor.id = proveedor.id;
        this.proveedor.rut = proveedor.rut;
        this.proveedor.razon_social = proveedor.razon_social;
        this.proveedor.nombre_contacto = proveedor.nombre_contacto;
        this.proveedor.telefono = proveedor.telefono;
        this.proveedor.email = proveedor.email;
        this.proveedor.direccion = proveedor.direccion;
        this.proveedor.id_empresa = proveedor.id_empresa;
        this.proveedor.giro = proveedor.giro;

        this.modal.show();
        this.isUpdate = true;
    }

    /**
    * Cuando se envia el formulario.
    */
    onSubmit() {
        if (!this.isUpdate)
            this.addProveedor();
        else
            this.updateProveedor();
    }

    /**
    * Conecta con el servicio y envia el proveedor para su actualizacion
    */
    addProveedor() {

        this.buttonSubmit.nativeElement.disabled = true;

        this.proveedoresService.addProveedor(this.proveedor).subscribe(
            res => {
                this.buttonSubmit.nativeElement.disabled = false;

                swal({
                    title: '¡Proveedor Creado!',
                    text: 'El proveedor ha sido creado correctamente',
                    type: 'success'
                }).then(() => this.cleanFormAndReload());
            },
            err => {
                this.buttonSubmit.nativeElement.disabled = false;
                
                swal({
                    title: 'Ups! Tenemos un problema',
                    html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
                    type: err.status == 422 ? 'warning' : 'error'
                });
            }
        )
    }

    /**
    * Conecta con el servicio y envia el proveedor para su actualizacion
    */
    updateProveedor() {
        swal({
            title: `Se actualizará el proveedor ` + this.proveedor.razon_social,
            text: "¿Estás seguro de actualizar?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Actualizar'
        }).then(result => {
            if (result.value) {
        
                this.buttonSubmit.nativeElement.disabled = true;

                this.proveedoresService.updateProveedor(this.proveedor).subscribe(
                    res => {
                        this.buttonSubmit.nativeElement.disabled = false;

                        swal(
                            '¡Proveedor Actualizado!',
                            'El proveedor ha sido actualizado correctamente!',
                            'success'
                        ).then(() => this.cleanFormAndReload());
                    },
                    err => {

                        this.buttonSubmit.nativeElement.disabled = false;

                        swal({
                            title: 'Ups! Tenemos un problema',
                            html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
                            type: err.status == 422 ? 'warning' : 'error'
                        });
                    }
                );
            }
        });
    }

    /**
    * Conecta con el servicio para eliminar un proveedor enviando el id
    * @param id Id Proveedor
    */
    deleteProveedor(id) {

        let proveedor = this.dataSourceProveedores.data.find(p => p.id = id);

        swal({
            title: `Se eliminará el proveedor: ${proveedor.razon_social}`,
            text: "¿Estás seguro de eliminar?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar'
        }).then(result => {
            if (result.value) {
                this.proveedoresService.deleteProveedor(proveedor.id).subscribe(
                    res => {
                        swal(
                            '¡Proveedor Eliminado!',
                            'El proveedor ha sido eliminado correctamente!',
                            'success'
                        ).then(() => this.cleanFormAndReload());
                    },
                    err => {
                        swal({
                            title: 'Ups! Tenemos un problema',
                            text: err.error || 'Intentelo más tarde.',
                            type: 'error'
                        });
                    }
                );
            }
        });
    }

    /**
    * Se gatilla al momento de limpiar el form
    */
    cleanForm() {
        this.isUpdate = false;
        this.proveedoresForm.reset();
    }

    getCount() {
        // return this.source.count();
    }

}
