import { Component, AfterViewInit, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Usuario } from "../../models/usuario";
import { Empresa } from "../../models/empresa";
import { Cliente } from "../../models/cliente";
import { ClientesService } from "../../services/clientes.service";
import { EmpresaService } from "../../services/empresas.service";
import { EmailValidator } from "@angular/forms/src/directives/validators";
import { isValidRut } from "../../shared/rut.validation";
import { isValidEmail } from "../../shared/email.validation";
import { RutPipe } from "ng2-rut";
import { Router } from "@angular/router";
import { getErrorsForm } from "../../shared/utils";
import swal from "sweetalert2";
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ModalBasicComponent } from '../../shared/modal-basic/modal-basic.component';

@Component({
  moduleId: module.id,
  selector: "clientes",
  templateUrl: "clientes.component.html",
  providers: [RutPipe]
})
export class ClientesComponent implements OnInit, AfterViewInit {

  cliente = new Cliente();
  isLoading: boolean = true;
  showFormState: boolean = false;
  showFormTitle: boolean = false;
  isUpdate: boolean = false;
  usuario: Usuario;
  empresas: Empresa[];
  columnsClientes = ['rut', 'razon_social', 'nombre_contacto', 'email', 'empresa', 'acciones'];
  dataSourceClientes = new MatTableDataSource<Cliente>();
  havePermission: boolean = false;

  @ViewChild('buttonSubmit') buttonSubmit: ElementRef;
  @ViewChild('paginatorClientes') paginatorClientes: MatPaginator;
  @ViewChild('modal') modal: ModalBasicComponent;

  formErrors = {
    rut: "",
    razon_social: "",
    contacto: "",
    direccion: "",
    telefono: "",
    email: "",
    id_empresa: "",
    giro: ""
  };

  validationMessages = {
    rut: {
      required: "Debe ingresar un Rut.",
      maxlength: "El Rut no debe superar los 10 caracteres.",
      minlength: "El Rut debe tener al menos 8 caracteres.",
      isValidRut: "El Rut no es válido."
    },
    razon_social: {
      required: "Debe ingresar una Razón Social.",
      maxlength: "La  Razón Social no debe superar los 300 caracteres.",
      minlength: "La  Razón Social debe tener al menos 3 caracteres."
    },
    contacto: {
      maxlength: "El Nombre del Contacto no debe superar los 200 caracteres."
    },
    direccion: {
      required: "Debe ingresar una Dirección.",
      maxlength: "La Dirección no deben superar los 200 caracteres.",
      minlength: "La Dirección deben tener como mínimo 3 caracteres."
    },
    telefono: {
      required: "Debe ingresar un Teléfono."
    },
    email: {
      required: "Debe ingresar un Email",
      maxlength: "El Email no debe superar los 50 caracteres.",
      minlength: "El Email debe tener como mínimo 3 caracteres.",
      isValidEmail: "Debe ingresar un Email válido."
    },
    giro: {
      required: "Debe ingresar un giro",
      maxlength: "El Giro no debe superar los 300 caracteres.",
      minlength: "El Giro debe tener como mínimo 3 caracteres"
    },
    id_empresa: {
      required: "El campo empresa es obligatorio"
    }
  };

  clientesForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    public clientesService: ClientesService,
    public rutPipe: RutPipe,
    public authService: AuthService,
    public empresaService: EmpresaService,
    public router: Router
  ) {
    this.usuario = new Usuario();
    this.empresas = [];
  }

  ngOnInit() {
    this.columnsClientes.pop();

    this.getEmpresas();
    this.LoadAllClientes();
    this.getAuthenticatedUser();
    this.buildForm();
  }

  ngAfterViewInit(): void {
    this.dataSourceClientes.paginator = this.paginatorClientes;
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.usuario = data;

        if (this.usuario.id_rol != 3) {
          this.columnsClientes.push('acciones');
          this.havePermission = true;
        }
        this.showFormTitle = true;
      },
        err => {
          localStorage.removeItem("bat");
          location.reload();
        }
      );
  }

  onChangeEmpresa(empresa: number) {
    this.cliente.id_empresa = empresa;
  }

  /**
   * Construye el formulario y sus validaciones
   */
  buildForm() {
    let id_empresa = [
      this.cliente.id_empresa,
      Validators.compose([Validators.required])
    ];

    if (this.usuario.id_rol != 1) {
      id_empresa = [];
    }

    this.clientesForm = this.fb.group({
      rut: [
        this.cliente.rut,
        Validators.compose([
          Validators.required,
          Validators.maxLength(10),
          Validators.minLength(8),
          isValidRut()
        ])
      ],
      razon_social: [
        this.cliente.razon_social,
        Validators.compose([
          Validators.required,
          Validators.maxLength(300),
          Validators.minLength(3)
        ])
      ],
      contacto: [
        this.cliente.nombre_contacto,
        Validators.compose([Validators.maxLength(200)])
      ],
      direccion: [
        this.cliente.direccion,
        Validators.compose([
          Validators.required,
          Validators.maxLength(200),
          Validators.minLength(3)
        ])
      ],
      telefono: [
        this.cliente.telefono,
        Validators.compose([Validators.required])
      ],
      email: [
        this.cliente.email,
        Validators.compose([
          Validators.required,
          Validators.maxLength(50),
          Validators.minLength(3),
          isValidEmail()
        ])
      ],
      giro: [
        this.cliente.giro,
        Validators.compose([
          Validators.required,
          Validators.maxLength(300),
          Validators.minLength(3)
        ])
      ],
      id_empresa: id_empresa
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.clientesForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.clientesForm) {
      return;
    }

    const form = this.clientesForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = "";
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + " ";
        }
      }
    }
  }

  getEmpresas() {
    this.empresaService.getAllEmpresas().subscribe(
      data => {
        this.empresas = data;
      },
      error => {
        swal(
          "Ups! Tenemos un problema.",
          "Lo sentimos, no se han podido cargar las empresas",
          "error"
        ).then(() => this.router.navigate(["/home"]));
      }
    );
  }

  LoadAllClientes() {
    this.dataSourceClientes.data = [];
    
    this.clientesService.getAllClientes().subscribe(
      data => {
        setTimeout(() => {
          this.isLoading = false;
          this.dataSourceClientes.data = data;
        }, 500);
      },
      err => {
        swal({
          title: "Ups! Tenemos un problema",
          html: err.error,
          type: "error"
        }).then(() => {
          this.cleanFormAndReload();
        });
      }
    );
  }

  /**
   * Limpia el formulario y carga nuevamente los Clientes.
   */
  cleanFormAndReload() {
    this.isLoading = true;
    this.isUpdate = false;
    this.showFormState = false;
    this.LoadAllClientes();
    this.clientesForm.reset();
    this.hide();
  }

  /**
   * Se encarga de mostrar/esconder el formulario.
   */
  show() {
    this.modal.show();
    if (this.isUpdate)
      this.cleanForm();
  }

  hide() {
    this.modal.hide();
  }

  /**
   * Se encarga de llenar el formulario con los datos para actualizar el clientes.
   * @param event
   */
  fillFormForEdit(value) {
    let cliente = this.dataSourceClientes.data.find(r => r.id == value);

    this.cliente.id = cliente.id;
    this.cliente.rut = cliente.rut;
    this.cliente.razon_social = cliente.razon_social;
    this.cliente.nombre_contacto = cliente.nombre_contacto;
    this.cliente.telefono = cliente.telefono;
    this.cliente.email = cliente.email;
    this.cliente.direccion = cliente.direccion;
    this.cliente.id_empresa = cliente.id_empresa;
    this.cliente.giro = cliente.giro;

    this.modal.show();
    this.isUpdate = true;
  }

  /**
   * Cuando se envia el formulario.
   */
  onSubmit() {
    if (!this.isUpdate) this.addCliente();
    else this.updateCliente();
  }

  /**
   * Conecta con el servicio y envia el cliente para su actualizacion
   */
  addCliente() {

    this.buttonSubmit.nativeElement.disabled = true;

    this.clientesService.addCliente(this.cliente).subscribe(
      res => {
        this.buttonSubmit.nativeElement.disabled = false;

        swal({
          title: "¡Cliente Creado!",
          text: "El cliente ha sido creado correctamente",
          type: "success"
        }).then(() => this.cleanFormAndReload());
      },
      err => {
        this.buttonSubmit.nativeElement.disabled = false;
        swal({
          title: "Ups! Tenemos un problema",
          html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
          type: err.status == 422 ? 'warning' : 'error'
        });
      }
    );
  }

  /**
   * Conecta con el servicio y envia el cliente para su actualizacion
   */
  updateCliente() {
    swal({
      title: `Se actualizará el cliente ` + this.cliente.razon_social,
      text: "¿Estás seguro de actualizar?",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Actualizar"
    }).then(result => {
      if (result.value) {
        this.buttonSubmit.nativeElement.disabled = true;

        this.clientesService.updateCliente(this.cliente).subscribe(
          res => {
            this.buttonSubmit.nativeElement.disabled = false;

            swal(
              "Cliente Actualizado!",
              "El cliente ha sido actualizado correctamente!",
              "success"
            ).then(() => this.cleanFormAndReload());
          },
          err => {
            this.buttonSubmit.nativeElement.disabled = false;

            swal({
              title: "Ups! Tenemos un problema",
              html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
              type: err.status == 422 ? 'warning' : 'error'
            });
          }
        );
      }
    });
  }

  /**
   * Conecta con el servicio para eliminar un cliente enviando el id
   * @param event Id Cliente
   */
  deleteCliente(value) {
    let cliente = this.dataSourceClientes.data.find(r => r.id == value);

    swal({
      title: `Se eliminará el cliente: ${cliente.razon_social}`,
      text: "¿Estás seguro de eliminar?",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Eliminar"
    }).then(result => {
      if (result.value) {
        this.clientesService.deleteCliente(cliente.id).subscribe(
          res => {
            swal(
              "¡Cliente Eliminado!",
              "El cliente ha sido eliminado correctamente!",
              "success"
            ).then(() => this.cleanFormAndReload());
          },
          err => {
            swal({
              title: "Ups! Tenemos un problema",
              text: err.error || 'Intentelo más tarde.',
              type: 'error'
            });
          }
        );
      }
    });
  }

  /**
   * Se gatilla al momento de limpiar el form
   */
  cleanForm() {
    this.isUpdate = false;
    this.clientesForm.reset();
  }

  getCount() {
    // return this.source.count();
  }
}
