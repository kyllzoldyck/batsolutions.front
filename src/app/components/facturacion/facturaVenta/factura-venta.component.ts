import { Component, ViewChild, OnInit, AfterViewInit, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormsModule, FormControl } from '@angular/forms';
import { ArticulosService } from '../../../services/articulos.service';
import { ClientesService } from '../../../services/clientes.service';
import { FacturasService } from '../../../services/facturas.service';
import { ModalArticulosComponent } from './../shared/modal-articulos/modal-articulos.component';
import { Impuesto } from '../../../models/impuesto';
import { Factura } from '../../../models/factura';
import { TipoFactura } from "../../../shared/enum/TipoFactura";
import { DatePipe } from '@angular/common';
import { Articulo } from "../../../models/articulo";
import { isValidEmail } from '../../../shared/email.validation';
import { ImpuestosService } from '../../../services/impuestos.services';
import { getErrorsForm } from '../../../shared/utils';
import { AuthService } from '../../../services/auth.service';
import { Usuario } from '../../../models/usuario';
import { EmpresaService } from '../../../services/empresas.service';
import { Empresa } from '../../../models/empresa';
import { Router } from '@angular/router';
import swal from "sweetalert2";
import { Cliente } from '../../../models/cliente';
import { IOption } from "ng-select";
import { MatTable, MatPaginator, MatTableDataSource } from "@angular/material";

@Component({
  moduleId: module.id,
  providers: [DatePipe],
  selector: 'factura-venta',
  templateUrl: 'factura-venta.html',
  styleUrls: ['factura-venta.component.css']
})
export class FacturaVentaComponent implements OnInit, AfterViewInit {

  facturaVenta = new Factura();
  arrayArticulosAux = [];
  txtBuscar: string;
  cliente: Cliente;
  clientes: Cliente[] = [];
  clientesOptions: Array<IOption> = new Array<IOption>();
  showFormState: boolean = false;
  isUpdate: boolean = false;
  isLoading: boolean = true;
  isLoadingSearch: boolean = false;
  facturasEncontradasPorNumero = [];
  impuesto: Impuesto;
  usuario: Usuario;
  showNuevaFactura = false;
  empresas: Empresa[];

  @ViewChild(ModalArticulosComponent)
  modalChild: ModalArticulosComponent;

  @ViewChild('tableFacturas') tableFacturas: MatTable<any>;
  @ViewChild('tableDetalle') tableDetalle: MatTable<any>;

  @ViewChild('paginatorFactura') paginatorFactura: MatPaginator;
  @ViewChild('paginatorDetalle') paginatorDetalle: MatPaginator;

  @ViewChild('buttonSubmitUpdate') buttonSubmitUpdate: ElementRef;
  @ViewChild('buttonSubmit') buttonSubmit: ElementRef;

  constructor(
    private empresaService: EmpresaService,
    private authService: AuthService,
    private fb: FormBuilder,
    private articulosService: ArticulosService,
    private clientesService: ClientesService,
    private facturasService: FacturasService,
    public datePipe: DatePipe,
    private impuestosService: ImpuestosService,
    public router: Router,
    private cdr: ChangeDetectorRef
  ) {
    this.impuesto = new Impuesto();
    this.usuario = new Usuario();
    this.cliente = new Cliente();
  }

  columnsFacturas = [
    { name: 'Número', prop: 'numero' },
    { name: 'Fecha', prop: 'fecha' },
    { name: 'Cliente', prop: 'razon_social' },
    { name: 'Empresa', prop: 'name_empresa' },
    { name: 'Total', prop: 'total' },
    { name: '¿Anulada?', prop: 'name_nula' }
  ];

  columnsFactura = ['numero', 'fecha_de_creacion', 'fecha', 'cliente', 'empresa', 'exenta', 'total', 'name_nula', 'acciones'];
  dataSourceFacturas = new MatTableDataSource<Factura>();

  columnsDetalleFactura = ['descripcion', 'precio_de_compra', 'unidades', 'subtotal', 'accion'];
  dataSourceDetalle = new MatTableDataSource();

  factVentaForm: FormGroup;

  formErrors = {
    empresa: '',
    cliente: '',
    numero: '',
    fecha: '',
    fono: '',
    email: '',
    descripcion: '',
    exenta: ''
  };

  validationMessages = {
    'empresa': {
      'required': 'Debe seleccionar una empresa'
    },
    'cliente': {
      'required': 'Debe seleccionar un Cliente.',
    },
    'numero': {
      'required': 'Debe ingresar el Número de Factura.',
    },
    'fecha': {
      'required': 'Debe ingresar una Fecha.',
    },
    'fono': {
      'required': 'Debe ingresar un Télefono.',
    },
    'email': {
      'required': 'Debe ingresar un Email.',
      'isValidEmail': 'Debe ingresar un Email válido.'
    },
    'exenta': {
      'required': 'Debe seleccionar exenta.',
      'min': 'Exenta es inválido.',
      'max': 'Exenta es inválido'
    }
  };

  ngOnInit() {
    this.columnsFactura.pop();

    this.getAuthenticatedUser();
    this.LoadAllClientes();
    this.LoadAllFacturasVenta();
    this.getIva();
    this.buildForm();

    this.dataSourceFacturas.filterPredicate = (data: Factura, filter) => {
      const dataStr = data.numero + data.razon_social.toLowerCase() + data.name_empresa.toLowerCase();
      return dataStr.indexOf(filter) != -1;
    };
  }

  ngAfterViewInit(): void  {
    this.dataSourceFacturas.paginator = this.paginatorFactura;
    this.dataSourceDetalle.paginator = this.paginatorDetalle;
  }

  /**
  * Construye el formulario y sus validaciones
  */
  buildForm() {
    let empresa = [];

    if (this.usuario.id_rol == 1) {
      empresa = [
        this.facturaVenta.id_empresa, Validators.compose([
          Validators.required
        ])];
    }

    this.factVentaForm = this.fb.group({
      empresa: empresa,
      cliente: [
        this.facturaVenta.razon_social, Validators.compose([
          Validators.required,
        ])],
      numero: [
        this.facturaVenta.numero, Validators.compose([
          Validators.required,
        ])],
      fecha: [
        this.facturaVenta.fecha, Validators.compose([
          Validators.required,
        ])],
      descripcion: [
        this.facturaVenta.descripcion
      ],
      exenta: [
        this.facturaVenta.exenta,
        Validators.compose([
          Validators.required
        ])
      ]
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.factVentaForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.factVentaForm) {
      return;
    }

    const form = this.factVentaForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  showAll() {
    this.txtBuscar = "";
    this.showFormState = false;
    this.isLoading = true;
    this.LoadAllFacturasVenta();
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser().subscribe(
      data => {

        this.usuario = data;

        if (this.usuario.id_rol != 3) {
          // this.settingsFactura.actions.edit = true;
          // this.settingsFactura.actions.delete = true;
          this.columnsFactura.push('acciones');
          this.showNuevaFactura = true;
        }

        if (this.usuario.id_rol == 1) {
          this.getEmpresas();
          // this.settingsFactura.columns.id_empresa;
        }

        if (this.usuario.id_rol != 1) {
          // delete this.settingsFactura.columns.id_empresa;
        }
      },
      err => {
        localStorage.removeItem('bat');
        location.reload();
      }
    );
  }

  getIva() {
    this.impuestosService.getImpuestoByNombre('iva').subscribe(
      data => {
        this.impuesto = data;
      },
      error => {
        swal({
          title: 'Ups! Tenemos un problema',
          text: 'No se pudo cargar el IVA',
          type: 'error'
        });
      }
    );
  }

  /**
  * Alimenta el DDL de selección de clientes.
  */
  LoadAllClientes() {
    this.clientesService.getAllClientes().subscribe(
      data => {
        this.clientesOptions = [];
        data.forEach(d => {

          this.clientes.push(d);

          let clienteOption: IOption = { value: d.rut, label: d.rut + ' | ' + d.razon_social, disabled: false };

          this.clientesOptions.push(clienteOption);
        });
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        }).then(() => {
          this.cancel()
        })
      }
    )
  }

  /**
   * Carga la grilla de facturas.
   */
  LoadAllFacturasVenta() {
    this.facturasService.getAllFacturasDeVenta().subscribe(
      data => {
        this.dataSourceFacturas.data = [];
        this.dataSourceFacturas.paginator = this.paginatorFactura;

        let facturas: Factura[] = [];

        data.forEach(f => {
          f.name_nula = f.nula == 0 ? 'No' : 'Si';
          facturas.push(f);
        });

        this.dataSourceFacturas.data = facturas;
        setTimeout(() => this.isLoading = false, 500);
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        }).then(() => {
          this.cancel()
        })
      }
    );
  }

  /**
  * Retorna la cantidad de articulos asociados a una factura
  */
  getCountArticulosFactura(): number {
    return this.dataSourceDetalle.data.length;
  }

  /**
  * Retorna la cantidad de facturas
  */
  getCountFacturas(): number {
    return this.dataSourceFacturas.data.length;
  }

  /**
   * Elimina una factura
   * @param event
   */
  deleteArticuloFactura(event): void {
    swal({
      title: `¿Estás seguro?`,
      text: `Se eliminará el articulo: ${event.descripcion}`,
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar'
    }).then(result => {
      if (result.value) {
        let index = this.dataSourceDetalle.data.indexOf(event)
        if (index > -1) this.dataSourceDetalle.data.splice(index, 1);

        this.dataSourceDetalle.data = this.dataSourceDetalle.data;
        this.calculateTotal();
      }
    });
  }

  /**
   * Envia una factura para ser eliminada en el servidor.
   * @param event
   */
  deleteFactura(event) {
    swal({
      title: `¿Estás seguro?`,
      text: `Se eliminará la factura número: ${event.numero}`,
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar'
    }).then(result => {
      if (result.value) {
        this.facturasService.deleteFacturaById(event.id).subscribe(
          res => {
            swal(
              'Factura Eliminada!',
              'La Factura ha sido eliminado correctamente!',
              'success'
            ).then(() => {
              this.isLoading = true;
              this.LoadAllFacturasVenta()
            });
          },
          err => {
            swal({
              title: 'Ups! Tenemos un problema',
              text: err.error,
              type: 'error'
            });
          }
        );
      }
    });
  }

  /**
   * Ejecuta la carga de los articulos en el modal.
   */
  LoadAllArticulosModal() {
    this.modalChild.LoadAllArticulos(true, this.facturaVenta.id_empresa);
  }

  /**
  * Obtiene los articulos desde el modal
  * @param event
  */
  getArticulosFromChild(event): void {
    if (this.arrayArticulosAux.length > 0) {
      event.forEach(element => {
        //Verifica que exista el articulo
        let articuloExistente = this.arrayArticulosAux.filter(a => {
          return a.id == element.id
        });

        if (articuloExistente.length != 0) {
          //Obtiene el indice del articulo que existe.
          let index = this.arrayArticulosAux.indexOf(articuloExistente[0], 0)

          //Replaza el articulo.
          if (index != -1)
            this.arrayArticulosAux.splice(index, 1, element)

        } else
          //Si no existe, lo agrega al final.
          this.arrayArticulosAux.push(element);
      });
    } else
      this.arrayArticulosAux = event;

    this.dataSourceDetalle.paginator = this.paginatorDetalle;
    this.dataSourceDetalle.data = this.arrayArticulosAux;

    this.calculateTotal();
    this.tableDetalle.renderRows();
  }

  /**
  * Calcula el valor de los totales en la factura
  */
  calculateTotal() {

    this.facturaVenta.sub_total = 0;
    this.facturaVenta.iva = 0;
    this.facturaVenta.total = 0;
    this.arrayArticulosAux.forEach(articulo => {
      this.facturaVenta.sub_total += articulo.subtotal;
    });

    this.facturaVenta.detalles_factura = this.arrayArticulosAux;
    this.facturaVenta.iva = this.facturaVenta.exenta == 1 ? 0 : + (this.facturaVenta.sub_total * this.impuesto.valor);
    this.facturaVenta.total = + (this.facturaVenta.sub_total + this.facturaVenta.iva);

    this.facturaVenta.iva = this.facturaVenta.exenta == 1 ? 0 : Math.round(this.facturaVenta.iva);
    this.facturaVenta.sub_total = Math.round(this.facturaVenta.sub_total);
    this.facturaVenta.total = Math.round(this.facturaVenta.total);

  }

  /**
   * Gatilla la creacion de una factura
   */
  onSubmit() {
    this.buttonSubmit.nativeElement.disabled = true;
    this.facturaVenta.tipo_factura = TipoFactura.Venta;

    this.facturasService.addFactura(this.facturaVenta).subscribe(
      res => {
        this.buttonSubmit.nativeElement.disabled = false;
        swal({
          title: 'Factura Creada!',
          text: 'La factura ha sido creada correctamente',
          type: 'success'
        }).then(() => this.cancelFromButton());
      },
      err => {
        this.buttonSubmit.nativeElement.disabled = false;
        swal({
          title: 'Ups! Tenemos un problema',
          html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
          type: err.status == 422 ? 'warning' : 'error'
        });
      }
    );
  }

  /**
  * Actualiza una factura
  * @param event
  */
  onUpdate(event) {
    swal({
      title: `Se actualizará la Factura número` + this.facturaVenta.numero,
      text: "¿Estás seguro de actualizar?",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar'
    }).then(result => {

      if (result.value) {
        this.buttonSubmitUpdate.nativeElement.disabled = true;

        this.facturasService.updateFactura(this.facturaVenta).subscribe(
          res => {
            this.buttonSubmitUpdate.nativeElement.disabled = false;

            swal(
              '¡Factura Actualizada!',
              'La Factura ha sido actualizado correctamente!',
              'success'
            ).then(() => this.cancelFromButton());
          },
          err => {
            this.buttonSubmitUpdate.nativeElement.disabled = false;

            swal({
              title: 'Ups! Tenemos un problema',
              html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
              type: err.status == 422 ? 'warning' : 'error'
            });
          }
        );
      }
    });
  }

  /**
  * Reinicia el fomulario y limpia
  */
  cancel() {
    this.LoadAllClientes();
    this.factVentaForm.reset();
    this.txtBuscar = "";
    this.showFormState = true;
    this.isUpdate = false;
    this.arrayArticulosAux = [];
    this.facturaVenta = new Factura();
    this.cliente = new Cliente();
    this.columnsDetalleFactura.indexOf('accion') == -1 ? this.columnsDetalleFactura.push('accion') : null;
    this.dataSourceDetalle.data = [];
    this.dataSourceFacturas.data = [];
  }

  cancelFromButton() {
    this.LoadAllClientes();
    this.factVentaForm.reset();
    this.txtBuscar = "";
    this.showFormState = false;
    this.isUpdate = false;
    this.arrayArticulosAux = [];
    this.facturaVenta = new Factura();
    this.cliente = new Cliente();
    this.showAll();
  }

  /**
   * Busca una factura obtiene su detalle y renderiza los datos.
   */
  searchFacturaByNumber() {
    this.facturasService.getFacturaVentaByNumero(+this.txtBuscar).subscribe(
      res => {
        this.facturasEncontradasPorNumero = JSON.parse(res);

        if (this.facturasEncontradasPorNumero.length > 0) {
          this.facturasEncontradasPorNumero.forEach(fact => {
            if (fact.rut) {
              let cliente = this.clientes.find(c => c.rut == fact.rut)
              fact.razon_social = cliente.razon_social;
            } else {
              fact.razon_social = "-";
            }
          });
        } else {
          swal({
            title: '¡Facturas no encontradas!',
            text: `No existen facturas con el número ${this.txtBuscar}.`,
            type: 'info'
          })
        }
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        })
      }
    )
  }

  /**
  * Se encarga de pasar una factura al metodo que completa el formulario.
  * @param factura
  */
  selectFacturaToView(factura) {
    this.searchFacturaByIdAux(factura.id);
    this.facturasEncontradasPorNumero = [];
  }

  closeSearchBox() {
    this.facturasEncontradasPorNumero = [];
  }

  /**
   * Busca una factura obtiene su detalle y renderiza los datos.
   */
  searchFacturaByIdAux(id) {
    this.facturasService.getFacturaById(id).subscribe(
      res => {
        this.fillTheFacturaFound(res, true)
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        })
      }
    )
  }

  /**
   * Busca una factura obtiene su detalle y renderiza los datos.
   */
  searchFacturaById(factura) {
    this.facturasService.getFacturaById(factura.id).subscribe(
      res => {
        this.fillTheFacturaFound(res, true)
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        })
      }
    )
  }

  /**
   * Arma un objeto para mostrar la data en el formulario.
   * @param factura
  */
  fillTheFacturaFound(factura, mustParse) {
    //TODO: OPTIMIZAR ESTE CODIGO!
    this.showFormState = true;
    this.isUpdate = true;
    this.isLoadingSearch = true;
    this.columnsDetalleFactura.indexOf('accion') > 0 ? this.columnsDetalleFactura.splice(this.columnsDetalleFactura.indexOf("accion"), 1) : null;
    //let Factura = mustParse ? JSON.parse(factura) : factura;
    this.facturaVenta.id = factura.id;
    this.facturaVenta.descripcion = factura.descripcion;
    this.facturaVenta.numero = factura.numero;
    this.facturaVenta.fecha = factura.fecha.split(" ")[0];
    this.facturaVenta.iva = factura.iva;
    this.facturaVenta.nula = factura.nula;
    this.facturaVenta.rut = factura.rut;
    this.facturaVenta.razon_social = factura.razon_social;
    this.facturaVenta.nombre_contacto = factura.nombre_contacto;
    this.facturaVenta.id_empresa = factura.id_empresa;
    this.facturaVenta.sub_total = factura.sub_total;
    this.facturaVenta.total = factura.total;
    this.facturaVenta.exenta = factura.exenta;
    this.facturaVenta.giro = factura.giro;
    let sourceArticulosFactura = [];
    this.dataSourceDetalle.data = [];

    factura.detalles_factura.forEach(item => {
      let obj = new Object();

      obj['id'] = item.id;
      obj['id_factura'] = item.id_factura;
      obj['precio'] = item.precio;
      obj['codigo_articulo'] = item.codigo_articulo;
      obj['descripcion_articulo'] = item.descripcion_articulo;
      obj['iva'] = item.iva;
      obj['unidades'] = item.unidades;
      obj['subtotal'] = parseInt(item.precio) * parseInt(item.unidades);

      sourceArticulosFactura.push(obj);
      this.arrayArticulosAux.push(obj);
    });
    this.getCliente(this.facturaVenta.rut);
    this.isLoadingSearch = false;
    setTimeout(() => this.dataSourceDetalle.paginator = this.paginatorDetalle);
    this.dataSourceDetalle.data = [...this.arrayArticulosAux];
  }

  cleanModal() {
    // jQuery('#datatable_articulos').DataTable().destroy();
  }

  getEmpresas() {
    this.empresaService.getAllEmpresas().subscribe(
      data => {
        this.empresas = data;
      },
      error => {
        swal(
          'Ups! Tenemos un problema.',
          'Lo sentimos, no se han podido cargar las empresas',
          'error'
        ).then(() => this.router.navigate(['/home']));
      }
    );
  }

  getClientesByEmpresa(e) {
    if (e != null) {
      this.clientesService.getClientesByEmpresa(e)
        .subscribe(
          data => {
            this.clientes = data;

            this.clientesOptions = [];

            data.forEach(d => {
              let clienteOption: IOption = { value: d.rut, label: d.rut + ' | ' + d.razon_social, disabled: false };
              this.clientesOptions.push(clienteOption);
            });

          },
          err => {
            swal({
              title: '¡Ups! Lo sentimos.',
              text: 'No se han podido cargar los porveedores',
              type: 'error'
            });
          }
        );
    } else {
      this.clientes = [];
    }
  }


  getCliente(c) {
    if (c != null) {
      this.cliente = this.clientes.find(c => c.rut == this.facturaVenta.rut);
      this.facturaVenta.giro = this.cliente.giro;
    }
  }

  changeExenta() {
    this.calculateTotal();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSourceFacturas.filter = filterValue;
  }
}
