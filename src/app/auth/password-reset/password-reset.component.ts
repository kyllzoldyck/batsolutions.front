import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Router, ActivatedRoute } from "@angular/router";
import swal from "sweetalert2";
import { getErrorsForm } from "../../shared/utils";

@Component({
    selector: 'password-reset',
    templateUrl: './password-reset.component.html',
    styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {

    public password: string;
    public passwordRepeat: string;
    public token: string;
    
    @ViewChild('newPasswordButton') newPasswordButton: ElementRef;

    public newPasswordForm: FormGroup;

    formErrors = {
        'password': '',
        'passwordRepeat': '',
    };

    validationMessages = {
        'password': {
            'required': 'Debe ingresar una contraseña.',
            'maxlength': 'La contraseña supera el máximo de caracteres.',
            'minLength': 'La contraseña no cuenta con el mínimo de caracteres.'
        },
        'passwordRepeat': {
            'required': 'Debe ingresar una contraseña.',
            'maxlength': 'La contraseña supera el máximo de caracteres.',
            'minLength': 'La contraseña no cuenta con el mínimo de caracteres.'
        }
    };

    constructor(private authService: AuthService, private fb: FormBuilder, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        this.buildForm();
        this.route.queryParams.subscribe(data => { this.token = data['t']; });
    }

    buildForm() {
        this.newPasswordForm = this.fb.group({
            password: [
                this.password, Validators.compose([
                    Validators.required,
                    Validators.maxLength(100),
                    Validators.minLength(6),
                ])
            ],
            passwordRepeat: [
                this.passwordRepeat, Validators.compose([
                    Validators.required,
                    Validators.maxLength(100),
                    Validators.minLength(6)         
                ])      
            ]
        });
    }

    // Cada vez que los valores cambian en el Form, se validan.
    onValueChanged(data?: any) {
        if (!this.newPasswordForm) {
            return;
        }

        const form = this.newPasswordForm;
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    newPassword() {
        console.log('entre');
        if (this.password == this.passwordRepeat)
        {
            this.newPasswordButton.nativeElement.disabled = true;

            this.authService.newPassword(this.token, this.password, this.passwordRepeat).subscribe(data => {
                this.newPasswordButton.nativeElement.disabled = false;
            
                swal('¡Muy Bien!', 'Su contraseña se ha creado correctamente.', 'success').then(() => {
                    this.router.navigate(['/auth/login']);
                });
            },
            error => {
                console.log(error);
                this.newPasswordButton.nativeElement.disabled = false;

                swal({
                    title: '¡Lo sentimos!',
                    html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error.error,
                    type: error.status == 422 ? 'warning' : 'error'
                  });
            });
        }
        else 
        {
            swal('Lo sentimos!', 'Las contraseñas no coinciden.', 'warning');
        }
    }

}