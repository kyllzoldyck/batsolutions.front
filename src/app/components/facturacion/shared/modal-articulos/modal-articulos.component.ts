import { Component, ViewChild, Output, EventEmitter, OnInit, AfterViewInit, ViewEncapsulation, Input } from "@angular/core";
import { ArticulosService } from "../../../../services/articulos.service";
import { FacturasService } from "../../../../services/facturas.service";
import { ProveedoresService } from "../../../../services/proveedores.service";
import { InputUnidadesComponent } from "../input-unidades/input-unidades.component";
import { Articulo } from "../../../../models/articulo";
import { ImpuestosService } from "../../../../services/impuestos.services";
import { Impuesto } from "../../../../models/impuesto";
import { Subject } from "rxjs/Subject";
import swal from "sweetalert2";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";
declare var jQuery: any;

const initialSelection = [];
const allowMultiSelect = true;

@Component({
  moduleId: module.id,
  selector: "modal-articulos",
  templateUrl: "modal-articulos.html",
  styleUrls: ["./modal-articulos.component.css"]
})
export class ModalArticulosComponent implements OnInit, AfterViewInit {

  isLoading: boolean = true;
  articulos: Articulo[];
  articulosAux: any[];
  impuesto: Impuesto;
  selected = [];
  unidades: number;

  public visible = false;
  public visibleAnimate = false;

  @ViewChild(InputUnidadesComponent)
  inputUnidad: InputUnidadesComponent;

  @ViewChild('paginator') paginator: MatPaginator;

  @Output() send: EventEmitter<any> = new EventEmitter();

  @Input('showInputPrecioVenta') showInputPrecioVenta: boolean = false;
  @Input('isPresupuesto') isPresupuesto: boolean = false;

  constructor(
    private articulosService: ArticulosService,
    private impuestosService: ImpuestosService
  ) {
    this.impuesto = new Impuesto();
    this.articulos = [];
    this.articulosAux = [];
  }

  ngOnInit() {
    this.getIva();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  columnsArticulos = [
    "select",
    "descripcion",
    "precio_de_venta",
    "precio_de_compra",
    "stock",
    "unidades",
    "subtotal"
  ];

  dataSource = new MatTableDataSource<Articulo>();
  selection = new SelectionModel(allowMultiSelect, initialSelection);

  rowsArticulos: Articulo[];

  /**
   * Carga los articulos cuando se desean agregar a la factura
   */
  LoadAllArticulos(esVenta: boolean, idEmpresa = null, filterByProveedor?) {

    this.isLoading = true;
    this.articulos = [];
    this.rowsArticulos = [];
    this.selection.clear();
    this.dataSource.paginator.firstPage();
    this.dataSource.data = [];
    this.show();

    this.articulosService.getAllArticulos().subscribe(
      data => {
        let articulos = [];

        if (filterByProveedor) {
          articulos = data.filter(a => {
            return a.proveedor.rut == filterByProveedor;
          });
        }
        else {
          articulos = idEmpresa != null ? data.filter(a => a.proveedor.id_empresa == idEmpresa) : data;
        }

        this.articulos = articulos;
        this.selection.deselect();
        
        this.isLoading = false;
        this.dataSource.data = articulos;

        articulos.map(articulo => {
          //Setea todo los parametros para mostrarlos calculados al inicio del modal.
          articulo["unidades"] = 1;
          if (esVenta) {
            if (articulo.stock == 0) {
              articulo["subtotal"] = 0;
            } else {
              articulo["subtotal"] =
                parseInt(articulo["unidades"]) *
                parseInt(articulo["precio_de_venta"]);
            }

            articulo["esVenta"] = esVenta;
          } else {
            articulo["subtotal"] =
              parseInt(articulo["unidades"]) *
              parseInt(articulo["precio_de_compra"]);
            articulo["esVenta"] = esVenta;
          }
          articulo["iva"] =
            parseInt(articulo["subtotal"]) * this.impuesto.valor;
          articulo["total"] =
            parseInt(articulo["subtotal"]) + parseInt(articulo["iva"]);
        });
      },
      err => {
        swal({
          title: "Ups! Tenemos un problema",
          text: err.errors || err.error,
          type: "error"
        });
      }
    );
  }

  getIva() {
    this.impuestosService.getImpuestoByNombre("iva").subscribe(
      data => {
        this.impuesto = data;
      },
      error => {
        swal({
          title: "Ups! Tenemos un problema",
          text: "No se pudo cargar el IVA",
          type: "error"
        });
      }
    );
  }

  sendArticulos() {
    if (this.selection.selected.length > 0) {

      if (this.isPresupuesto)
      {
        let toPresupuesto = [];
        
        this.selection.selected.forEach(s => {
          toPresupuesto.push(
            {
              codigo_articulo: s.codigo,
              descripcion_articulo: s.descripcion,
              iva: s.iva,
              precio: s.precio_de_venta,
              subtotal: s.subtotal,
              unidades: s.unidades,
              id_articulo: s.id
            }
          );
        });
        console.log(toPresupuesto);
        this.send.emit(toPresupuesto);
      }
      else 
      {
        this.send.emit(this.selection.selected);
      }

      this.hide();
    } else {
      swal({
        title: "Lo sentimos.",
        text: "Debe seleccionar al menos 1 artículo.",
        type: "info"
      });
    }
  }

  public show(): void {
    this.visible = true;
    setTimeout(() => (this.visibleAnimate = true), 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => (this.visible = false), 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }
  calculaSubtotal(row: Articulo) {

    for (let i = 0; i < this.rowsArticulos.length; i++) {
      if (this.rowsArticulos[i].id == row.id) {
        if (row.esVenta) {
          if (row.unidades > row.stock) {
            swal({
              title: "Ups! Tenemos un problema",
              text: `El stock no es suficiente para este articulo. Cantidad máxima permitida es ${
                row.stock
                } unidades.`,
              type: "error"
            });
            row.unidades = row.stock;
          }
          row.subtotal = row.precio_de_venta * row.unidades;
        } else {
          row.subtotal = row.precio_de_compra * row.unidades;
        }

        row.iva = row.subtotal * this.impuesto.valor;
        row.total = row.subtotal + row.iva;

        this.rowsArticulos[i] = row;
      }
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  changePrecioVenta(value, row: Articulo)
  {
    row.precio_de_venta = value;
    row.subtotal = row.unidades * row.precio_de_venta; 
    let indice = this.dataSource.data.findIndex(d => d.id == row.id);
    this.dataSource.data.splice(indice, 1, row);
  }
}
