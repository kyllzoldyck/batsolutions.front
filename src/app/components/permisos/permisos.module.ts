import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermisosRoutingModule } from './permisos-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PermisosComponent } from './permisos.component';
import { AccessesService } from '../../services/accesses.service';
import { EmpresaService } from '../../services/empresas.service';
import { RolesService } from '../../services/roles.service';
import { MatTableModule, MatPaginatorModule, MatSelectModule, MatCheckboxModule, MatProgressSpinnerModule, MatPaginatorIntl } from '@angular/material';
import { SharedModule } from '../../shared/shared.module';
import { getSpanishPaginatorIntl } from '../../shared/spanish-datatable-intl';
import { MenuItems } from '../../shared/menu-items/menu-items';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PermisosRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    SharedModule
  ],
  declarations: [
    PermisosComponent
  ],
  providers: [
    AccessesService,
    EmpresaService,
    RolesService,
    { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() },
    MenuItems
  ]
})
export class PermisosModule { }
