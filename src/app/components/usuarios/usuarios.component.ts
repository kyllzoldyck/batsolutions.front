import { Component, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from '../../models/usuario';
import { Empresa } from '../../models/empresa';
import { isValidEmail } from '../../shared/email.validation';
import { Rol } from '../../models/rol';
import { RolesService } from '../../services/roles.service';
import { UsuariosService } from '../../services/usuarios.service';
import { EmpresaService } from '../../services/empresas.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ToastData, ToastOptions, ToastyService } from 'ng2-toasty';
import swal from 'sweetalert2';
import { getErrorsForm } from '../../shared/utils';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ModalBasicComponent } from '../../shared/modal-basic/modal-basic.component';

@Component({
    moduleId: module.id,
    selector: 'usuarios',
    templateUrl: 'usuarios.component.html',
    providers: [DatePipe]
})

export class UsuariosComponent {

    usuario: Usuario;
    isLoadingUsuarios: boolean = true;
    showFormState: boolean = false;
    isUpdate: boolean = false;
    roles: Rol[];
    empresas: Empresa[];
    dataSourceUsuarios = new MatTableDataSource<Usuario>();
    columnsUsuarios = ['username', 'nombre', 'apellidos', 'created_at', 'rol', 'name_activo', 'empresa', 'acciones'];

    @ViewChild('buttonSubmit') buttonSubmit: ElementRef;
    @ViewChild('paginatorUsuarios') paginatorUsuarios: MatPaginator;
    @ViewChild('modal') modal: ModalBasicComponent;

    formErrors = {
        'username': '',
        'nombre': '',
        'apellidos': '',
        'email': '',
        'password': '',
        'id_rol': '',
        'activo': '',
        'id_empresa': ''
    };

    validationMessages = {
        'username': {
            'required': 'Debe ingresar un Nombre de Usuario.',
            'maxlength': 'El Nombre de Usuario no debe superar los 10 caracteres.',
            'minlength': 'El Nombre de Usuario debe tener al menos 3 caracteres.'
        },
        'nombre': {
            'required': 'Debe ingresar un Nombre',
            'maxlength': 'El Nombre no debe superar los 100 caracteres.',
            'minlength': 'El Nombre debe tener como mínimo 3 caracteres.'
        },
        'apellidos': {
            'required': 'Debe ingresar los Apellidos',
            'maxlength': 'Los Apellidos no deben superar los 150 caracteres.',
            'minlength': 'Los Apellidos deben tener como mínimo 3 caracteres.'
        },
        'email': {
            'required': 'Debe ingresar un Email',
            'maxlength': 'El Email no debe superar los 50 caracteres.',
            'minlength': 'El Email debe tener como mínimo 3 caracteres.',
            'isValidEmail': 'Debe ingresar un Email válido.'
        },
        'password': {
            'required': 'Debe ingresar una contraseña',
            'maxlength': 'La contraseña no debe superar los 100 caracteres.',
            'minlength': 'La contraseña debe tener un minimo de 5 caracteres.'
        },
        'id_rol': {
            'required': 'Debe seleccionar un rol'
        },
        'activo': {
            'required': 'Debe seleccionar si esta activado o desactivado.'
        },
        'id_empresa': {
            'required': 'La empresa es obligatoria.'
        }
    };

    usuariosForm: FormGroup;

    constructor(public fb: FormBuilder, public usuariosService: UsuariosService, public datePipe: DatePipe, public rolesService: RolesService, public router: Router, public empresaService: EmpresaService, public toastyService: ToastyService) {
        this.usuario = new Usuario()
    }

    ngOnInit() {
        this.buildForm();
        this.LoadAllUsuarios();
        this.getRoles();
        this.getEmpresas();
    }

    ngAfterViewInit(): void {
        this.dataSourceUsuarios.paginator = this.paginatorUsuarios;
    }

    /**
    * Construye el formulario y sus validaciones
    */
    buildForm() {

        let password = [this.usuario.password, Validators.compose([
            Validators.required,
            Validators.maxLength(100),
            Validators.minLength(5)
        ])];

        if (this.isUpdate) {
            password = [];
        }

        this.usuariosForm = this.fb.group({
            username: [this.usuario.username, Validators.compose([
                Validators.required,
                Validators.maxLength(10),
                Validators.minLength(3)
            ])],
            nombre: [this.usuario.nombre, Validators.compose([
                Validators.required,
                Validators.maxLength(100),
                Validators.minLength(3)
            ])],
            apellidos: [this.usuario.apellidos, Validators.compose([
                Validators.required,
                Validators.maxLength(150),
                Validators.minLength(3)
            ])],
            email: [this.usuario.email, Validators.compose([
                Validators.required,
                Validators.maxLength(50),
                Validators.minLength(3),
                isValidEmail()
            ])],
            id_rol: [this.usuario.id_rol, Validators.compose([
                Validators.required
            ])],
            activo: [this.usuario.activo, Validators.compose([
                Validators.required
            ])],
            password: password,
            id_empresa: [this.usuario.id_empresa, Validators.compose([
                Validators.required
            ])]
        });

        // Cada vez que hay un cambio en el form llama a onValueChanged
        this.usuariosForm.valueChanges.subscribe(data => {
            this.onValueChanged(data);
        });

        this.onValueChanged();
    }

    // Cada vez que los valores cambian en el Form, se validan.
    onValueChanged(data?: any) {

        if (!this.usuariosForm) {
            return;
        }

        const form = this.usuariosForm;
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    LoadAllUsuarios() {

        this.dataSourceUsuarios.data = [];

        this.usuariosService.getAllUsuarios().subscribe(
            data => {
                setTimeout(() => 
                {
                    this.isLoadingUsuarios = false;
                    this.dataSourceUsuarios.data = data;
                }, 500);
            },
            err => {
                swal({
                    title: 'Ups! Tenemos un problema',
                    text: err.error,
                    type: 'error'
                }).then(() => {
                    this.cleanUsuariosForm()
                })
            }
        )
    }
    blockAgents(value) { console.log('blockAgents', value) }
    approveAgent(value) { console.log('approveAgent', value) }
    /**
    * Limpia el formulario y carga nuevamente los usuarios.
    */
    cleanUsuariosForm() {
        this.isLoadingUsuarios = true;
        this.isUpdate = false;
        this.LoadAllUsuarios();
        this.usuariosForm.reset();
        this.hide();
    }

    /**
    * Se encarga de mostrar/esconder el formulario.
    */
    show() {
        this.modal.show();
        if (this.isUpdate)
            this.cleanForm();
    }

    hide() {
        this.modal.hide();
    }

    /**
    * Se encarga de llenar el formulario con los datos para actualizar el usuario.
    * @param id Usuario
    */
    fillFormForEditUsuario(id) {

        let usuario = this.dataSourceUsuarios.data.find(r => r.id == id);

        this.usuario.id = usuario.id;
        this.usuario.username = usuario.username;
        this.usuario.nombre = usuario.nombre;
        this.usuario.apellidos = usuario.apellidos;
        this.usuario.email = usuario.email;
        this.usuario.activo = usuario.activo;
        this.usuario.id_rol = usuario.id_rol;
        this.usuario.id_empresa = usuario.id_empresa;

        this.modal.show();
        this.isUpdate = true;
        this.buildForm();
    }

    /**
    * Cuando se envia el formulario.
    */
    onSubmit() {
        if (!this.isUpdate)
            this.addUsuario();
        else
            this.updateUsuario();
    }

    /**
    * Conecta con el servicio y envia el usuario para su actualizacion
    */
    addUsuario() {
        this.buttonSubmit.nativeElement.disabled = true;

        this.usuariosService.addUsuario(this.usuario).subscribe(
            res => {
                this.buttonSubmit.nativeElement.disabled = false;

                swal({
                    title: '¡Usuario Creado!',
                    text: 'El usuario ha sido creado correctamente',
                    type: 'success'
                }).then(() => this.cleanUsuariosForm());
            },
            err => {
                this.buttonSubmit.nativeElement.disabled = false;
                swal({
                    title: 'Ups! Tenemos un problema',
                    html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
                    type: err.status == 422 ? 'warning' : 'error'
                });//.then(() => this.cleanUsuariosForm());
            }
        )
    }

    /**
    * Conecta con el servicio y envia el usuario para su actualizacion
    */
    updateUsuario() {
        swal({
            title: `Se actualizará el usuario ` + this.usuario.username,
            text: "¿Estás seguro de actualizar?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Actualizar'
        }).then(result => {

            if (result.value)
            {
                this.buttonSubmit.nativeElement.disabled = true;

                this.usuariosService.updateUsuario(this.usuario).subscribe(
                    res => {
                        this.buttonSubmit.nativeElement.disabled = false;

                        swal(
                            'Usuario Actualizado!',
                            'El usuario ha sido actualizado correctamente!',
                            'success'
                        ).then(() => this.cleanUsuariosForm());
                    },
                    err => {
                        this.buttonSubmit.nativeElement.disabled = false;
                        
                        swal({
                            title: 'Ups! Tenemos un problema.',
                            html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error, 
                            type: err.status == 422 ? 'warning' : 'error'
                        });//.then(() => this.cleanUsuariosForm());
                    }
                );
            }
        });
    }

    /**
    * Conecta con el servicio para eliminar un usuario enviando el id
    * @param id Id Usuario
    */
    deleteUsuario(id) {

        let usuario = this.dataSourceUsuarios.data.find(r => r.id = id);

        swal({
            title: `Se eliminará el usuario: ${usuario.username}`,
            text: "¿Estás seguro de eliminar?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar'
        })
        .then(result => {
            if (result.value)
            {
                this.usuariosService.deleteUsuario(usuario.id).subscribe(
                    res => {
                        swal(
                            '¡Usuario Eliminado!',
                            'El usuario ha sido eliminado correctamente!',
                            'success'
                        ).then(() => this.cleanUsuariosForm());
                    },
                    err => {
                        swal({
                            title: 'Ups! Tenemos un problema.',
                            text: err.error || getErrorsForm(err.errors),
                            type: 'error'
                        }).then(() => this.cleanUsuariosForm());
                    }
                );
            }
        });
    }

    getRoles() {
        this.rolesService.getAllRoles().subscribe(
            data => {
                this.roles = data;
            },
            err => {
                swal(
                    'Ups! Tenemos un problema',
                    'Lo sentimos, no se han podido cargar los roles.',
                    'error'
                ).then(() => this.router.navigate(['/home']));
            }
        );
    }

    getEmpresas() {
        this.empresaService.getAllEmpresas().subscribe(
            data => {
                this.empresas = data;
            },
            err => {
                swal(
                    'Ups! Tenemos un problema.',
                    'Lo sentimos, no se han podido cargar las empresas',
                    'error'
                ).then(() => this.router.navigate(['/home']));
            }
        );
    }

    onChangeRol(rol: number) {
        this.usuario.id_rol = rol;
    }

    onChangeActivo(activo: number) {
        this.usuario.activo = activo;
    }

    onChangeEmpresa(empresa: number) {
        this.usuario.id_empresa = empresa;
    }

    /**
    * Se gatilla al momento de limpiar el form
    */
    cleanForm() {
        this.isUpdate = false;
        this.usuariosForm.reset()
    }
}
