import { Component, Input, OnInit, ElementRef } from '@angular/core';
import { Factura } from '../../models/factura';
import { Usuario } from '../../models/usuario';
import { Cliente } from '../../models/cliente';
import { Empresa } from '../../models/empresa';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-template-reporte-facturas-cliente',
  templateUrl: './reporte.facturas.cliente.html',
  styleUrls: ['./reporte.facturas.cliente.css']
})
export class ReporteFacturasClientePDFComponent implements OnInit {

  public date: number;
  public dataColumnsFacturasCliente = ['numero', 'fecha', 'cliente', 'rut', 'total', 'nula'];

  @Input('fechaDesde') fechaDesde: Date;
  @Input('fechaHasta') fechaHasta: Date;
  @Input('facturas') dataSourceFacturasCliente = new MatTableDataSource<Factura>();
  @Input('usuario') usuario: Usuario;
  @Input('cliente') cliente: Cliente;
  @Input('empresa') empresa: Empresa;

  constructor() {
    this.date = Date.now();
  }

  ngOnInit() {

  }
}
