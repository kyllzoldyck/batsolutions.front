import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Access } from "../models/access";
import { Observable } from "rxjs/Observable";
import { getHeaders } from "../shared/utils";

const httpOptions = {
  headers: new HttpHeaders({})
};

@Injectable()
export class AccessesService {

  constructor(private http: HttpClient) {}

  getAll(): Observable<Access[]> {
    let URI = environment.URI;

    httpOptions.headers.set("Accept", "application/json");

    return this.http.get<Access[]>(
      URI + "print_menus",
      { headers: getHeaders() }
    );
  }

  updateAccess(id_rol: number, id_empresa: number, id_modulo: number, state: number): Observable<any> {
    let URI = environment.URI;

    let body: any = {
      id_rol: id_rol,
      id_entity: id_empresa,
      id_print_menu: id_modulo,
      state: state
    };

    return this.http.post(URI + "accesses", JSON.stringify(body), { headers: getHeaders() });
  }
}
