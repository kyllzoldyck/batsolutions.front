import { Component, OnInit, Input } from '@angular/core';
import { Empresa } from '../../models/empresa';
import { Articulo } from '../../models/articulo';
import { Usuario } from '../../models/usuario';

@Component({
  selector: 'app-template-reporte-articulos',
  templateUrl: './reporte.articulos.html',
  styleUrls: ['./reporte.articulos.css']
})
export class ReporteArticulosPDFComponent implements OnInit {

  @Input('empresa') empresa: Empresa;
  @Input('articulos') articulos: Articulo[];
  @Input('usuario') usuario: Usuario;

  public date: number;

  constructor() {
    this.date = Date.now();
  }

  ngOnInit() { }
}
