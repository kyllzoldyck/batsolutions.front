import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UsuariosComponent } from "./usuarios.component";

const routes: Routes = [
    {
        path: '',
        component: UsuariosComponent,
        data: {
            breadcrumb: 'Usuarios',
            icon: '<i class="fa fa-users" aria-hidden="true"></i>',
            status: false
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsuariosRoutingModule { }