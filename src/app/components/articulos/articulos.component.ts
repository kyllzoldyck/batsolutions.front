import { Component, ElementRef, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Articulo } from '../../models/articulo';
import { ArticulosService } from '../../services/articulos.service';
import { ProveedoresService } from '../../services/proveedores.service';
import { Proveedor } from '../../models/proveedor';
import { DecimalsPipe } from './../../shared/pipes/decimals.pipe';
import { AuthService } from '../../services/auth.service';
import { Usuario } from '../../models/usuario';
import { getErrorsForm } from '../../shared/utils';
import swal from "sweetalert2";
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ModalBasicComponent } from '../../shared/modal-basic/modal-basic.component';

@Component({
  moduleId: module.id,
  providers: [DecimalsPipe],
  selector: 'articulos',
  templateUrl: 'articulos.component.html',
})

export class ArticulosComponent implements OnInit, AfterViewInit {

  articulo: Articulo;
  proveedores: Proveedor[];
  isLoading: boolean = true;
  showFormState: boolean = false;
  showFormTitle: boolean = false;
  isUpdate: boolean = false;
  usuario: Usuario;
  columnsArticulos = ['codigo', 'descripcion', 'razon_social', 'precio_de_compra', 'precio_de_venta', 'stock', 'acciones'];
  dataSourceArticulos = new MatTableDataSource<Articulo>();
  havePermission: boolean = false;

  @ViewChild('buttonSubmit') buttonSubmit: ElementRef;
  @ViewChild('paginatorArticulos') paginatorArticulos: MatPaginator;
  @ViewChild('modal') modal: ModalBasicComponent;

  formErrors = {
    'descripcion': '',
    'proveedor': '',
    'precio_de_compra': '',
    'precio_de_venta': '',
    'stock': '',
    'codigo': ''
  };

  validationMessages = {
    'descripcion': {
      'required': 'Debe ingresar una Descripción.',
      'maxlength': 'La Descripción no debe superar los 100 caracteres.',
      'minlength': 'La Descripción debe tener al menos 3 caracteres.'
    },
    'proveedor': {
      'required': 'Debe seleccionar un Proveedor.',
    },
    'precio_de_compra': {
      'required': 'Debe ingresar el Precio de Compra.',
      'pattern': 'Precio de compra debe ser númerico.'
    },
    'precio_de_venta': {
      'required': 'Debe ingresar el Precio de Venta.',
      'pattern': 'Precio de venta debe ser númerico.'
    },
    'stock': {
      'required': 'Debe ingresar el Stock',
      'pattern': 'Stock debe ser númerico.'
    },
    'codigo': {
      'maxlength': 'El código no debe superar los 100 caracteres.'
    }
  };

  articulosForm: FormGroup;

  constructor(public fb: FormBuilder, public authService: AuthService, public articulosService: ArticulosService, public proveedoresService: ProveedoresService) {
    this.proveedores = [];
    this.usuario = new Usuario();
    this.articulo = new Articulo();
  }

  ngOnInit() {
    this.columnsArticulos.pop();

    this.getAuthenticatedUser();
    this.LoadAllArticulos();
    this.LoadAllProveedores();
    this.buildForm();
  }

  ngAfterViewInit() {
    this.dataSourceArticulos.paginator = this.paginatorArticulos;
  }

  /**
   * Construye el formulario y sus validaciones
   */
  buildForm() {

    this.articulosForm = this.fb.group({
      descripcion: [this.articulo.descripcion, Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(3),
      ])],
      proveedor: [this.articulo.id_proveedor, Validators.compose([
        Validators.required
      ])],
      precio_de_compra: [this.articulo.precio_de_compra, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]+$')
      ])],
      precio_de_venta: [this.articulo.precio_de_venta, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]+$')
      ])],
      stock: [this.articulo.stock, Validators.compose([
        Validators.required,
        Validators.pattern('^[0-9]+$')
      ])],
      codigo: [this.articulo.codigo, Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])]
    });
    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.articulosForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.usuario = data;
        if (this.usuario.id_rol != 3) {
          this.columnsArticulos.push('acciones');
          this.havePermission = true;
        }
        this.showFormTitle = true;
      }, err => {
        localStorage.removeItem('bat');
        location.reload();
      });
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.articulosForm) {
      return;
    }

    const form = this.articulosForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  LoadAllArticulos() {
    this.dataSourceArticulos.data = [];
    
    this.articulosService.getAllArticulos().subscribe(
      data => {
        setTimeout(() => {
          this.isLoading = false;
          this.dataSourceArticulos.data = data;
        }, 500)
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          text: err.error,
          type: 'error'
        }).then(() => {
          this.cleanFormAndReload()
        })
      }
    )
  }

  /**
   * Alimenta el DDL de selección de proveedores.
   */
  LoadAllProveedores() {
    this.proveedoresService.getAllProveedores().subscribe(
      data => {
        data.map(prov => {
          this.proveedores.push(new Proveedor(prov));
        });
      },
      err => {
        swal({
          title: 'Ups! Tenemos un problema',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        }).then(() => {
          this.cleanFormAndReload()
        })
      }
    )
  }
  /**
   * Limpia el formulario y carga nuevamente los Articulos.
   */
  cleanFormAndReload() {
    this.isLoading = true;
    this.isUpdate = false;
    this.showFormState = false;
    this.LoadAllArticulos();
    this.articulosForm.reset();
    this.hide();
  }

  /**
   * Se encarga de mostrar/esconder el formulario.
   */
  show() {
    this.modal.show();
    if (this.isUpdate)
      this.cleanForm();
  }

  hide() {
    this.modal.hide();
  }

  /**
   * Se encarga de llenar el formulario con los datos para actualizar el articulos.
   * @param event
   */
  fillFormForEdit(value) {
    let articulo = this.dataSourceArticulos.data.find(r => r.id == value);

    this.articulo.id = articulo.id;
    this.articulo.id_proveedor = articulo.id_proveedor;
    this.articulo.descripcion = articulo.descripcion;
    this.articulo.precio_de_compra = articulo.precio_de_compra;
    this.articulo.precio_de_venta = articulo.precio_de_venta;
    this.articulo.stock = articulo.stock;
    this.articulo.codigo = articulo.codigo;

    this.modal.show();
    this.isUpdate = true;
  }

  /**
   * Cuando se envia el formulario.
   */
  onSubmit() {
    if (!this.isUpdate)
      this.addArticulo();
    else
      this.updateArticulo();
  }

  /**
   * Conecta con el servicio y envia el articulo para su actualizacion
   */
  addArticulo() {
    this.buttonSubmit.nativeElement.disabled = true;

    this.articulosService.addArticulo(this.articulo).subscribe(
      res => {
        this.buttonSubmit.nativeElement.disabled = false;

        swal({
          title: '¡Artículo Creado!',
          text: 'El artículo ha sido creado correctamente',
          type: 'success'
        }).then(() => this.cleanFormAndReload());
      },
      err => {
        this.buttonSubmit.nativeElement.disabled = false;

        swal({
          title: 'Ups! Tenemos un problema',
          html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
          type: err.status == 422 ? 'warning' : 'error'
        });
      }
    )
  }

  /**
   * Conecta con el servicio y envia el articulo para su actualizacion
   */
  updateArticulo() {
    swal({
      title: `Se actualizará el artículo ` + this.articulo.descripcion,
      text: "¿Estás seguro de actualizar?",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Actualizar'
    }).then(result => {
      if (result.value) {
        this.buttonSubmit.nativeElement.disabled = true;

        this.articulosService.updateArticulo(this.articulo).subscribe(
          res => {
            this.buttonSubmit.nativeElement.disabled = false;

            swal(
              '¡Artículo Actualizado!',
              'El artículo ha sido actualizado correctamente!',
              'success'
            ).then(() => this.cleanFormAndReload());
          },
          err => {
            this.buttonSubmit.nativeElement.disabled = false;

            swal({
              title: 'Ups! Tenemos un problema',
              html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
              type: err.status == 422 ? 'warning' : 'error'
            });
          }
        );
      }
    });
  }

  /**
   * Conecta con el servicio para eliminar un articulo enviando el id
   * @param event Id Articulo
   */
  deleteArticulo(value) {
    let articulo = this.dataSourceArticulos.data.find(r => r.id == value);

    swal({
      title: `Se eliminará el artículo: ${articulo.descripcion}`,
      text: "¿Estás seguro de eliminar?",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar'
    }).then(result => {
      if (result.value) {
        this.articulosService.deleteArticulo(articulo.id).subscribe(
          res => {
            swal(
              '¡Artículo Eliminado!',
              'El artículo ha sido eliminado correctamente!',
              'success'
            ).then(() => this.cleanFormAndReload());
          },
          err => {
            swal({
              title: 'Ups! Tenemos un problema',
              text: err.error || 'Intentelo más tarde.',
              type: 'error'
            });
          }
        );
      }
    });
  }

  /**
   * Se gatilla al momento de limpiar el form
   */
  cleanForm() {
    this.isUpdate = false;
    this.articulosForm.reset();
  }

  /**
   * Valida que los los caracteres ingresados sean solo numeros o decimales
   * @param key
   */
  decimalValidator(key) {
    //getting key code of pressed key
    var keycode = (key.which) ? key.which : key.keyCode;

    if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57))
      return false;
    else {
      var parts = key.srcElement.value.split('.');
      if (parts.length > 1 && keycode == 46)
        return false;
      return true;
    }
  }

  // numberValidator(number) {
  //   const value: string = number || '';
  //   const valid = value.match(/^\d{9}$/);
  //   return valid ? false : { ssn: true };
  // }

  onlyNumbers(e) {
    var key = window.event ? e.which : e.keyCode;
    if (key < 48 || key > 57) {
      e.preventDefault();
    }
  }

  getCount() {
  }
}
