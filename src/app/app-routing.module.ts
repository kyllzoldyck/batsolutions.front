import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './layouts/admin/admin.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full',
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'usuarios',
                loadChildren: './components/usuarios/usuarios.module#UsuariosModule'
            },
            {
                path: 'proveedores',
                loadChildren: './components/proveedores/proveedores.module#ProveedorModule'
            },
            {
                path: 'clientes',
                loadChildren: './components/clientes/clientes.module#ClienteModule'
            },
            {
                path: 'articulos',
                loadChildren: './components/articulos/articulos.module#ArticuloModule'
            },
            {
                path: 'facturacion',
                loadChildren: './components/facturacion/facturacion.module#FacturacionModule'
            },
            {
                path: 'reportes',
                loadChildren: './components/reportes/reporte.module#ReporteModule'
            },
            {
                path: 'presupuestos',
                loadChildren: './components/presupuestos/presupuestos.module#PresupuestoModule'
            },
            {
                path: 'permisos',
                loadChildren: './components/permisos/permisos.module#PermisosModule'
            }
        ]
    },
    {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'auth',
                loadChildren: './auth/auth.module#AuthModule'
            }, {
                path: 'maintenance/offline-ui',
                loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
            }
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
