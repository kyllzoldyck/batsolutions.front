import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { ClientesComponent } from "./clientes.component";
import { ClientesService } from "../../services/clientes.service";
import { EmpresaService } from "../../services/empresas.service";
import { ClientesRoutingModule } from "./clientes-routing.module";
import { MatProgressSpinnerModule, MatPaginatorModule, MatTableModule, MatPaginatorIntl } from "@angular/material";
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule,
        ClientesRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule
    ],
    declarations: [ 
        ClientesComponent 
    ],
    providers: [ 
        ClientesService, 
        EmpresaService ,  
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class ClienteModule { }