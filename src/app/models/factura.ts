import { Articulo } from "../models/articulo";
import { DetalleFactura } from "../models/detalleFactura";
import { Empresa } from "./empresa";

export class Factura {

    public id: number;
    public descripcion: string;
    public email: string;
    public nula: number;
    public eliminado: number;
    public fecha: string;
    public fecha_de_creacion: string;
    public iva: number;
    public numero: number;
    public articulos = [];
    public detalles_factura = [];
    public tipo_factura: number;
    public razon_social: string;
    public direccion: string;
    public rut: string;
    public telefono: number;
    public nombre_contacto: string;
    public id_empresa: number;
    public sub_total: number;
    public total: number;
    public name_empresa: string;
    public name_nula: string;
    public empresa: Empresa;
    public exenta: number;
    public giro: string;

    public constructor( data: any = {})
    {
        this.id = data.Id || null;
        this.descripcion = data.descripcion || null;
        this.email = data.email || null;
        this.nula = data.nula || null;
        this.eliminado = data.eliminado || null;
        this.fecha = data.fecha || null;
        this.fecha_de_creacion = data.fecha_de_creacion || null;
        this.iva = data.iva || null;
        this.numero = data.numero || null;
        this.articulos = data.articulos || null;
        this.detalles_factura = data.detalles_factura || null;
        this.tipo_factura = data.tipo_factura || null;
        this.razon_social = data.razon_social || null;
        this.direccion = data.direccion || null;
        this.rut = data.rut || null;
        this.telefono = data.telefono || null;
        this.nombre_contacto = data.nombre_contacto || null;
        this.id_empresa = data.id_empresa || null;
        this.sub_total = data.sub_total || null;
        this.total = data.total || null;
        this.name_empresa = data.name_empresa || null;
        this.name_nula = data.name_nula || null;
        this.empresa = data.empresa || null;
        this.exenta = data.exenta || 0;
        this.giro = data.giro || null;
    }
}
