import { Component, Input, OnInit, ElementRef } from '@angular/core';
import { Factura } from '../../models/factura';
import { Usuario } from '../../models/usuario';
import { Empresa } from '../../models/empresa';
import { Proveedor } from '../../models/proveedor';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-template-reporte-facturas-proveedor',
  templateUrl: './reporte.facturas.proveedor.html',
  styleUrls: ['./reporte.facturas.proveedor.css']
})
export class ReporteFacturasProveedorPDFComponent implements OnInit {

  public date: number;
  public dataColumnsFacturasProveedor = ['numero', 'fecha', 'proveedor', 'rut', 'total', 'nula'];

  @Input('fechaDesde') fechaDesde: Date;
  @Input('fechaHasta') fechaHasta: Date;
  @Input('facturas') dataSourceFacturasProveedor = new MatTableDataSource<Factura>();
  @Input('usuario') usuario: Usuario;
  @Input('proveedor') proveedor: Proveedor;
  @Input('empresa') empresa: Empresa;

  constructor() {
    this.date = Date.now();
  }

  ngOnInit() {

  }
}
