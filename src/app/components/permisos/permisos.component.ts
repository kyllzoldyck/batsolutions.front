import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { AccessesService } from "../../services/accesses.service";
import { MatTable, MatPaginator, MatTableDataSource } from "@angular/material";
import { AuthService } from "../../services/auth.service";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Access } from "../../models/access";
import { Empresa } from "../../models/empresa";
import { EmpresaService } from "../../services/empresas.service";
import { Rol } from "../../models/rol";
import { SelectionModel } from "@angular/cdk/collections";
import swal from "sweetalert2";
import { RolesService } from "../../services/roles.service";
import { getErrorsForm } from "../../shared/utils";
import { MenuItems } from "../../shared/menu-items/menu-items";

const initialSelection = [];
const allowMultiSelect = true;
const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

@Component({
  selector: "app-permisos",
  templateUrl: "./permisos.component.html",
  styleUrls: ["./permisos.component.css"]
})
export class PermisosComponent implements OnInit, AfterViewInit {

  permisos: Access;
  empresas: Empresa[];
  roles: Rol[];
  rol: number;
  empresa: number;
  isLoading: boolean = true;

  @ViewChild("tablePermisos")
  tablePermisos: MatTable<any>;

  @ViewChild("paginator")
  paginator: MatPaginator;

  dataColumnsPermisos = ["modulo", "select"];
  dataSourcePermisos = new MatTableDataSource<Access>();
  selection = new SelectionModel(allowMultiSelect, initialSelection);

  permisosForm: FormGroup;

  formErrors = {
    'empresa': '',
    'rol': ''
  };

  validationMessages = {
    'descripcion': {
      'required': 'Debe ingresar una Descripción.'
    },
    'proveedor': {
      'required': 'Debe seleccionar un Proveedor.'
    }
  };

  constructor(private accessesService: AccessesService, private empresaService: EmpresaService, private rolesService: RolesService, private authService: AuthService, private fb: FormBuilder, private menuService: MenuItems) {
    this.empresas = [];
    this.roles = [];
    this.empresa = 0;
    this.rol = 0;
  }

  ngOnInit() {
    this.buildForm();
    this.getModules();
    this.getEmpresas();
    this.getRoles();
  }

  ngAfterViewInit() {
    this.dataSourcePermisos.paginator = this.paginator;
  }

  /**
   * Construye el formulario y sus validaciones
   */
  buildForm() {

    this.permisosForm = this.fb.group({
      empresa: [this.empresa, Validators.compose([
        Validators.required,
      ])],
      rol: [this.rol, Validators.compose([
        Validators.required
      ])]
    });
    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.permisosForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.permisosForm) {
      return;
    }

    const form = this.permisosForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser().subscribe(
      data => { },
      err => {
        localStorage.removeItem("bat");
        location.reload();
      }
    );
  }

  getModules() {
    this.dataSourcePermisos.data = [];

    this.accessesService.getAll().subscribe(
      data => {
        console.log(data);
        this.dataSourcePermisos.data = data;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        swal({
          title: '¡Ups! Tenemos un problema.',
          text: error.error,
          type: 'error'
        });
      }
    );
  }

  getEmpresas() {
    this.empresaService.getAllEmpresas().subscribe(
      data => {
        this.empresas = data;
      },
      error => {
        swal(
          "Ups! Tenemos un problema.",
          "Lo sentimos, no se han podido cargar las empresas",
          "error"
        );
      }
    );
  }

  getRoles() {
    this.rolesService.getAllRoles().subscribe(
      data => {
        this.roles = data;
      },
      error => {
        swal(
          "Ups! Tenemos un problema.",
          "Lo sentimos, no se han podido cargar los roles.",
          "error"
        );
      }
    );
  }

  updateAccess(row: Access) {

    let state = !this.selection.isSelected(row) ? 1 : 0;

    this.accessesService.updateAccess(this.rol, this.empresa, row.id, state).subscribe(
      data => {
        console.log(data);
        if (data.created) {
          toast({
            type: 'success',
            title: 'Permiso actualizado.'
          });
        }
      },
      error => {
        this.selection.toggle(row);
        swal({
          title: 'Ups! Tenemos un problema',
          html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error,
          type: error.status == 422 ? 'warning' : 'error'
        });
      }
    );
  }

  loadAccesses() {
    if (this.rol > 0 && this.empresa > 0) {

      let permisos = this.dataSourcePermisos.data;
      this.dataSourcePermisos.data = [];
      this.isLoading = true;
      this.dataSourcePermisos.paginator.firstPage();

      this.menuService.getAllWithRolAndEntity(this.rol, this.empresa).subscribe(
        data => {
          this.selection.clear();
          this.isLoading = false;
          
          this.dataSourcePermisos.data = permisos;
          this.dataSourcePermisos.data.forEach(originAccess => {
            data.forEach(newAccess => {
              if (originAccess.id == newAccess.id_print_menu) {
                this.selection.select(originAccess);
              }
            });
          });
        },
        error => {
          this.dataSourcePermisos.data = permisos;
          this.isLoading = false;

          swal({
            title: 'Ups! Tenemos un problema',
            html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error,
            type: error.status == 422 || error.status == 204 ? 'warning' : 'error'
          });
        }
      );
    }
    else {
      this.selection.clear();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourcePermisos.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSourcePermisos.data.forEach(row => this.selection.select(row));
  }
}
