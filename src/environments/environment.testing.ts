export const environment = {
  production: true,
  URI: 'https://testadminapi2.batsolutions.cl/api/',
  PATH_DOCUMENTS_PDF: '../../documents/',
};
