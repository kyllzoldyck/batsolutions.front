import { DetallePresupuesto } from "./detallePresupuesto";

export class Presupuesto {

    public id: number;
    public cliente_direccion: string;
    public cliente_email: string;
    public cliente_nombre_contacto: string;
    public cliente_razon_social: string;
    public cliente_rut: string;
    public cliente_telefono: string;
    public fecha_expiracion: string;
    public nombre: string;
    public email: string;
    public asunto: string;
    public descripcion: string;
    public id_empresa: number;
    public id_cliente: number;
    public eliminado: number;
    public detalles_presupuesto: DetallePresupuesto[];
    public created_at: string;
    public updated_at: string;
    public total: number;
    public sub_total: number;
    public iva: number;
    public numero: number;
    public exenta: number;

    constructor(data: any = {})
    {
        this.id = data.id || null;
        this.cliente_direccion = data.cliente_direccion || null;
        this.cliente_email = data.cliente_email || null;
        this.cliente_nombre_contacto = data.cliente_nombre_contacto || null;
        this.cliente_razon_social = data.cliente_razon_social || null;
        this.cliente_rut = data.cliente_rut || null;
        this.cliente_telefono = data.cliente_telefono || null;
        this.fecha_expiracion = data.fecha_expiracion || null;
        this.nombre = data.nombre || null;
        this.email = data.email || null;
        this.asunto = data.asunto || null;
        this.descripcion = data.descripcion || null;
        this.id_empresa = data.id_empresa || null;
        this.id_cliente = data.id_cliente || null;
        this.eliminado = data.eliminado || null;
        this.detalles_presupuesto = data.detalles_presupuesto || null;
        this.created_at = data.created_at || null;
        this.updated_at = data.updated_at || null;
        this.total = data.total || null;
        this.sub_total = data.sub_total || null;
        this.iva = data.iva || null;
        this.numero = data.numero || null;
        this.exenta = data.exenta || null;
    }

}