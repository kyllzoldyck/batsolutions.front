import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';
import { Usuario } from '../models/usuario';
import { handleError, extractData, getHeaders, getHeadersOauth } from '../shared/utils';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { tokenKey } from '@angular/core/src/view';

@Injectable()
export class AuthService {

  private API_URI = environment.URI;
  isLoggedIn = false;
  usuarioAccess: Usuario;

  constructor(private http: HttpClient, public router: Router) {
    this.usuarioAccess = new Usuario;
  }

  /**
   * Realiza la comprobación de credenciales en el servidor.
   * @param usuario
   */
  login(usuario: Usuario): Observable<any> {

    localStorage.removeItem('bat');

    const URL = this.API_URI + 'authenticate';
    const body = {
      username: usuario.username,
      password: usuario.password
    };

    return this.http.post(URL, JSON.stringify(body), { headers: getHeadersOauth() });
  }

  /**
   * Se encarga de cambiar la contraseña
   * @param usuario
   */
  changePassword(usuario: Usuario): Observable<Response> {
    let URL = this.API_URI + 'password';
    let body = {
      username: usuario.username,
      password: usuario.password,
      password_repeat: usuario.password_repeat
    };

    return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() })
      // .delay(2000)
      .map(extractData)
      .catch(handleError);
  }

  /**
   * Cierra la sesión del usuario, eliminando los datos de navegación.
   */
  logout() {
    localStorage.removeItem('bat');
    this.isLoggedIn = false;
    this.router.navigate(['login']);
  }

  /**
  * Obtiene el usuario autenticado
  */
  getAuthenticatedUser(): Observable<Usuario> {
      let URL = this.API_URI + 'authenticated';
      return this.http.get<Usuario>(URL, { headers: getHeaders() });
  }

  resetPassword(email: string): Observable<any> {
    let URL = this.API_URI + 'forgot/password';

    let body = {
      email: email
    };

    return this.http.post(URL, body, { headers: getHeadersOauth() });
  }

  newPassword(token: string, password: string, passwordRepeat: string) {
    let URL = this.API_URI + 'password/reset';
    let body = {
      token: token,
      password: password,
      password_confirmation: passwordRepeat
    };

    console.log(body);
    return this.http.post(URL, body, { headers: getHeadersOauth() });
  }
}
