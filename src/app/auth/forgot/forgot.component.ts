import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { isValidEmail } from '../../shared/email.validation';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  public email: string;
  public showForm: boolean = true;
  public isLoading: boolean = false;

  @ViewChild('resetPasswordButton') resetPasswordButton: ElementRef;

  public resetPasswordForm: FormGroup;

  formErrors = {
    'email': '',
  };

  validationMessages = {
    'email': {
      'required': 'Debe ingresar un email.',
      'maxlength': 'El email supera el máximo de caracteres.',
      'isValidEmail': 'No es un email válido.'
    }
  };

  constructor(private authService: AuthService, private fb: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.resetPasswordForm = this.fb.group({
      email: [
        this.email, Validators.compose([
          Validators.required,
          Validators.maxLength(100),
          isValidEmail()
        ])
      ]
    });

    this.resetPasswordForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.resetPasswordForm) {
      return;
    }

    const form = this.resetPasswordForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  resetPassword() {
    this.resetPasswordButton.nativeElement.disabled = true;
    this.isLoading = true;

    this.authService.resetPassword(this.email).subscribe(data => {
      this.isLoading = false;
      this.showForm = false;
      swal('¡Muy Bien!', 'Se le ha enviado un correo a ' + this.email + ' para restablecer su contraseña.', 'success');
    },
    error => {
      if (error.status === 400)
      {
        swal('¡Lo sentimos!', 'E-Mail no está asociado a ninguna cuenta.');
        this.isLoading = false;
      }
      else 
      {
        swal('¡Lo sentimos!', 'No se ha podido enviar el correo a ' + this.email + '. Puede que el usuario no exista, si el problema persiste contáctese con el administrador del sistema.', 'error');
        this.isLoading = false;
        this.resetPasswordButton.nativeElement.disabled = false;
      } 
    });
  }

}
