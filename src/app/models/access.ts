export class Access {

    public id: number;
    public id_menu: number;
    public id_sub_menu: number;
    public created_at: string;
    public updated_at: string;
    public state_access: number;
    public state_menu: number;
    public icon: string;
    public uri: string;
    public name_menu: string;
    public name_entity: string;
    public children: Access;
    public id_print_menu: number;

    constructor (data: any = {}) 
    {
        this.id = data.id || null;
        this.id_menu = data.id_menu || null;
        this.id_sub_menu = data.id_sub_menu || null;
        this.created_at = data.created_at || null;
        this.updated_at = data.updated_at || null;
        this.state_access = data.state_access || null;
        this.state_menu = data.state_menu || null;
        this.icon = data.icon || null;
        this.uri = data.uri || null;
        this.name_menu = data.name_menu || null;
        this.name_entity = data.name_entity || null;
        this.children = data.children || null;
        this.id_print_menu = data.id_print_menu || null;
    }

}