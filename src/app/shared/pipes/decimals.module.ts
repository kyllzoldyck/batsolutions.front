import { NgModule } from "@angular/core";
import { DecimalsPipe } from "./decimals.pipe";

@NgModule({
    declarations: [
        DecimalsPipe
    ],
    exports: [
        DecimalsPipe
    ]
})
export class DecimalsPipeModule { }