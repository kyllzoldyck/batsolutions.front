import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpresaService } from '../../../../services/empresas.service';
import { Empresa } from "../../../../models/empresa";
import { Proveedor } from '../../../../models/proveedor';
import { ProveedoresService } from '../../../../services/proveedores.service';
import { AuthService } from '../../../../services/auth.service';
import { Usuario } from "../../../../models/usuario";
import { Factura } from '../../../../models/factura';
import { FacturasService } from '../../../../services/facturas.service';
import { ReportesService } from '../../../../services/reportes.service';
import { ExcelService } from '../../../../services/excel.service';
import { ModalDetalleFacturaComponent } from '../../../../shared/reportes/modal.detalle.factura';
import * as $ from 'jquery';
import { ReporteFacturasProveedorPDFComponent } from '../../../../shared/reportes/reporte.facturas.proveedor';
import { getErrorsForm } from '../../../../shared/utils';
import { IOption } from 'ng-select';
import swal from "sweetalert2";
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ModalBasicComponent } from '../../../../shared/modal-basic/modal-basic.component';
import { saveAs } from 'file-saver/FileSaver';

declare var xepOnline: any;

const initialSelection = [];
const allowMultiSelect = true;

@Component({
  selector: "app-facturas-proveedores",
  templateUrl: "./facturas_proveedores.component.html",
  styleUrls: ["./facturas_proveedores.component.css"]
})
export class FacturasProveedoresComponent implements OnInit, AfterViewInit {

  public proveedoresOptions: Array<IOption> = new Array<IOption>();
  public empresasOptions: Array<IOption> = new Array<IOption>();
  public empresas: Empresa[];
  public proveedores: Proveedor[];
  public facturas: Factura[];
  public facturasAux: Factura[];
  public usuario: Usuario;
  public proveedorAux: Proveedor;
  public factura: Factura;
  public empresa: Empresa;
  public proveedor: Proveedor;

  public idEmpresa: number;
  public rutProveedor: string;
  public fechaDesde: any;
  public fechaHasta: any;

  public dataSourceFacturasProveedor = new MatTableDataSource<Factura>();
  public dataColumnsFacturasProveedor = ['select', 'numero', 'fecha', 'proveedor', 'total', 'nula', 'acciones'];
  public selection = new SelectionModel(allowMultiSelect, initialSelection);
  public selected = [];

  public isLoading: boolean = false;
  public facturasChecked: any[];
  public showAlertNotFound = false;
  public loadingProveedores: boolean = true;

  @ViewChild('modalFacturasProveedor') modalFacturasProveedor: ModalBasicComponent;
  @ViewChild('btnExportExcel') btnExportExcel: ElementRef;
  @ViewChild('btnExportPdf') btnExportPdf: ElementRef;
  @ViewChild("empresa") empresaElement: ElementRef;
  @ViewChild(ModalDetalleFacturaComponent)

  private modalDetalleFactura: ModalDetalleFacturaComponent;
  @ViewChild(ReporteFacturasProveedorPDFComponent)
  private reporteFacturasProveedorPDFComponent: ReporteFacturasProveedorPDFComponent;

  constructor(
    public fb: FormBuilder,
    private reportesService: ReportesService,
    private empresasService: EmpresaService,
    private proveedoresService: ProveedoresService,
    private authService: AuthService,
    private facturasService: FacturasService,
    private excelService: ExcelService,
    private cdr: ChangeDetectorRef,
    private datePipe: DatePipe
  ) {
    this.empresas = [];
    this.proveedores = [];
    this.facturas = [];
    this.facturasAux = [];
    this.facturasChecked = [];
    this.usuario = new Usuario();
    this.proveedorAux = new Proveedor();
    this.factura = new Factura();
    this.empresa = new Empresa();
    this.proveedor = new Proveedor();
  }

  ngOnInit() {
    this.getAuthenticatedUser();
    this.getEmpresas();
    this.buildForm();

    this.btnExportExcel.nativeElement.disabled = true;
    this.btnExportPdf.nativeElement.disabled = true;
  }

  ngAfterViewInit() {
  }

  formErrors = {
    'empresa': '',
    'proveedor': '',
    'fechaDesde': '',
    'fechaHasta': ''
  };

  validationMessages = {
    'empresa': {
      'required': 'Debe seleccionar una empresa',
    },
    'proveedor': {
      'required': 'Debe seleccionar un proveedor.',
    },
    'fechaDesde': {
      'required': 'Debe seleccionar una fecha desde.',
    },
    'fechaHasta': {
      'required': 'Debe seleccionar una fecha hasta.',
    }
  };

  facturasProveedorForm: FormGroup;

  /**
   * Construye el formulario y sus validaciones
   */
  buildForm() {

    let empresa = [
      this.idEmpresa,
      Validators.compose([Validators.required])
    ];

    if (this.usuario.id_rol != 1)
      empresa = [];

    this.facturasProveedorForm = this.fb.group({
      empresa,
      proveedor: [this.rutProveedor, Validators.compose([
        Validators.required,
      ])],
      fechaDesde: [this.fechaDesde, Validators.compose([
        Validators.required,
      ])],
      fechaHasta: [this.fechaHasta, Validators.compose([
        Validators.required,
      ])]
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.facturasProveedorForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.facturasProveedorForm) {
      return;
    }
    console.log(this.facturasProveedorForm);
    const form = this.facturasProveedorForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy');
  }

  getEmpresas() {

    this.empresasOptions = [];
    this.empresasService.getAllEmpresas().subscribe(
      data => {
        this.empresas = data;

        data.forEach(d => {
          let empresaOption: IOption = { value: d.id.toString(), label: d.razon_social, disabled: false };
          this.empresasOptions.push(empresaOption);
        });

        this.loadingProveedores = false;
      },
      err => {
        swal({
          title: "Ups! Ha ocurrido un problema",
          text: err.error || getErrorsForm(err.errors),
          type: "error"
        });
      }
    );
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser().subscribe(
      data => {
        this.usuario = data;
        if (this.usuario.id_rol != 1) {
          this.getProveedores();
          this.empresa = this.usuario.empresa;
        }
      },
      err => {
        localStorage.removeItem('bat');
        location.reload();
      }
    );
  }

  /**
   * Llena el select con los proveedores según la empresa seleccionada
   * @param e ID de la empresa
   */
  getProveedoresByEmpresa() {
    this.proveedoresOptions = [];
    this.empresaElement.nativeElement.disabled = true;

    this.proveedoresService
      .getProveedoresByEmpresa(this.empresaElement.nativeElement.value)
      .subscribe(
        data => {
          this.empresaElement.nativeElement.disabled = false;
          this.proveedores = data;

          let proveedorOption: IOption[] = [];

          data.forEach(p => {
            proveedorOption.push({ value: p.rut, label: p.rut + ' | ' + p.razon_social, disabled: false });
          });

          this.proveedoresOptions = proveedorOption;
        },
        err => {
          swal({
            title: "Ups! Ha ocurrido un problema",
            text: "No se han podido cargar los proveedores.",
            type: "error"
          });
          this.empresaElement.nativeElement.disabled = false;
        }
      );
    this.empresasService.getEmpresa(this.empresaElement.nativeElement.value)
      .subscribe(data => {
        this.empresa = data;
      }, err => { });
  }

  getReporteFacturasByProveedor() {

    this.isLoading = true;
    this.selection.clear();
    this.dataSourceFacturasProveedor.data = [];

    this.reportesService.getReporteFacturasByProveedor(this.rutProveedor, this.idEmpresa, this.fechaDesde, this.fechaHasta).subscribe(data => {

      this.isLoading = false;
      this.dataSourceFacturasProveedor.data = data;

    },
      err => {
        swal({
          title: "Ups! Ha ocurrido un problema.",
          text: err.error || getErrorsForm(err.errors),
          type: "error"
        });
        this.isLoading = false;
      });
  }

  showDetalleFactura(id: number) {
    this.modalDetalleFactura.loadFactura(id);
  }

  getProveedores() {
    this.proveedoresOptions = [];
    this.proveedoresService.getAllProveedores().subscribe(
      data => {
        this.proveedores = data;

        let proveedorOption: IOption[] = [];

        data.forEach(d => {
          proveedorOption.push({ value: d.rut, label: d.rut + ' | ' + d.razon_social, disabled: false });
        });

        this.proveedoresOptions = proveedorOption;
        this.loadingProveedores = false;
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        });
      }
    )
  }

  getProveedor() {
    this.proveedoresService.getProveedorByRut(this.rutProveedor).subscribe(
      data => {
        this.proveedorAux = data;
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        });
      }
    )
  }

  exportExcel() {
    let facturasAux = [];
    this.selection.selected.forEach((f: any) => {
      facturasAux.push({'Numero': f.numero, 'Fecha': this.transformDate(f.fecha), 'Proveedor': f.razon_social, 'RUT': f.rut, '¿Anulada?': (f.nula == 0 ? 'No' : 'Si'), 'SUB TOTAL': f.subTotal, 'TOTAL': f.total});
    });

    this.excelService.exportAsExcelFile(
      facturasAux,
      "Informe Factura Proveedor"
    );
  }

  addToReport(e) {
    let foundAux = this.facturasAux.find(fax => fax.id == e);
    let found = this.facturasChecked.find(f => f == e);
    if (found == undefined) {
      this.facturasChecked.push(e);
    } else {
      this.facturasChecked.splice(this.facturasChecked.indexOf(e), 1);
    }

    if (foundAux == undefined) {
      this.facturasAux.push(this.facturas.find(fs => fs.id == e));
    } else {
      this.facturasAux.splice(
        this.facturasAux.indexOf(this.facturasAux.find(fax => fax.id == e)),
        1
      );
    }
  }

  descargarPdf() {
    this.reportesService.makeReporteFacturasProveedor(this.selection.selected, this.fechaDesde, this.fechaHasta, this.empresa.id).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-facturas-proveedor.pdf');
    },
      error => {
        swal({
          title: 'Ups! Ha ocurrido un error.',
          html: 'No se ha podido generar el documento. Intentelo más tarde.',
          type: 'error'
        });
      });
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceFacturasProveedor.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSourceFacturasProveedor.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSourceFacturasProveedor.filter = filterValue;
  }
}
