import { Component, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { Proveedor } from '../../../models/proveedor';
import { Empresa } from '../../../models/empresa';
import { Usuario } from '../../../models/usuario';
import { AuthService } from '../../../services/auth.service';
import { ReportesService } from '../../../services/reportes.service';
import { EmpresaService } from '../../../services/empresas.service';
import { ExcelService } from '../../../services/excel.service';
import { ProveedoresService } from '../../../services/proveedores.service';
import { ReporteProveedoresPDFComponent } from '../../../shared/reportes/reporte.proveedores';
import * as $ from 'jquery';
import { saveAs } from 'file-saver/FileSaver';
import { getErrorsForm } from '../../../shared/utils';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import swal from 'sweetalert2';

@Component({
  selector: 'app-reporte-proveedores',
  templateUrl: './reporte_proveedores.component.html',
  styleUrls: ['./reporte_proveedores.component.css']
})
export class ReporteProveedoresComponent implements OnInit, AfterViewInit {

  public proveedores: Proveedor[];
  public proveedoresAux: Proveedor[];
  public empresas: Empresa[];
  public usuario: Usuario;
  public empresa: Empresa;
  public isLoading: boolean = true;
  public hideReport: boolean = false;
  public showAlertNotFound = false;

  @ViewChild('btnExportExcel') btnExportExcel: ElementRef;
  @ViewChild('btnExportPdf') btnExportPdf: ElementRef;
  @ViewChild('reporte') reporte: ElementRef;
  @ViewChild(ReporteProveedoresPDFComponent, { read: ElementRef })
  private templateReporteProveedores: ReporteProveedoresPDFComponent;

  @ViewChild('paginatorReporteProveedores') paginatorReporteProveedores: MatPaginator;
  public columnsProveedores = ['rut', 'razon_social', 'nombre_contacto', 'direccion', 'telefono', 'email'];
  public dataSourceProveedores = new MatTableDataSource<Proveedor>();

  constructor(private excelService: ExcelService, private reportesService: ReportesService, private empresasService: EmpresaService, private authService: AuthService, private proveedoresService: ProveedoresService) {
    this.proveedores = [];
    this.proveedoresAux = [];
    this.empresas = [];
    this.empresa = new Empresa();
    this.usuario = new Usuario();
  }

  ngOnInit() {
    this.templateReporteProveedores = new ReporteProveedoresPDFComponent();

    this.btnExportExcel.nativeElement.disabled = true;
    this.btnExportPdf.nativeElement.disabled = true;

    this.getAuthenticatedUser();
    this.getEmpresas();
    this.getReporteProveedores();
  }

  ngAfterViewInit() {
    this.dataSourceProveedores.paginator = this.paginatorReporteProveedores;
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.usuario = data;
        this.usuario = data;

        if (this.usuario.id_rol != 1)
        {
          this.empresa = this.usuario.empresa;
        }
      }, err => {
        localStorage.removeItem('bat');
        location.reload();
      });
  }

  getReporteProveedores() {
    this.dataSourceProveedores.data = [];

    this.reportesService.getReporteProveedores(this.empresa.id != null ? this.empresa.id : 0)
      .subscribe(
        data => {
          this.proveedores = data;

          this.dataSourceProveedores.data = data;
          this.isLoading = false;
          this.showAlertNotFound = false;

          if (this.dataSourceProveedores.data.length == 0) {
            this.btnExportExcel.nativeElement.disabled = true;
            this.btnExportPdf.nativeElement.disabled = true;
            this.showAlertNotFound = true;
          } else {
            this.btnExportExcel.nativeElement.disabled = false;
            this.btnExportPdf.nativeElement.disabled = false;
          }
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un error',
            html: getErrorsForm(err.error.errors),
            type: 'error'
          })
        }
      );
  }

  getEmpresas() {
    this.empresasService.getAllEmpresas()
      .subscribe(
        data => {
          this.empresas = data;
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un problema',
            html: getErrorsForm(err.error.errors),
            type: 'error'
          });
        }
      );
  }

  getEmpresa() {
    this.empresasService.getEmpresa(this.empresa.id)
      .subscribe(data => {
        this.empresa = data;
      }, err => {
        swal({
          title: 'Ups! Ha ocurrido un error',
          text: 'No se ha podido cargar la empresa',
          type: 'error'
        });
      }
      );
  }

  exportExcel() {

    let proveedoresAux = [];

    this.dataSourceProveedores.data.forEach((d: Proveedor) => {
      proveedoresAux.push({'rut': d.rut, 'razon_social': d.razon_social, 'nombre_contacto': d.nombre_contacto, 'direccion': d.direccion, 'telefono': d.telefono, 'email': d.email});
    });

    this.excelService.exportAsExcelFile(proveedoresAux, 'Informe Proveedores');
  }

  fillArrayWithProveedores(e) {
    this.proveedoresAux = [];

    e.selected.forEach(p => {
      this.proveedoresAux.push(p);
    });
  }

  searchProveedoresByEmpresa() {
    this.isLoading = true;
    this.dataSourceProveedores.data = [];

    this.proveedoresService.getProveedoresByEmpresa(this.empresa.id)
      .subscribe(data => {
        this.isLoading = false;
        this.proveedores = data;
        this.dataSourceProveedores.data = data;

        if (this.dataSourceProveedores.data.length == 0) {
          this.btnExportExcel.nativeElement.disabled = true;
          this.btnExportPdf.nativeElement.disabled = true;
        }
        else {
          this.btnExportExcel.nativeElement.disabled = false;
          this.btnExportPdf.nativeElement.disabled = false;
        }
      },
        err => {
          this.isLoading = false;
          swal({
            title: 'Ups! Ha ocurrido un error.',
            html: getErrorsForm(err.error.errors)
          });
        }
      );
  }

  descargarPdf() {

    let idEmpresa = this.empresa.id != null ? this.empresa.id : 0; 

    this.reportesService.makeReporteProveedores(this.dataSourceProveedores.data, idEmpresa).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-proveedores.pdf');
    },
    error => {
      swal({
        title: 'Ups! Ha ocurrido un error.',
        html: 'No se ha podido generar el documento. Intentelo más tarde.',
        type: 'error'
      });
    });
  }

}
