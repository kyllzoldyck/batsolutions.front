import { Component, Input, ViewChild, ElementRef, Output, AfterViewInit } from '@angular/core';
import { Proveedor } from '../../models/proveedor';
import { Usuario } from '../../models/usuario';
import { Empresa } from '../../models/empresa';
import * as $ from 'jquery';
import { EventEmitter } from 'protractor';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-template-reporte-proveedores',
  templateUrl: './reporte.proveedores.html',
  styleUrls: ['./reporte.proveedores.css']
})
export class ReporteProveedoresPDFComponent {

  public date: number;

  @Input('usuario') usuario: Usuario;
  @Input('proveedores') dataSourceProveedoresModal = new MatTableDataSource<Proveedor>()
  @Input('empresa') empresa: Empresa;

  public columnsProveedoresModal = ['rut', 'razon_social', 'nombre_contacto', 'direccion', 'telefono', 'email'];
  
  constructor() {
    this.date = Date.now();
  }
}
