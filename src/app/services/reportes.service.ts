import { Injectable } from '@angular/core';
import { Http, ResponseContentType } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../models/cliente';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { getHeaders, extractData, handleError } from '../shared/utils';
import { Proveedor } from '../models/proveedor';
import { Factura } from '../models/factura';
import { Articulo } from '../models/articulo';

@Injectable()
export class ReportesService {

  public API_URL = environment.URI;

  constructor(private http: HttpClient) { }

  getReporteClientes(empresa: number = 0): Observable<any> {
    const URL = this.API_URL + 'reportes/clientes?empresa=' + empresa;
    return this.http.get(URL, { headers: getHeaders() });
  }

  generarReporteClientes(html): Observable<any> {
    const URL = this.API_URL + 'reportes/clientes/pdf';
    const body = {
      document: html
    };
    return this.http
      .post(URL, JSON.stringify(body), { headers: getHeaders([{ key: 'Accept', value: 'application/pdf' }])/*, responseType: ResponseContentType.Blob */ });
  }

  getReporteProveedores(empresa: number = 0): Observable<any> {
    const URL = this.API_URL + 'reportes/proveedores?empresa=' + empresa;
    console.log(URL);

    return this.http
      .get(URL, { headers: getHeaders() });
  }

  getReporteFacturasByCliente(rut: string, empresa: number, fd: string, fh: string): Observable<any> {
    const URL = this.API_URL + 'reportes/facturas/clientes/' + rut + '?empresa=' + empresa + '&fechaDesde=' + fd + '&fechaHasta=' + fh;
    console.log(URL);

    return this.http
      .get(URL, { headers: getHeaders() });
  }

  getReporteFacturasByProveedor(rut: string, empresa: number, fd: string, fh: string): Observable<any> {
    const URL = this.API_URL + 'reportes/facturas/proveedores/' + rut + '?empresa=' + empresa + '&fechaDesde=' + fd + '&fechaHasta=' + fh;
    console.log(URL);

    return this.http
      .get(URL, { headers: getHeaders() });
  }

  getReporteArticulos(id: number, empresa: number): Observable<any> {
    const URL = this.API_URL + 'reportes/articulos/proveedores/' + id + '?empresa=' + empresa;
    console.log(URL);
    return this.http.get(URL, { headers: getHeaders() });
  }

  makeReporteClientes(clientes: Cliente[], id: number): Observable<any> {

    const URL = this.API_URL + 'reportes/generate/clientes?empresa=' + id;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];

    let body = {
      clientes: clientes
    };

    return this.http.post(URL, body, { headers: getHeaders(headers), responseType: "blob" });
  }

  makeReporteProveedores(proveedores: Proveedor[], id: number): Observable<any> {
    const URL = this.API_URL + 'reportes/generate/proveedores?empresa=' + id;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];
    let body = {
      proveedores: proveedores
    };

    return this.http.post(URL, body, { headers: getHeaders(headers), responseType: "blob" });
  }

  makeReporteFacturasProveedor(facturas: any[], fechaDesde: string, fechaHasta: string, empresa: number)
  {
    const URL = this.API_URL + 'reportes/generate/facturas-proveedor?empresa=' + empresa + '&fechaDesde=' + fechaDesde + '&fechaHasta=' + fechaHasta;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];
    let body = {
      facturas: facturas
    };

    return this.http.post(URL, body, { headers: getHeaders(headers), responseType: "blob" });
  }

  makeReporteFacturasCliente(facturas: any[], fechaDesde: string, fechaHasta: string, empresa: number)
  {
    const URL = this.API_URL + 'reportes/generate/facturas-cliente?empresa=' + empresa + '&fechaDesde=' + fechaDesde + '&fechaHasta=' + fechaHasta;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];
    let body = {
      facturas: facturas
    };

    return this.http.post(URL, body, { headers: getHeaders(headers), responseType: "blob" });
  }

  makeReporteDetalleFactura(id: number) {
    const URL = this.API_URL + 'reportes/generate/detalle-factura/' + id;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];
    return this.http.get(URL, { headers: getHeaders(headers), responseType: "blob" });
  }

  makeReporteArticulos(articulos: Articulo[], id: number): Observable<any> {

    const URL = this.API_URL + 'reportes/generate/articulos?empresa=' + id;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];

    let body = {
      articulos: articulos
    };

    return this.http.post(URL, body, { headers: getHeaders(headers), responseType: "blob" });
  }

  makeReporteDetallePresupuesto(id: number) {
    const URL = this.API_URL + 'reportes/generate/detalle-presupuesto/' + id;
    let headers = [{ key: 'Accept', value: 'application/pdf' }];
    return this.http.get(URL, { headers: getHeaders(headers), responseType: "blob" });
  }
}
