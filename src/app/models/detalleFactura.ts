export class DetalleFactura {

    public id: number;
    public id_factura: number;
    public iva: number;
    public unidades: number;
    public descripcion_articulo: string;
    public precio: number;
    public id_articulo: number;
    public codigo_articulo: string;

    public constructor( data: any = {}) {
        this.id = data.id || 0;
        this.id_factura = data.id_factura || 0;
        this.unidades = data.unidades || null;
        this.iva = data.iva || null;
        this.precio = data.precio || null;
        this.id_articulo = data.id_articulo || null;
        this.descripcion_articulo = data.descripcion_articulo || null;
        this.codigo_articulo = data.codigo_articulo || null;
    }
}
