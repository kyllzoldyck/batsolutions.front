export class DetallePresupuesto {

    public id: number;
    public id_presupuesto: number;
    public iva: number;
    public unidades: number;
    public descripcion_articulo: string;
    public precio: number;
    public codigo_articulo: string;
    public id_articulo: number;

    public descripcion: string;
    public codigo: string;
    public precio_de_venta: number;

    constructor(data: any = {}) {
        this.id = data.id || null;
        this.id_presupuesto = data.id_presupuesto || null;
        this.iva = data.iva || null;
        this.unidades = data.unidades || null;
        this.descripcion_articulo = data.descripcion_articulo || null;
        this.precio = data.precio || null;
        this.codigo_articulo = data.codigo_articulo || null;
        this.id_articulo = data.id_articulo || null;
    }
}