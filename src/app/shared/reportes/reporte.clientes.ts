import { Component, Input, ViewChild, AfterViewInit } from '@angular/core';
import { Cliente } from '../../models/cliente';
import { Usuario } from '../../models/usuario';
import { Empresa } from '../../models/empresa';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-template-reporte-clientes',
  templateUrl: './reporte.clientes.html',
  styleUrls: ['./reporte.clientes.css']
})
export class ReporteClientesPDFComponent implements AfterViewInit {

  public date: number;
  
  @Input('usuario') usuario: Usuario;
  @Input('clientes') dataSourceClientesModal = new MatTableDataSource<Cliente>();
  @Input('empresa') empresa: Empresa;

  @ViewChild('paginatorReporteClientesModal') paginatorReporteClientesModal: MatPaginator;
  public columnsClientesModal = ['rut', 'razon_social', 'nombre_contacto', 'direccion', 'telefono', 'email'];

  constructor() {
    this.date = Date.now();
  }

  ngAfterViewInit() {
    // this.dataSourceClientesModal.paginator = this.paginatorReporteClientesModal;
  }
}
