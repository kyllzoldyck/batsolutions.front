import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatTableModule, MatFormFieldModule, MatProgressSpinnerModule, MatPaginatorModule, MatCheckboxModule, MatInputModule } from "@angular/material";
import { ArticulosService } from "../../../../services/articulos.service";
import { ImpuestosService } from "../../../../services/impuestos.services";
import { ModalArticulosComponent } from "./modal-articulos.component";
import { ThousandsPipeModule } from "../../../../shared/pipes/thousands.module";
import { DecimalsPipeModule } from "../../../../shared/pipes/decimals.module";
import { InputUnidadesComponent } from "../input-unidades/input-unidades.component";
import { InputUnidadesModule } from "../input-unidades/input-unidades.module";

@NgModule({
    imports: [
        CommonModule,
        MatTableModule,
        MatFormFieldModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatCheckboxModule,
        ThousandsPipeModule,
        DecimalsPipeModule,
        InputUnidadesModule,
        MatInputModule
    ],
    providers: [
        ArticulosService,
        ImpuestosService
    ],
    declarations: [
        ModalArticulosComponent
    ],
    exports: [
        ModalArticulosComponent
    ]
})
export class ModalArticulosModule { }