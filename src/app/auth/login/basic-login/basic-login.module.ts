import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicLoginComponent } from './basic-login.component';
import { BasicLoginRoutingModule } from './basic-login-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { AuthGuard } from '../../auth.guard';
import { AuthService } from '../../../services/auth.service';
import { MatProgressSpinnerModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    BasicLoginRoutingModule,
    SharedModule,
    MatProgressSpinnerModule
  ],
  declarations: [BasicLoginComponent],
  providers: [AuthService, AuthGuard]
})
export class BasicLoginModule { }
