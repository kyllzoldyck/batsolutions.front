import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpresaService } from '../../../../services/empresas.service';
import { Empresa } from '../../../../models/empresa';
import { Cliente } from '../../../../models/cliente';
import { ClientesService } from '../../../../services/clientes.service';
import { AuthService } from '../../../../services/auth.service';
import { Usuario } from '../../../../models/usuario';
import { Factura } from '../../../../models/factura';
import { FacturasService } from '../../../../services/facturas.service';
import { ReportesService } from '../../../../services/reportes.service';
import { ExcelService } from '../../../../services/excel.service';
import { ModalDetalleFacturaComponent } from '../../../../shared/reportes/modal.detalle.factura';
import { ReporteFacturasClientePDFComponent } from '../../../../shared/reportes/reporte.facturas.cliente';
import { getErrorsForm } from '../../../../shared/utils';
import * as $ from 'jquery';
import { IOption } from 'ng-select';
import swal from "sweetalert2";
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ModalBasicComponent } from '../../../../shared/modal-basic/modal-basic.component';
import { saveAs } from 'file-saver/FileSaver';

declare var xepOnline: any;

const initialSelection = [];
const allowMultiSelect = true;

@Component({
  selector: 'app-facturas-clientes',
  templateUrl: './facturas_clientes.component.html',
  styleUrls: ['./facturas_clientes.component.css']
})
export class FacturasClientesComponent implements OnInit, AfterViewInit {

  public clientesOptions: Array<IOption> = new Array<IOption>();
  public empresasOptions: Array<IOption> = new Array<IOption>();
  public empresas: Empresa[];
  public clientes: Cliente[];
  public facturas: Factura[];
  public facturasAux: Factura[];
  public usuario: Usuario;
  public clienteAux: Cliente;
  public factura: Factura;
  public empresa: Empresa;
  public cliente: Cliente;

  public idEmpresa: number;
  public rutProveedor: string;
  public fechaDesde: any;
  public fechaHasta: any;

  public dataSourceFacturasCliente = new MatTableDataSource<Factura>();
  public dataColumnsFacturasCliente = ['select', 'numero', 'fecha', 'cliente', 'total', 'nula', 'acciones'];
  public selection = new SelectionModel(allowMultiSelect, initialSelection);
  public selected = [];

  public isLoading: boolean = false;
  public facturasChecked: any[];
  public showAlertNotFound = false;
  public loadingProveedores: boolean = true;

  @ViewChild('modalFacturasProveedor') modalFacturasProveedor: ModalBasicComponent;
  @ViewChild('btnExportExcel') btnExportExcel: ElementRef;
  @ViewChild('btnExportPdf') btnExportPdf: ElementRef;
  @ViewChild('empresa') empresaElement: ElementRef;
  @ViewChild(ModalDetalleFacturaComponent)

  private modalDetalleFactura: ModalDetalleFacturaComponent;
  @ViewChild(ReporteFacturasClientePDFComponent)
  private reporteFacturasClientePDFComponent: ReporteFacturasClientePDFComponent;

  constructor(
    public fb: FormBuilder,
    private reportesService: ReportesService,
    private empresasService: EmpresaService,
    private clientesService: ClientesService,
    private authService: AuthService,
    private facturasService: FacturasService,
    private excelService: ExcelService,
    private cdr: ChangeDetectorRef,
    private datePipe: DatePipe
  ) {
    this.empresas = [];
    this.clientes = [];
    this.facturas = [];
    this.facturasAux = [];
    this.facturasChecked = [];
    this.usuario = new Usuario();
    this.clienteAux = new Cliente();
    this.factura = new Factura();
    this.empresa = new Empresa();
    this.cliente = new Cliente();
  }

  ngOnInit() {
    this.getAuthenticatedUser();
    this.getEmpresas();
    this.buildForm();

    this.btnExportExcel.nativeElement.disabled = true;
    this.btnExportPdf.nativeElement.disabled = true;
  }

  ngAfterViewInit() {
  }

  formErrors = {
    'empresa': '',
    'cliente': '',
    'fechaDesde': '',
    'fechaHasta': ''
  };

  validationMessages = {
    'empresa': {
      'required': 'Debe seleccionar una empresa',
    },
    'cliente': {
      'required': 'Debe seleccionar un cliente.',
    },
    'fechaDesde': {
      'required': 'Debe seleccionar una fecha desde.',
    },
    'fechaHasta': {
      'required': 'Debe seleccionar una fecha hasta.',
    }
  };

  facturasClienteForm: FormGroup;

  /**
   * Construye el formulario y sus validaciones
   */
  buildForm() {

    let empresa = [
      this.idEmpresa,
      Validators.compose([Validators.required])
    ];

    if (this.usuario.id_rol != 1)
      empresa = [];

    this.facturasClienteForm = this.fb.group({
      empresa,
      cliente: [this.rutProveedor, Validators.compose([
        Validators.required,
      ])],
      fechaDesde: [this.fechaDesde, Validators.compose([
        Validators.required,
      ])],
      fechaHasta: [this.fechaHasta, Validators.compose([
        Validators.required,
      ])]
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.facturasClienteForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );

    this.onValueChanged();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChanged(data?: any) {
    if (!this.facturasClienteForm) {
      return;
    }

    const form = this.facturasClienteForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy');
  }

  getEmpresas() {

    this.empresasOptions = [];
    this.empresasService.getAllEmpresas().subscribe(
      data => {
        this.empresas = data;

        data.forEach(d => {
          let empresaOption: IOption = { value: d.id.toString(), label: d.razon_social, disabled: false };
          this.empresasOptions.push(empresaOption);
        });

        this.loadingProveedores = false;
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        });
      }
    );
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser().subscribe(
      data => {
        this.usuario = data;
        if (this.usuario.id_rol != 1) {
          this.getClientes();
          this.empresa = this.usuario.empresa;
        }
      },
      err => {
        localStorage.removeItem('bat');
        location.reload();
      }
    );
  }

  /**
   * Llena el select con los clientes según la empresa seleccionada
   * @param e ID de la empresa
   */
  getClientesByEmpresa() {
    this.clientesOptions = [];
    this.empresaElement.nativeElement.disabled = true;

    this.clientesService
      .getClientesByEmpresa(this.empresaElement.nativeElement.value)
      .subscribe(
        data => {
          this.empresaElement.nativeElement.disabled = false;
          this.clientes = data;

          let clienteOption: IOption[] = [];

          data.forEach(p => {
            clienteOption.push({ value: p.rut, label: p.rut + ' | ' + p.razon_social, disabled: false });
          });

          this.clientesOptions = clienteOption;
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un problema',
            text: 'No se han podido cargar los clientes.',
            type: 'error'
          });
          this.empresaElement.nativeElement.disabled = false;
        }
      );
    this.empresasService.getEmpresa(this.empresaElement.nativeElement.value)
      .subscribe(data => {
        this.empresa = data;
      }, err => { });
  }

  getReporteFacturasByCliente() {

    this.isLoading = true;
    this.selection.clear();
    this.dataSourceFacturasCliente.data = [];

    this.reportesService.getReporteFacturasByCliente(this.cliente.rut, this.idEmpresa, this.fechaDesde, this.fechaHasta).subscribe(data => {

      this.isLoading = false;
      this.dataSourceFacturasCliente.data = data;

    },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        });
        this.isLoading = false;
      });
  }

  showDetalleFactura(id: number) {
    this.modalDetalleFactura.loadFactura(id);
  }

  getClientes() {
    this.clientesOptions = [];
    this.clientesService.getAllClientes().subscribe(
      data => {
        this.clientes = data;
        let clienteOption: IOption[] = [];

        data.forEach(d => {
          clienteOption.push({ value: d.rut, label: d.rut + ' | ' + d.razon_social, disabled: false });
        });

        this.clientesOptions = clienteOption;
        this.loadingProveedores = false;
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        });
      }
    );
  }

  getCliente() {
    this.clientesService.getClienteByRut(this.cliente.rut).subscribe(
      data => {
        this.clienteAux = data;
        console.log('cliente', data);
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: err.error || getErrorsForm(err.errors),
          type: 'error'
        });
      }
    )
  }

  exportExcel() {
    let facturasAux = [];
    this.selection.selected.forEach((f: any) => {
      facturasAux.push({'Numero': f.numero, 'Fecha': this.transformDate(f.fecha), 'Cliente': f.razon_social, 'RUT': f.rut, '¿Anulada?': (f.nula == 0 ? 'No' : 'Si'), 'SUB TOTAL': f.subTotal, 'TOTAL': f.total});
    });

    this.excelService.exportAsExcelFile(
      facturasAux,
      'Informe Factura Cliente'
    );
  }

  addToReport(e) {
    let foundAux = this.facturasAux.find(fax => fax.id == e);
    let found = this.facturasChecked.find(f => f == e);
    if (found == undefined) {
      this.facturasChecked.push(e);
    } else {
      this.facturasChecked.splice(this.facturasChecked.indexOf(e), 1);
    }

    if (foundAux == undefined) {
      this.facturasAux.push(this.facturas.find(fs => fs.id == e));
    } else {
      this.facturasAux.splice(
        this.facturasAux.indexOf(this.facturasAux.find(fax => fax.id == e)),
        1
      );
    }
  }

  descargarPdf() {
    this.reportesService.makeReporteFacturasCliente(this.selection.selected, this.fechaDesde, this.fechaHasta, this.empresa.id).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-facturas-clientes.pdf');
    },
      error => {
        swal({
          title: 'Ups! Ha ocurrido un error.',
          html: 'No se ha podido generar el documento. Intentelo más tarde.',
          type: 'error'
        });
      });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceFacturasCliente.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSourceFacturasCliente.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSourceFacturasCliente.filter = filterValue;
  }
}
