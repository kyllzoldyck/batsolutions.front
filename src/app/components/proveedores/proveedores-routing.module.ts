import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProveedoresComponent } from "./proveedores.component";

const routes: Routes = [
    {
        path: '',
        component: ProveedoresComponent,
        data: {
            breadcrumb: 'Proveedores',
            icon: 'fa fa-truck',
            status: false
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProveedoresRoutingModule {}