import { Empresa } from "./empresa";
import { Proveedor } from "./proveedor";

export class Articulo {

    public id: number;
    public descripcion: string;
    public eliminado: number;
    public id_proveedor: string;
    public precio_de_compra: number;
    public precio_de_venta: number;
    public stock: number;
    public created_at: Date;
    public updated_at: Date;
    public codigo: string;
    public rut_proveedor: string;
    public proveedor: Proveedor;
    public empresa: Empresa;
    public subtotal: number;
    public esVenta: boolean;
    public unidades: number;
    public iva: number;
    public total: number;

    public constructor( data: any = {}) {
        this.id = data.id || null;
        this.descripcion = data.descripcion || null;
        this.eliminado = data.eliminado || null;
        this.id_proveedor = data.id_proveedor || null;
        this.precio_de_compra = data.precio_de_compra || null;
        this.precio_de_venta = data.precio_de_venta || null;
        this.stock = data.stock || null;
        this.created_at = data.created_at || null;
        this.updated_at = data.updated_at || null;
        this.codigo = data.codigo || null;
        this.rut_proveedor = data.rut_proveedor || null;
        this.proveedor = data.proveedor || null;
        this.empresa = data.empresa || null;
        this.subtotal = data.subtotal || null;
        this.esVenta = data.esVenta || null;
        this.unidades = data.unidades || null;
        this.iva = data.iva || null;
        this.total = data.total || null;
    }
}
