import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { InputUnidadesComponent } from "./input-unidades.component";
import { ImpuestosService } from "../../../../services/impuestos.services";
import { FormsModule } from "@angular/forms";

@NgModule({
    exports: [
        InputUnidadesComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        InputUnidadesComponent
    ],
    providers: [
        ImpuestosService
    ]
})
export class InputUnidadesModule { }