import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotComponent } from './forgot.component';
import { SharedModule } from '../../shared/shared.module';
import { ForgotRoutingModule } from './forgot-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ForgotRoutingModule,
    SharedModule,
    MatProgressSpinnerModule
  ],
  declarations: [ForgotComponent]
})
export class ForgotModule { }
