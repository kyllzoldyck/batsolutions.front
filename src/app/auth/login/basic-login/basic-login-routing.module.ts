import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasicLoginComponent } from './basic-login.component';
import { AuthGuard } from '../../auth.guard';

const routes: Routes = [
  {
    path: '',
    component: BasicLoginComponent,
    canActivate: [AuthGuard],
    data: {
      breadcrumb: 'Login',
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicLoginRoutingModule { }
