import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';

declare var swal: any;

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, public router: Router) { }

  // Maneja los estados de authenticación de un usuario
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    let sw = true;

    if (localStorage.getItem('bat') == '') {
      this.router.navigate(['/auth/login']);
      return false;
    }

    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        sw = true;
      }, 
      err => {
        localStorage.removeItem('bat');
        this.router.navigate(['/auth/login']);
        sw = false;
      }
    );

    return sw;
  }
}
