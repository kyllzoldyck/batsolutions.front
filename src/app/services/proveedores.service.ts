import { Injectable } from "@angular/core";
import { Response, Headers } from "@angular/http";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Proveedor } from "../models/proveedor";
import { handleError, extractData, getHeaders } from "../shared/utils";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

@Injectable()
export class ProveedoresService {
  private API_URI = environment.URI;

  constructor(private http: HttpClient, public router: Router) {}

  /**
   * Funcion encargada de obtener todos los proveedores desde el Servidor.
   */
  getAllProveedores(): Observable<Proveedor[]> {
    const URL = this.API_URI + "proveedores";
    return this.http.get<Proveedor[]>(URL, { headers: getHeaders() });
  }

  /**
   * Funcion encargada de obtener un proveedor desde el servidor.
   */
  getProveedorById(id: number): Observable<any> {
    let URL = this.API_URI + "proveedores/" + id;
    return this.http
      .get(URL, { headers: getHeaders() })
      .map(extractData)
      .catch(handleError);
  }

  /**
   * Agrega un proveedor enviandolo al Servidor.
   * @param proveedor
   */
  addProveedor(proveedor: Proveedor): Observable<any> {
    let URL = this.API_URI + "proveedores";
    let body = {
        rut: proveedor.rut,
        razon_social: proveedor.razon_social,
        nombre_contacto: proveedor.nombre_contacto,
        direccion: proveedor.direccion,
        telefono: proveedor.telefono,
        email: proveedor.email,
        id_empresa: proveedor.id_empresa,
        giro: proveedor.giro
    };
    return this.http.post(URL, JSON.stringify(body), { headers: getHeaders() });
        // .map(extractData)
        // .catch(handleError)
  }

  /**
   * Actualiza un proveedor enviandolo al servidor.
   * @param proveedor
   */
  updateProveedor(proveedor: Proveedor): Observable<any> {
    let URL = this.API_URI + "proveedores/" + proveedor.id;

    let body = {
        razon_social: proveedor.razon_social,
        nombre_contacto: proveedor.nombre_contacto,
        direccion: proveedor.direccion,
        telefono: proveedor.telefono,
        email: proveedor.email,
        id_empresa: proveedor.id_empresa,
        rut: proveedor.rut,
        giro: proveedor.giro
    };

    console.log('body proveedor', body);

    return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() });
    // .map(extractData)
    // .catch(handleError);
  }

  /**
   * Elimina un proveedor dado su Id.
   * @param id Proveedor
   */
  deleteProveedor(id: Number): Observable<Response> {
    let URL = this.API_URI + "proveedores/" + id;

    return (
      this.http
        .delete(URL, { headers: getHeaders() })
        // .map(extractData)
        .catch(handleError)
    );
  }

  /**
   * Obtiene proveedores según el ID de la empresa
   * @param empresa ID de la empresa
   */
  getProveedoresByEmpresa(empresa: number): Observable<any> {
    let URL = this.API_URI + "proveedores/empresas/" + empresa;

    return this.http.get(URL, { headers: getHeaders() });
  }

  getProveedorByRut(rut: string): Observable<Proveedor> {
    let URL = this.API_URI + "proveedores/rut/" + rut;
    return this.http.get<Proveedor>(URL, { headers: getHeaders() })
  }
}
