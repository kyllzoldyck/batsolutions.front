import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PasswordResetComponent } from "./password-reset.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { PasswordResetRoutingModule } from "./password-reset-routing.module";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        PasswordResetRoutingModule
    ],
    declarations: [
        PasswordResetComponent
    ],
    providers: [
        AuthService
    ]
})
export class PasswordResetModule { }