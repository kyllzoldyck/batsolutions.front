import { ValidatorFn, AbstractControl } from '@angular/forms';

export function isValidEmail(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (control.value == null)
      return null

    if (control.value.match(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)) {
      return null;
    } else {
      return { 'isValidEmail': true }
    }
    // if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
    //     return null;
    // } else {
    //     return { 'isValidEmail': true };
    // }
  }
}
