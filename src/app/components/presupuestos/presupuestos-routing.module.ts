import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PresupuestoComponent } from "./presupuestos.component";
import { CommonModule } from "@angular/common";

const routes: Routes = [
    {
        path: '',
        component: PresupuestoComponent,
        data: {
            breadcrumb: 'Presupuesto',
            icon: '',
            status: false
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PresupuestoRoutingModule { }