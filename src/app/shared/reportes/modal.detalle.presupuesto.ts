import { Component, OnInit, Output, Input, ElementRef, ViewChild } from '@angular/core';
import { EmpresaService } from '../../services/empresas.service';
import { Empresa } from '../../models/empresa';
import { ModalBasicComponent } from '../modal-basic/modal-basic.component';
import { ReportesService } from '../../services/reportes.service';
import { saveAs } from 'file-saver/FileSaver';
import { Presupuesto } from '../../models/presupuesto';
import { PresupuestoService } from '../../services/presupuestos.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-modal-detalle-presupuesto',
  templateUrl: './modal.detalle.presupuesto.html',
  styleUrls: ['./modal.detalle.presupuesto.css']
})
export class ModalDetallePresupuestoComponent implements OnInit {

  public presupuesto: Presupuesto;
  public date: number;
  public isLoading: boolean = true;
  public empresa: Empresa;

  @ViewChild('modal') modal: ModalBasicComponent;

  constructor(private presupuestosService: PresupuestoService, private empresaService: EmpresaService, private reportesService: ReportesService) {
    this.presupuesto = new Presupuesto();
    this.date = Date.now();
    this.empresa = new Empresa();
  }

  ngOnInit() {
  }

  loadPresupuesto(id: number) {
    this.modal.show();

    this.presupuestosService.getPresupuesto(id)
      .subscribe(data => {
        this.presupuesto = data;
        this.isLoading = false;
        this.empresaService.getEmpresa(this.presupuesto.id_empresa).subscribe(data => this.empresa = data, err => {});
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: 'No se ha podido cargar el detalle del presupuesto.',
          type: 'error'
        });
      }
    );
  }

  exportarPDF() {
    this.reportesService.makeReporteDetallePresupuesto(this.presupuesto.id).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-presupuesto.pdf');
    },
    error => {
      console.log(error);
    });
  }

}
