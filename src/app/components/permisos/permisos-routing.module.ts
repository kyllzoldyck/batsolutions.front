import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PermisosComponent } from './permisos.component';

const routes: Routes = [
  {
    path: '',
    component: PermisosComponent,
    data: {
        breadcrumb: 'Permiso',
        icon: '',
        status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PermisosRoutingModule { }
