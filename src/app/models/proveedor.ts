import { Empresa } from "./empresa";

export class Proveedor {

  public id: number;
  public rut: string;
  public razon_social: string;
  public nombre_contacto: string;
  public direccion: string;
  public telefono: string;
  public email: string;
  public id_empresa: number;
  public empresa: Empresa;
  public giro: string;

  public constructor(data: any = {}) {
    this.id = data.id || null;
    this.rut = data.rut || null;
    this.razon_social = data.razon_social || null;
    this.nombre_contacto = data.nombre_contacto || null;
    this.direccion = data.direccion || null;
    this.telefono = data.telefono || null;
    this.email = data.email || null;
    this.id_empresa = data.id_empresa || null;
    this.empresa = data.empresa || null;
    this.giro = data.giro || null;
  }
}
