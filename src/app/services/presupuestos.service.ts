import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { Presupuesto } from "../models/presupuesto";
import { getHeaders } from "../shared/utils";

@Injectable()
export class PresupuestoService { 

    constructor(private http: HttpClient) { }

    getPresupuestos(): Observable<Presupuesto[]> {
        const URI = environment.URI + 'presupuestos';
        return this.http.get<Presupuesto[]>(URI, { headers: getHeaders() });
    }

    addPresupuesto(presupuesto: Presupuesto): Observable<any> {
        const URI = environment.URI + 'presupuestos';
        return this.http.post(URI, presupuesto, { headers: getHeaders() });
    }

    updatePresupuest(presupuesto: Presupuesto): Observable<any> {
        const URI = environment.URI + 'presupuestos/' + presupuesto.id;
        return this.http.put(URI, presupuesto, { headers: getHeaders() }); 
    }

    getPresupuesto(id: number): Observable<Presupuesto> {
        const URI = environment.URI + 'presupuestos/' + id;
        return this.http.get<Presupuesto>(URI, { headers: getHeaders() });
    }

    deletePresupuesto(id: number): Observable<any> {
        const URI = environment.URI + 'presupuestos/' + id;
        return this.http.delete(URI, { headers: getHeaders() });
    }

    getDefaultEmail(id: number): Observable<any> {
        const URI = environment.URI + 'presupuestos/default-email/' + id;
        return this.http.get(URI, { headers: getHeaders() });
    }

    sendPresupuestoEmail(id: number, email: string): Observable<any> {
        const URI = environment.URI + 'presupuestos/send-email/' + id;
        let body = {
            email: email
        }
        return this.http.post(URI, body, { headers: getHeaders() });
    }
}