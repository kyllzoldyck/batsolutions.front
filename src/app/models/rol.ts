export class Rol {

  public id: number;
  public name: string;
  public state: number;
  public created_at: string;
  public updated_at: string;

  public constructor(data: any = {}) {
    this.id = data.id || null;
    this.name = data.name || null;
    this.state = data.state || null;
    this.created_at = data.created_at || null;
    this.updated_at = data.updated_at || null;
  }
}
