import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core';
import { FacturasService } from '../../../../services/facturas.service';
import { Impuesto } from '../../../../models/impuesto';
import { ImpuestosService } from '../../../../services/impuestos.services';
import swal from "sweetalert2";

@Component({
    selector: 'input-unidades',
    template: `<input type="number" id="{{ 'unidades_' + rowData.id }}" class="form-control" (focusout)="sendCantidadUnidades()" min="1" [(ngModel)]="unidades" style="text-align:center; width: 80px;" placesholder="Cantidad">`,
})

export class InputUnidadesComponent implements OnInit {
    renderValue: string;
    unidades: number;
    
    @Input() value: string | number;
    @Input() rowData: any;
    @Input() impuesto: Impuesto;

    @Output() sendArticulo: EventEmitter<any> = new EventEmitter();

    constructor(private impuestosService: ImpuestosService) {
        this.impuesto = new Impuesto();
    }

    ngOnInit() {
        if (this.rowData.stock == 0 && this.rowData.esVenta)
            this.unidades = 0;
        else
            this.unidades = this.rowData.unidades;
    }

    sendCantidadUnidades() {

        if (this.unidades < 0) {
            swal({
                title: 'Ups! Tenemos un problema',
                text: 'Debe ingresar un valor mayor a cero en Unidades.',
                type: 'error'
            })

            this.unidades = 1;
        }
        else {

            let articulo = this.rowData;
            articulo.unidades = this.unidades;

            if (articulo.esVenta) 
            {
                if (articulo.unidades > articulo.stock) 
                {
                    swal({
                        title: 'Ups! Tenemos un problema',
                        text: `El stock no es suficiente para este articulo. Cantidad máxima permitida es ${articulo.stock} unidades.`,
                        type: 'info'
                    })
                    articulo.unidades = articulo.stock;
                }
                articulo.subtotal = (articulo.precio_de_venta * articulo.unidades);
            }
            else
                articulo.subtotal = (articulo.precio_de_compra * articulo.unidades);

            articulo["iva"] = (articulo.subtotal * this.impuesto.valor);
            articulo["total"] = (articulo.subtotal + articulo.iva);

            this.sendArticulo.emit(articulo);
        }
    }

    getIva() {
        this.impuestosService.getImpuestoByNombre('iva')
            .subscribe(data => {
                this.impuesto = data;
            },
                error => {
                    swal({
                        title: 'Ups! Tenemos un problema',
                        text: 'No se pudo cargar el IVA',
                        type: 'error'
                    });
                }
            );
    }

}
