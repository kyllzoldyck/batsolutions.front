import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { handleError, extractData, getHeaders } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { NotaDeCredito } from "../models/notaCredito";

@Injectable()
export class NotasDeCreditoService {
    private API_URI = environment.URI;

    constructor(private http: HttpClient, public router: Router) {}

    addNotas(notas: NotaDeCredito): Observable<any> {
        let URL = this.API_URI + 'notas';

        let body = {
            numero: notas.numero,
            descripcion: notas.descripcion,
            id_factura: notas.id_factura,
            fecha: notas.fecha,
        }

        return this.http.post(URL, JSON.stringify(body), { headers: getHeaders() });
    }

    getNotasByNumero(numero: number): Observable<any> {
        let URL = this.API_URI + 'notas/numero/' + numero;
        return this.http.get(URL, { headers: getHeaders() });
    }


    getNotasById(id: number): Observable<any> {
        let URL = this.API_URI + 'notas/' + id;

        return this.http.get(URL, { headers: getHeaders() });
    }

    getAllNotas(): Observable<any> {
        let URL = this.API_URI + 'notas';

        return this.http.get(URL, { headers: getHeaders() });
    }

    updateNota(nota: NotaDeCredito): Observable<any> {
        let URL = this.API_URI + 'notas/' + nota.id;
        let body = {
            descripcion: nota.descripcion,
            fecha: nota.fecha,
        }

        return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() });
    }

    /**
     * Elimina una nota de credito dado su Id.
     * @param id Factura
     */
    deleteNotaById(id: number): Observable<any> {
        let URL = this.API_URI + 'notas/' + id;

        return this.http.delete(URL, { headers: getHeaders() });
    }
}
