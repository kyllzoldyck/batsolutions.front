import { Component, OnInit, Output, Input, ElementRef, ViewChild } from '@angular/core';
import { FacturasService } from '../../services/facturas.service';
import { Factura } from '../../models/factura';
import { ReporteDetalleFacturaPDFComponent } from './reporte.detalle.factura';
import * as $ from 'jquery';
import { EmpresaService } from '../../services/empresas.service';
import { Empresa } from '../../models/empresa';
import { ModalBasicComponent } from '../modal-basic/modal-basic.component';
import { ReportesService } from '../../services/reportes.service';
import { saveAs } from 'file-saver/FileSaver';

declare var swal: any;
declare var xepOnline: any;

@Component({
  selector: 'app-modal-detalle-factura',
  templateUrl: './modal.detalle.factura.html',
  styleUrls: ['./modal.detalle.factura.css']
})
export class ModalDetalleFacturaComponent implements OnInit {

  public factura: Factura;
  public date: number;
  public isLoading: boolean = true;
  public empresa: Empresa;

  @ViewChild('modal') modal: ModalBasicComponent;

  @ViewChild(ReporteDetalleFacturaPDFComponent, { read: ElementRef })
  private templateReporteDetalleFactura: ReporteDetalleFacturaPDFComponent;

  constructor(private facturasService: FacturasService, private empresaService: EmpresaService, private reportesService: ReportesService) {
    this.factura = new Factura();
    this.date = Date.now();
    this.empresa = new Empresa();
  }

  ngOnInit() {
  }

  loadFactura(id: number) {
    this.modal.show();

    this.facturasService.getFacturaById(id)
      .subscribe(data => {
        this.factura = data;
        this.isLoading = false;
        this.empresaService.getEmpresa(this.factura.id_empresa).subscribe(data => this.empresa = data, err => {});
      },
      err => {
        swal({
          title: 'Ups! Ha ocurrido un problema.',
          text: 'No se ha podido cargar el detalle de la factura.',
          type: 'error'
        });
      }
    );
  }

  exportarPDF() {
    this.reportesService.makeReporteDetalleFactura(this.factura.id).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-facturas-proveedor.pdf');
    },
    error => {
      console.log(error);
    });
  }

}
