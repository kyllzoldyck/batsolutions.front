import { Rol } from "./rol";
import { Empresa } from "./empresa";

export class Usuario {

  public id: number;
  public username: string;
  public nombre: string;
  public apellidos: string;
  public email: string;
  public password: string;
  public id_rol: number;
  public activo: number;
  public eliminado: number;
  public created_at: string;
  public updated_at: string;
  public remember_token: string;
  public id_empresa: number;
  public password_repeat: string;
  public rol: Rol;
  public empresa: Empresa;
  public name_activo: string;

  public constructor(data: any = {}) {
    this.id = data.id || null;
    this.username = data.username || null;
    this.nombre = data.nombre || null;
    this.apellidos = data.apellidos || null;
    this.email = data.email || null;
    this.password = data.password || null;
    this.id_rol = data.id_rol || null;
    this.activo = data.activo || null;
    this.eliminado = data.eliminado || null;
    this.created_at = data.created_at || null;
    this.updated_at = data.updated_at || null;
    this.remember_token = data.remember_token || null;
    this.id_empresa = data.id_empresa || null;
    this.password_repeat = data.password_repeat || null;
    this.rol = data.rol || new Rol();
    this.empresa = data.empresa || new Empresa();
    this.name_activo = data.name_activo ||  null;
  }
}
