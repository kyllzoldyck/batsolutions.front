import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Factura } from '../models/factura';
import { handleError, extractData, getHeaders } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { DetalleFactura } from "../models/detalleFactura";

@Injectable()
export class FacturasService {

    private API_URI = environment.URI;
    IVA: number = 0.19;

    constructor(private http: HttpClient, public router: Router) {}

    /**
    * Envia una factura para ser creada en el servidor
    * @param factura
    */
    addFactura(factura: Factura): Observable<any> {
        let URL = this.API_URI + 'facturas';
        let detalles = [];

        //Recibe articulos y se seleccionan los elementos del detalle solamente.
        // factura.Articulos.forEach(item => {
        //     let detalle: DetalleFactura = new DetalleFactura();
        //     detalle.IdArticulo = item.id;
        //     detalle.Unidades = item.unidades;
        //     detalle.Subtotal = item.subtotal;
        //     detalle.Iva = item.iva;
        //     detalle.Total = item.total

        //     detalles.push(detalle);
        // });

        factura.detalles_factura.forEach( e => {
          let detalle: DetalleFactura = new DetalleFactura();
          detalle.iva = e.iva;
          detalle.unidades = e.unidades;
          detalle.precio = factura.tipo_factura == 2 ? e.precio_de_compra : e.precio_de_venta;
          detalle.descripcion_articulo = e.descripcion;
          detalle.id_articulo = e.id;
          detalle.codigo_articulo = e.codigo;
          detalles.push(detalle);
        });

        let body = {
            numero: factura.numero,
            descripcion: factura.descripcion,
            fecha: factura.fecha,
            tipo_factura: factura.tipo_factura,
            iva: factura.iva,
            detalles_factura: detalles,
            rut: factura.rut,
            id_empresa: factura.id_empresa,
            exenta: factura.exenta,
            giro: factura.giro
        }

        console.log(body);
        return this.http.post(URL, JSON.stringify(body), { headers: getHeaders() });
    }

    getAllFacturas(): Observable<any> {
        let URL = this.API_URI + 'facturas';

        return this.http.get(URL, { headers: getHeaders() });
    }

    getAllFacturasDeCompra(): Observable<Factura[]> {
        let URL = this.API_URI + 'facturas/compra';

        return this.http.get<Factura[]>(URL, { headers: getHeaders() });
        // .map(extractData)
        // .catch(handleError);
    }

    getAllFacturasDeVenta(): Observable<Factura[]> {
        let URL = this.API_URI + 'facturas/venta';

        return this.http.get<Factura[]>(URL, { headers: getHeaders() });
    }

    updateFactura(factura: Factura): Observable<any> {
        let URL = this.API_URI + 'facturas/' + factura.id;
        let body = {
          rut: factura.rut,
          fecha: factura.fecha,
          telefono: factura.telefono,
          email: factura.email,
          descripcion: factura.descripcion,
          id_empresa: factura.id_empresa,
          giro: factura.giro
        }
        return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() });
    }
    /**
     * Elimina una factura dado su Id.
     * @param id Factura
     */
    deleteFacturaById(id: number): Observable<any> {
        let URL = this.API_URI + 'facturas/' + id;

        return this.http.delete(URL, { headers: getHeaders() });
    }

    getFacturaCompraByNumero(numero: number): Observable<any> {
        let URL = this.API_URI + 'facturas/compra/' + numero;

         return this.http.get(URL, { headers: getHeaders() });
    }

    getFacturaVentaByNumero(numero: number): Observable<any> {
        let URL = this.API_URI + 'facturas/venta/' + numero;

         return this.http.get(URL, { headers: getHeaders() });
    }

    getAllFacturasByNumero(numero: number): Observable<any> {
        let URL = this.API_URI + 'facturas/numero/' + numero;
         return this.http.get(URL, { headers: getHeaders() });
    }

    getFacturaById(id: number): Observable<any> {
        let URL = this.API_URI + 'facturas/' + id;

         return this.http.get(URL, { headers: getHeaders() });
    }
}
