import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import { MatTableDataSource, MatPaginator } from "@angular/material";
import { Presupuesto } from "../../models/presupuesto";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { isValidEmail } from "../../shared/email.validation";
import { ClientesService } from "../../services/clientes.service";
import { EmpresaService } from "../../services/empresas.service";
import { Cliente } from "../../models/cliente";
import { Empresa } from "../../models/empresa";
import swal from "sweetalert2";
import { getErrorsForm } from "../../shared/utils";
import { IOption } from "ng-select";
import { ModalArticulosComponent } from "../facturacion/shared/modal-articulos/modal-articulos.component";
import { ArticulosService } from "../../services/articulos.service";
import { Impuesto } from "../../models/impuesto";
import { ImpuestosService } from "../../services/impuestos.services";
import { PresupuestoService } from "../../services/presupuestos.service";
import { DetallePresupuesto } from "../../models/detallePresupuesto";
import { ModalDetallePresupuestoComponent } from "../../shared/reportes/modal.detalle.presupuesto";
import { ModalBasicComponent } from "../../shared/modal-basic/modal-basic.component";
import { AuthService } from "../../services/auth.service";
import { Usuario } from "../../models/usuario";

@Component({
    selector: 'app-presupuestos',
    templateUrl: 'presupuestos.component.html',
    styleUrls: ['presupuestos.component.css']
})
export class PresupuestoComponent implements OnInit, AfterViewInit {

    public usuario: Usuario;
    public clientes: Cliente[];
    public empresas: Empresa[];
    public presupuesto: Presupuesto;
    public empresa: number;
    public cliente: number;
    public fechaCreacion: string = '';
    public fechaExpiracion: string = '';
    public nombre: string = '';
    public email: string = '';
    public asunto: string;
    public descripcion: string;
    public total: number;
    public sub_total: number;
    public iva: number;
    public arrayArticulosAux = [];
    public impuesto: Impuesto;
    public isUpdate: boolean = false;
    public isLoading: boolean = true;
    public clientesOptions: Array<IOption> = new Array<IOption>();
    public showFormState = false;
    public showNuevoPresupuesto = false;
    public editor;
    public editorContent;
    public editorConfig = { placeholder: 'Descripción del presupuesto' };
    public dateToday: Date = new Date(Date.now());
    public destinationMail: string;
    public destinationId: number;

    @ViewChild('buttonSubmit') buttonSubmit: ElementRef;
    @ViewChild('buttonSubmitUpdate') buttonSubmitUpdate: ElementRef;
    @ViewChild(ModalArticulosComponent) modalChild: ModalArticulosComponent;
    @ViewChild('paginator') paginator: MatPaginator;
    @ViewChild('paginatorDetallePresupuestos') paginatorDetallePresupuestos: MatPaginator;
    @ViewChild(ModalDetallePresupuestoComponent) modalDetallePresupuesto: ModalDetallePresupuestoComponent;
    @ViewChild('modalEnviarCorreo') modalEnviarCorreo: ModalBasicComponent;
    @ViewChild('enviarCorreo') enviarCorreo: ElementRef;

    public dataSourcePresupuestos = new MatTableDataSource<Presupuesto>();
    public dataColumnsPresupuestos = ['numero', 'cliente', 'fecha', 'fecha_expiracion', 'total', 'acciones'];

    public dataSourceDetallePresupuestos = new MatTableDataSource<DetallePresupuesto>();
    public dataColumnsDetallePresupuestos = ['descripcion', 'precio_de_venta', 'unidades', 'subtotal', 'accion'];

    public presupuestoForm: FormGroup;

    public formErrors = {
        'empresa': '',
        'cliente': '',
        'fechaExpiracion': '',
        'nombre': '',
        'email': '',
        'asunto': '',
        'descripcion': '',
        'exenta': ''
    };

    public validationMessages = {
        'empresa': {
            'required': 'Debe seleccionar una empresa'
        },
        'cliente': {
            'required': 'Debe seleccionar un Cliente.',
        },
        'fechaExpiracion': {
            'required': 'Debe ingresar una fecha de expiración.',
        },
        'nombre': {
            'required': 'Debe ingresar un nombre.',
            'maxlength': 'El nombre debe ser menor a 100 caracteres.',
            'minlength': 'El nombre debe ser mayor a 5 caracteres.'
        },
        'email': {
            'required': 'Debe ingresar un Email.',
            'isValidEmail': 'Debe ingresar un Email válido.'
        },
        'asunto': {
            'required': 'Debe ingresar un asunto.',
            'minlength': 'El asunto debe ser mayor a 5 caracteres.',
            'maxlength': 'El asunto debe ser menor a 100 caracteres.'
        },
        'descripcion': {
            'required': 'Debe ingresar una descripción.',
            'minlength': 'La descripción debe ser mayor a 5 caracteres.',
            'maxlength': 'La descripción debe ser menor a 500 caracteres.'
        },
        'exenta': {

        }
    };

    public validationMessagesSendEmail = {
        'email': {
            'required': 'Debe ingresar un email.',
            'email': 'Debe ingresar un email válido.'
        }
    };

    public formErrorsSendEmail = {
        'email': ''
    };

    public sendEmailForm: FormGroup;

    constructor(private authService: AuthService, private fb: FormBuilder, private clientesService: ClientesService, private empresasService: EmpresaService, private articulosService: ArticulosService, private impuestosService: ImpuestosService, private presupuestoService: PresupuestoService) {
        this.total = 0;
        this.sub_total = 0;
        this.iva = 0;
        this.presupuesto = new Presupuesto();
        this.usuario = new Usuario();
    }

    ngOnInit() {
        this.dataColumnsPresupuestos.pop();

        this.getAuthenticatedUser();
        this.getIva();
        this.getClientes();
        this.getEmpresas();
        this.buildForm();
        this.buildFormSendEmail();
        this.getPresupuestos();

        setTimeout(() => {
            this.editorContent = this.editorContent;
            // this.editor.disable();
        }, 2800);
    }

    ngAfterViewInit() {
        this.dataSourcePresupuestos.paginator = this.paginator;
        this.dataSourceDetallePresupuestos.paginator = this.paginatorDetallePresupuestos;
    }

    buildFormSendEmail() {
        this.sendEmailForm = this.fb.group({
            email: [
                this.destinationMail,
                Validators.compose([
                    Validators.required,
                    Validators.email
                ])
            ]
        });

        // Cada vez que hay un cambio en el form llama a onValueChanged
        this.sendEmailForm.valueChanges.subscribe(
            data => this.onValueChangedSendEmail(data)
        );

        this.onValueChangedSendEmail();
    }

    buildForm() {

        let empresaValidator = [];

        if (this.usuario.id_rol == 1)
        {
            empresaValidator = [
                this.presupuesto.id_empresa,
                Validators.compose(
                [
                    Validators.required
                ])
            ];
        }

        this.presupuestoForm = this.fb.group({
            empresa: empresaValidator,
            cliente: [
                this.presupuesto.id_cliente,
                Validators.compose(
                    [
                        Validators.required
                    ]
                )
            ],
            fechaExpiracion: [
                this.presupuesto.fecha_expiracion,
                Validators.compose(
                    [
                        Validators.required,
                    ]
                )
            ],
            nombre: [
                this.presupuesto.nombre,
                Validators.compose(
                    [
                        Validators.required,
                        Validators.minLength(5),
                        Validators.maxLength(100)
                    ]
                )
            ],
            email: [
                this.presupuesto.email,
                Validators.compose(
                    [
                        Validators.required,
                        isValidEmail()
                    ]
                )
            ],
            asunto: [
                this.presupuesto.asunto,
                Validators.compose(
                    [
                        Validators.required,
                        Validators.minLength(5),
                        Validators.maxLength(100)
                    ]
                )
            ],
            descripcion: [
                this.presupuesto.descripcion,
                Validators.compose(
                    [
                        Validators.required,
                        Validators.minLength(5),
                        Validators.maxLength(500)
                    ]
                )
            ],
            exenta: []
        });

        // Cada vez que hay un cambio en el form llama a onValueChanged
        this.presupuestoForm.valueChanges.subscribe(
            data => this.onValueChanged(data)
        );

        this.onValueChanged();
    }

    // Cada vez que los valores cambian en el Form, se validan.
    onValueChanged(data?: any) {
        if (!this.presupuestoForm) {
            return;
        }

        const form = this.presupuestoForm;
        for (const field in this.formErrors) {
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    onValueChangedSendEmail(data?: any) {
        if (!this.sendEmailForm) {
            return;
        }

        const form = this.sendEmailForm;
        for (const field in this.formErrorsSendEmail) {
            this.formErrorsSendEmail[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessagesSendEmail[field];
                for (const key in control.errors) {
                    this.formErrorsSendEmail[field] += messages[key] + ' ';
                }
            }
        }
    }

    getIva() {
        this.impuestosService.getImpuestoByNombre('iva').subscribe(
            data => {
                this.impuesto = data;
            },
            error => {
                swal({
                    title: 'Ups! Tenemos un problema',
                    text: 'No se pudo cargar el IVA',
                    type: 'error'
                });
            }
        );
    }

    getAuthenticatedUser() {
        this.authService.getAuthenticatedUser().subscribe(
            data => {

                this.usuario = data;

                if (this.usuario.id_rol != 3) {
                    this.dataColumnsPresupuestos.push('acciones');
                    this.showNuevoPresupuesto = true;
                }
            },
            err => {
                localStorage.removeItem("bat");
                location.reload();
            }
        );
    }

    getClientes() {
        this.clientesService.getAllClientes().subscribe(
            data => {
                this.clientesOptions = [];

                data.forEach(d => {
                    let cliente: IOption = { value: d.rut, label: d.rut + ' | ' + d.razon_social, disabled: false };
                    this.clientesOptions.push(cliente);
                });

            },
            error => {
                swal({
                    title: 'Ups! Tenemos un problema',
                    html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error,
                    type: error.status == 422 ? 'warning' : 'error'
                });
            }
        );
    }

    getEmpresas() {
        this.empresasService.getAllEmpresas().subscribe(
            data => {
                this.empresas = data;
            },
            error => {
                swal(
                    'Ups! Tenemos un problema.',
                    'Lo sentimos, no se han podido cargar las empresas',
                    'error'
                );
            }
        );
    }

    getClientesByEmpresa(e) {
        if (e != null) {
            this.clientesService.getClientesByEmpresa(e)
                .subscribe(
                    data => {
                        this.clientes = data;

                        this.clientesOptions = [];

                        data.forEach(d => {
                            let clienteOption: IOption = { value: d.rut, label: d.rut + ' | ' + d.razon_social, disabled: false };
                            this.clientesOptions.push(clienteOption);
                        });

                    },
                    err => {
                        swal({
                            title: '¡Ups! Lo sentimos.',
                            text: 'No se han podido cargar los porveedores',
                            type: 'error'
                        });
                    }
                );
        } else {
            this.clientes = [];
        }
    }

    getCountPresupuestos() { return 0 }

    LoadAllArticulosModal() {
        if (this.usuario.id_rol == 1) {
            this.modalChild.LoadAllArticulos(true);
        }
        else {
            this.modalChild.LoadAllArticulos(true, this.usuario.id_empresa);
        }
    }

    showAll() {
        this.presupuestoForm.reset();
        this.showFormState = false;
        this.isLoading = true;
        this.getPresupuestos();
    }

    cancel() {
        this.presupuestoForm.reset();
        this.showFormState = true;
        this.isUpdate = false;
        this.arrayArticulosAux = [];
        this.presupuesto = new Presupuesto();
        this.presupuesto.exenta = 0;
        this.dataSourcePresupuestos.data = [];
        this.dataSourceDetallePresupuestos.data = [];
    }

    cancelFromButton() {
        this.presupuestoForm.reset();
        this.arrayArticulosAux = [];
        this.showFormState = false;
        this.isUpdate = false;
        this.dataSourcePresupuestos.data = [];
        this.dataSourceDetallePresupuestos.data = [];
        this.presupuesto = new Presupuesto();
        this.presupuesto.exenta = 0;
        this.total = 0;
        this.sub_total = 0;
        this.iva = 0;
        this.showAll();
    }

    getArticulosFromChild(event): void {
        if (this.arrayArticulosAux.length > 0) {

            event.forEach(element => {
                //Verifica que exista el articulo
                let articuloExistente = this.arrayArticulosAux.filter(a => {
                    return a.id_articulo == element.id_articulo
                });

                if (articuloExistente.length != 0) {
                    //Obtiene el indice del articulo que existe.
                    let index = this.arrayArticulosAux.indexOf(articuloExistente[0], 0)

                    //Replaza el articulo.
                    if (index != -1)
                        this.arrayArticulosAux.splice(index, 1, element)

                } else
                    //Si no existe, lo agrega al final.
                    this.arrayArticulosAux.push(element);
            });
        } else
            this.arrayArticulosAux = event;

        this.dataSourceDetallePresupuestos.paginator = this.paginatorDetallePresupuestos;
        this.dataSourceDetallePresupuestos.data = this.arrayArticulosAux;
        this.calculateTotal();
    }

    calculateTotal() {
        // this.sub_total = 0;
        // this.iva = 0;
        // this.total = 0;

        // this.arrayArticulosAux.forEach(articulo => {
        //     this.sub_total += articulo.subtotal;
        // });
        // this.dataSourceDetallePresupuestos.data = this.arrayArticulosAux;
        // this.iva = + (this.sub_total * this.impuesto.valor);
        // this.total = + (this.sub_total + this.iva);

        // this.iva = Math.round(this.iva);
        // this.sub_total = Math.round(this.sub_total);
        // this.total = Math.round(this.total);


        this.presupuesto.sub_total = 0;
        this.presupuesto.iva = 0;
        this.presupuesto.total = 0;
        this.arrayArticulosAux.forEach(articulo => {
            this.presupuesto.sub_total += articulo.subtotal;
        });

        this.presupuesto.detalles_presupuesto = this.arrayArticulosAux;
        this.presupuesto.iva = this.presupuesto.exenta == 1 ? 0 : +(this.presupuesto.sub_total * this.impuesto.valor);
        this.presupuesto.total = +(this.presupuesto.sub_total + this.presupuesto.iva);

        this.presupuesto.iva = this.presupuesto.exenta == 1 ? 0 : Math.round(this.presupuesto.iva);
        this.presupuesto.sub_total = Math.round(this.presupuesto.sub_total);
        this.presupuesto.total = Math.round(this.presupuesto.total);

    }

    deleteArticuloPresupuesto(event): void {
        swal({
            title: `¿Estás seguro?`,
            text: `Se eliminará el articulo: ${event.descripcion_articulo}`,
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar'
        }).then(result => {
            if (result.value) {
                let index = this.dataSourceDetallePresupuestos.data.findIndex(d => d.id = event.id);

                if (index > -1) this.dataSourceDetallePresupuestos.data.splice(index, 1);

                // this.dataSourceDetallePresupuestos.data = this.dataSourceDetallePresupuestos.data;
                this.calculateTotal();
            }
        });
    }

    getPresupuestos() {

        this.isLoading = true;
        this.dataSourcePresupuestos.data = [];

        this.presupuestoService.getPresupuestos().subscribe(data => {
            console.log(data);
            setTimeout(() => {
                this.isLoading = false;
                this.dataSourcePresupuestos.data = data;
            }, 1000);
        },
            error => {
                this.isLoading = false;
                swal({
                    title: 'Ups! Tenemos un problema',
                    html: 'No se han podido cargar los presupuestos',
                    type: 'warning'
                });
            });
    }

    onSubmit() {

        this.presupuesto.detalles_presupuesto = this.dataSourceDetallePresupuestos.data;
        this.buttonSubmit.nativeElement.disabled = true;

        this.presupuestoService.addPresupuesto(this.presupuesto).subscribe(data => {
            this.buttonSubmit.nativeElement.disabled = false;

            swal({
                title: '¡Presupuesto creado!',
                text: 'El presupuesto ha sido creado correctamente',
                type: 'success'
            }).then(() => this.cancelFromButton());
        },
            error => {
                this.buttonSubmit.nativeElement.disabled = false;
                console.log(error);
                swal({
                    title: 'Ups! Tenemos un problema',
                    html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error.error,
                    type: error.status == 422 || error.status == 204 ? 'warning' : 'error'
                });
            });
    }

    onUpdate() {
        this.presupuesto.detalles_presupuesto = this.dataSourceDetallePresupuestos.data;
        this.buttonSubmitUpdate.nativeElement.disabled = true;

        this.presupuestoService.updatePresupuest(this.presupuesto).subscribe(data => {
            this.buttonSubmitUpdate.nativeElement.disabled = false;

            swal({
                title: '¡Presupuesto Actualizado!',
                text: 'El presupuesto ha sido actualizado correctamente',
                type: 'success'
            }).then(() => this.cancelFromButton());
        },
            error => {
                this.buttonSubmitUpdate.nativeElement.disabled = false;

                swal({
                    title: 'Ups! Tenemos un problema',
                    html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error,
                    type: error.status == 422 || error.status == 204 ? 'warning' : 'error'
                });
            });
    }

    findPresupuestoById(id) {
        this.presupuestoService.getPresupuesto(id).subscribe(data => {
            this.showFormState = true;
            this.isUpdate = true;

            data.fecha_expiracion = data.fecha_expiracion.split(' ')[0];

            this.presupuesto = data;
            this.arrayArticulosAux = this.presupuesto.detalles_presupuesto;
            this.dataSourceDetallePresupuestos.data = this.presupuesto.detalles_presupuesto;
        },
            error => {
                swal({
                    title: 'Ups! Tenemos un problema',
                    html: error.status == 422 ? getErrorsForm(error.error.errors) : error.error,
                    type: error.status == 422 || error.status == 204 ? 'warning' : 'error'
                });
            });
    }

    deletePresupuesto(element) {

        swal({
            title: `¿Estás seguro?`,
            text: `Se eliminará el presupuesto número: ${element.numero }`,
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar'
        })
            .then(result => {
                if (result.value) {
                    this.presupuestoService.deletePresupuesto(element.id).subscribe(
                        res => {
                            swal(
                                'Presupuesto Eliminado!',
                                'El presupuesto ha sido eliminado correctamente!',
                                'success'
                            ).then(() => {
                                this.isLoading = true;
                                this.getPresupuestos();
                            });
                        },
                        error => {
                            swal({
                                title: 'Ups! Tenemos un problema',
                                text: error.error,
                                type: 'error'
                            });
                        }
                    );
                }
            });
    }

    showDetallePresupuesto(id: number) {
        this.modalDetallePresupuesto.loadPresupuesto(id);
    }

    showModalEnviarCorreo(id: number) {
        this.presupuestoService.getDefaultEmail(id).subscribe(data => {
            this.destinationId = id;
            this.destinationMail = data;
            this.modalEnviarCorreo.show();
        },
        error => { });
    }

    sendPresupuestoEmail() {
        this.enviarCorreo.nativeElement.disabled = true;

        this.presupuestoService.sendPresupuestoEmail(this.destinationId, this.destinationMail).subscribe(data => {
            setTimeout(() => {
                this.enviarCorreo.nativeElement.disabled = false;
                this.modalEnviarCorreo.hide();
                swal('¡Muy Bien!', 'Se ha enviado el presupuesto correctamente.', 'success');
            }, 1000);
        },
        error => {
            this.enviarCorreo.nativeElement.disabled = false;
            swal('¡Lo sentimos!', 'No hemos podido enviar el correo. Intentelo más tarde.', 'warning');
        });
    }

    hideModal()
    {
      this.modalEnviarCorreo.hide();
    }

    changeExenta() {
        this.calculateTotal();
    }
}
