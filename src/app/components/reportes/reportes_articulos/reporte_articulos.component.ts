import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { EmpresaService } from '../../../services/empresas.service';
import { Empresa } from '../../../models/empresa';
import { AuthService } from '../../../services/auth.service';
import { Usuario } from '../../../models/usuario';
import { ProveedoresService } from '../../../services/proveedores.service';
import { Proveedor } from '../../../models/proveedor';
import { Articulo } from '../../../models/articulo';
import { ArticulosService } from '../../../services/articulos.service';
import { ReportesService } from '../../../services/reportes.service';
import { ExcelService } from '../../../services/excel.service';
import { ReporteArticulosPDFComponent } from '../../../shared/reportes/reporte.articulos';
import { getErrorsForm } from '../../../shared/utils';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import swal from 'sweetalert2';
import { IOption } from 'ng-select';
import { saveAs } from 'file-saver/FileSaver';

declare var xepOnline: any;

@Component({
  selector: 'app-reporte-articulos',
  templateUrl: './reporte_articulos.component.html',
  styleUrls: ['./reporte_articulos.component.css']
})
export class ReporteArticulosComponent implements OnInit, AfterViewInit {

  public proveedoresOptions: Array<IOption> = new Array<IOption>();
  public empresas: Empresa[];
  public proveedores: Proveedor[];
  public proveedor: Proveedor;
  public empresa: Empresa;
  public usuario: Usuario;
  public isLoading: boolean = false;
  public articulos: Articulo[];
  public articulosAux: Articulo[];
  public id_empresa: number = null;
  public showAlertNotFound = true;
  public visible = false;
  public visibleAnimate = false;

  @ViewChild('btnExportExcel') btnExportExcel: ElementRef;
  @ViewChild('btnExportPdf') btnExportPdf: ElementRef;
  @ViewChild('empresa') empresaElement: ElementRef;

  @ViewChild(ReporteArticulosPDFComponent, { read: ElementRef })
  private templateReporteArticulo: ReporteArticulosPDFComponent;

  @ViewChild('paginatorReporteArticulos') paginatorReporteArticulos: MatPaginator;
  columnsArticulos = ['descripcion', 'proveedor', 'precio_de_compra', 'precio_de_venta', 'stock'];
  dataSourceArticulos = new MatTableDataSource<Articulo>();

  constructor(private excelService: ExcelService, private empresasService: EmpresaService, private authService: AuthService, private proveedoresService: ProveedoresService, private articulosService: ArticulosService, private reportesService: ReportesService) {
    this.empresas = [];
    this.proveedores = [];
    this.articulos = [];
    this.articulosAux = [];
    this.empresa = new Empresa();
    this.usuario = new Usuario();
    this.proveedor = new Proveedor();
    this.id_empresa = 0;
  }

  ngOnInit() {
    this.templateReporteArticulo = new ReporteArticulosPDFComponent();
    this.getAuthenticatedUser();
    this.getEmpresas();
  }

  ngAfterViewInit() {
    this.dataSourceArticulos.paginator = this.paginatorReporteArticulos;
    this.btnExportExcel.nativeElement.disabled = true;
    this.btnExportPdf.nativeElement.disabled = true;
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.usuario = data;

        if (this.usuario.id_rol != 1) {
          this.getProveedores();

        }
        
        this.empresa = this.usuario.empresa;

      }, err => {
        localStorage.removeItem('bat');
        location.reload();
      });
  }

  getReporteArticulos() {

    this.isLoading = true;
    this.btnExportExcel.nativeElement.disabled = true;
    this.btnExportPdf.nativeElement.disabled = true;
    this.dataSourceArticulos.data = [];
    this.showAlertNotFound = false;

    let empresa = 0;
    let proveedor = 0;

    if (this.usuario.id_rol == 1) {
      empresa = this.id_empresa;
    }

    if (this.proveedor.id != null)
      proveedor = this.proveedor.id;

    this.reportesService.getReporteArticulos(proveedor, empresa)
      .subscribe(data => {

        this.dataSourceArticulos.data = data;
        this.articulos = data;

        if (this.articulos.length == 0) {
          this.btnExportExcel.nativeElement.disabled = true;
          this.btnExportPdf.nativeElement.disabled = true;
        }
        else {
          this.btnExportExcel.nativeElement.disabled = false;
          this.btnExportPdf.nativeElement.disabled = false;
        }

        this.isLoading = false;
        this.showAlertNotFound = false;

        if (this.articulos.length == 0) {
          this.showAlertNotFound = true;
        }
      }, err => {
        this.isLoading = false;

        swal({
          title: 'Ups! Tenemos un problema',
          html: getErrorsForm(err.error.errors),
          type: 'error'
        });

        this.btnExportExcel.nativeElement.disabled = true;
        this.btnExportPdf.nativeElement.disabled = true;
      }
      );
  }

  /**
   * Llena el select con los proveedores según la empresa seleccionada
   * @param e ID de la empresa
   */
  getProveedoresByEmpresa() {

    this.proveedoresOptions = [];
    this.empresaElement.nativeElement.disabled = true;

    this.proveedoresService
      .getProveedoresByEmpresa(this.id_empresa)
      .subscribe(
        data => {
          this.empresaElement.nativeElement.disabled = false;
          this.proveedores = data;

          let proveedorOption: IOption[] = [];

          data.forEach(p => {
            proveedorOption.push({ value: p.id, label: p.rut + ' | ' + p.razon_social, disabled: false });
          });

          this.proveedoresOptions = proveedorOption;
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un problema',
            text: 'No se han podido cargar los proveedores.',
            type: 'error'
          });
          this.empresaElement.nativeElement.disabled = false;
        }
    );

    if (this.id_empresa != 0)
    {
      this.empresasService.getEmpresa(this.id_empresa)
      .subscribe(empresa => {
        this.empresa = empresa;
      }, err => { });
    }
    else 
    {
      this.proveedor = new Proveedor();
      this.empresa = new Empresa();
      this.empresa.id = 0;
    }
  }

  getProveedores() {
    this.proveedoresService
      .getAllProveedores()
      .subscribe(
        data => {
          this.proveedores = data;
          let proveedorOption: IOption[] = [];
          
          data.forEach(d => {
            proveedorOption.push({ value: d.id.toString(), label: d.rut + ' | ' + d.razon_social, disabled: false });
          });

          this.proveedoresOptions = proveedorOption;
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un problema',
            text: 'No se han podido cargar los proveedores.',
            type: 'error'
          });
        }
      );
  }

  getEmpresas() {
    this.empresasService.getAllEmpresas()
      .subscribe(
        data => {
          this.empresas = data;
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un problema',
            text: err.error || getErrorsForm(err.errors),
            type: 'error'
          });
        }
      );
  }

  getEmpresa() {
    this.empresasService.getEmpresa(this.empresa.id)
      .subscribe(data => {
        this.empresa = data;
      }, err => {
        swal({
          title: 'Ups! Ha ocurrido un error',
          text: 'No se ha podido cargar la empresa',
          type: 'error'
        });
      }
      );
  }

  searchArticulosByProveedor() {
    this.isLoading = true;
    this.proveedoresService.getProveedoresByEmpresa(this.empresa.id)
      .subscribe(data => {
        this.isLoading = false;
        this.proveedores = data;
        this.dataSourceArticulos.data = data;

        if (this.dataSourceArticulos.data.length == 0) {
          this.btnExportExcel.nativeElement.disabled = true;
          this.btnExportPdf.nativeElement.disabled = true;
        }
        else {
          this.btnExportExcel.nativeElement.disabled = false;
          this.btnExportPdf.nativeElement.disabled = false;
        }
      },
        err => {
          this.isLoading = false;
          swal({
            title: 'Ups! Ha ocurrido un error.',
            html: getErrorsForm(err.error.errors),
            type: 'warning'
          });
        }
      );
  }

  fillArrayWithArticulo(e) {
    this.articulosAux = [];

    e.selected.forEach(c => {
      this.articulosAux.push(c);
    });
  }
  exportExcel() {

    let articulosAux = [];

    this.dataSourceArticulos.data.forEach((d: Articulo) => {
      articulosAux.push({ 'codigo': d.codigo, 'descripcion': d.descripcion, 'eliminado': d.eliminado == 1 ? 'si' : 'no', 'precio_de_compra': d.precio_de_compra, 'precio_de_venta': d.precio_de_venta, 'stock': d.stock, 'proveedor': d.proveedor.razon_social });
    });

    this.excelService.exportAsExcelFile(articulosAux, 'Informe Artículos');
  }

  descargarPdf() {
    this.reportesService.makeReporteArticulos(this.dataSourceArticulos.data, this.id_empresa).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-articulos.pdf');
    },
    error => {
      swal({
        title: 'Ups! Ha ocurrido un error.',
        html: 'No se ha podido generar el documento. Intentelo más tarde.',
        type: 'error'
      });
    });
  }

  public show(): void {
    this.visible = true;
    setTimeout(() => (this.visibleAnimate = true), 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => (this.visible = false), 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains("modal")) {
      this.hide();
    }
  }

}
