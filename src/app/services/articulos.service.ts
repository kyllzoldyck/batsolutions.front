import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Articulo } from '../models/articulo';
import { handleError, extractData, getHeaders } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { Proveedor } from '../models/proveedor';
import { DecimalsPipe } from '../shared/pipes/decimals.pipe';

@Injectable()
export class ArticulosService {
  private API_URI = environment.URI;

  constructor(private http: HttpClient, private router: Router,  private decimalsPipe: DecimalsPipe) { }

  /**
   * Funcion encargada de obtener todos los articulos desde el Servidor.
   */
  getAllArticulos(): Observable<any> {
    let URL = this.API_URI + 'articulos';

    return this.http.get(URL, { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }

  /**
   * Funcion encargada de obtener un articulo especifico desde el Servidor.
   */
  getArticuloById(id: number): Observable<any> {
    let URL = this.API_URI + 'articulos/' + id;

    return this.http.get(URL, { headers: getHeaders() })
      .map(extractData)
      .catch(handleError);
  }

  /**
   * Agrega un articulo enviandolo al Servidor.
   * @param articulo
   */
  addArticulo(articulo: Articulo): Observable<any> {
    let URL = this.API_URI + 'articulos';

    let body = {
      id_proveedor: articulo.id_proveedor,
      descripcion: articulo.descripcion,
      precio_de_compra: articulo.precio_de_compra,
      precio_de_venta: articulo.precio_de_venta,
      stock: articulo.stock,
      codigo: articulo.codigo
    };

    return this.http.post(URL, JSON.stringify(body), { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }

  /**
   * Actualiza un articulo enviandolo al servidor.
   * @param articulo
   */
  updateArticulo(articulo: Articulo): Observable<any> {
    let URL = this.API_URI + 'articulos/' + articulo.id;

    let body = {
      id_proveedor: articulo.id_proveedor,
      descripcion: articulo.descripcion,
      precio_de_compra: articulo.precio_de_compra,
      precio_de_venta: articulo.precio_de_venta,
      stock: articulo.stock,
      codigo: articulo.codigo
    };

    return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }

  /**
   * Elimina un articulo dado su Id.
   * @param id Articulo
   */
  deleteArticulo(id: Number): Observable<any> {
    let URL = this.API_URI + 'articulos/' + id;

    return this.http.delete(URL, { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }

    /**
   * Permite formatear un numero a CLP y coma en caso decimal.
   * @param value
   */
  formatDecimalToCLP(value, removeSymbol?: boolean) {
    if (value == 0)
      return 0;

    let formatted = this.decimalsPipe.transform(value);
    //Si la parte decimal es mayor a cero
    let decimalPart = formatted.split('.')[1]
    let intPart = formatted.split('.')[0]

    let symbol = removeSymbol ? ' ' : '$ '

    if (+decimalPart > 0)
      return symbol + formatted.replace('.', ',').split(' ').join('.')
    else
      return symbol + intPart.split(' ').join('.')
  }
}
