export class Empresa {

    public id: number;
    public name: string;
    public state: number;
    public razon_social: string;
    public rut: string;
    public url_img: string;
    public created_at: Date;
    public updated_at: Date;

    constructor(data: any = {}) {
        this.id = data.id || null;
        this.name = data.name || null;
        this.state = data.state || null;
        this.razon_social = data.razon_social || null;
        this.rut = data.rut || null;
        this.url_img = data.url_img || null;
        this.created_at = data.created_at || null;
        this.updated_at = data.updated_at || null;
    }

}
