import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PresupuestoRoutingModule } from "./presupuestos-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { PresupuestoService } from "../../services/presupuestos.service";
import { ClientesService } from "../../services/clientes.service";
import { PresupuestoComponent } from "./presupuestos.component";
import { SharedModule } from "../../shared/shared.module";
import { MatPaginatorModule, MatTableModule, MatProgressSpinnerModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatPaginatorIntl } from "@angular/material";
import { EmpresaService } from "../../services/empresas.service";
import { ThousandsPipeModule } from "../../shared/pipes/thousands.module";
import { DecimalsPipeModule } from "../../shared/pipes/decimals.module";
import { ModalArticulosComponent } from "../facturacion/shared/modal-articulos/modal-articulos.component";
import { InputUnidadesComponent } from "../facturacion/shared/input-unidades/input-unidades.component";
import { ArticulosService } from "../../services/articulos.service";
import { DecimalsPipe } from "../../shared/pipes/decimals.pipe";
import { ThousandsSeparatorPipe } from "../../shared/pipes/thousands.separator.pipe";
import { ImpuestosService } from "../../services/impuestos.services";
import { ModalArticulosModule } from "../facturacion/shared/modal-articulos/modal-articulos.module";
import { ModalDetallePresupuestoComponent } from "../../shared/reportes/modal.detalle.presupuesto";
import { ReportesService } from "../../services/reportes.service";
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";

@NgModule({
    declarations: [
        PresupuestoComponent,
        ModalDetallePresupuestoComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        PresupuestoRoutingModule,
        SharedModule,
        MatPaginatorModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        ThousandsPipeModule,
        DecimalsPipeModule,
        ModalArticulosModule
    ],
    providers: [
        ClientesService,
        EmpresaService,
        PresupuestoService,
        ArticulosService,
        DecimalsPipe,
        ThousandsSeparatorPipe,
        ImpuestosService,
        ReportesService,
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class PresupuestoModule { }