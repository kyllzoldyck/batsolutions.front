import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';

import { AuthService } from '../../services/auth.service';
import { AuthGuard } from '../auth.guard';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [AuthService, AuthGuard]
})
export class LoginModule { }
