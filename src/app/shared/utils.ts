import { Observable } from 'rxjs/Observable';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

export function handleError(error) {
  let errorBody = error.json();
  let errMsg;
  switch (error.status) {
    case 0: {
      errMsg = 'No fue posible establecer conexión con el servidor.';
      break;
    }
    case 500: {
      errMsg = 'Algo anda mal con el servidor, inténtalo nuevamente.';
    }
    default: {
      errMsg = errorBody.Message || errorBody;
      console.log(errMsg)
    }
  }
  console.error(`${error.status}-${error.statusText}: ${errMsg}`);

  return Observable.throw(errMsg);
}

/**
 * Extrae los datos desde la respuesta del servidor.
 * @param res
 */
export function extractData(res) {
  let body = res.json() || null;
  return body;
}

/**
 * Obtiene las cabeceras para realizar las peticiones.
 */
export function getHeaders(additional: any[] = []): HttpHeaders {
  let headers = new HttpHeaders();
  let bearer = 'Bearer ';

  if (window.localStorage) {
    bearer += localStorage.getItem('bat');
  }
  
  headers = headers.append('Authorization', bearer);
  headers = headers.append('Content-Type', 'application/json; charset=utf-8');

  additional.forEach(a => {
    headers = headers.append(a.key, a.value);
  });
  
  return headers;
}

/**
 * OBtiene las cabeceras para oauth
 */
export function getHeadersOauth(): HttpHeaders {
  let headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  return headers;
}

export function getErrorsForm(errors): string {
  let message = '';
  for (let i in errors) {
    message += errors[i] + '<br>';
  }
  return message ? message : null;
}
export function dateFormat(texto) {
  return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g, '$3-$2-$1');
}
