import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { ArticulosComponent } from "./articulos.component";
import { ArticulosService } from "../../services/articulos.service";
import { EmpresaService } from "../../services/empresas.service";
import { ArticuloRoutingModule } from "./articulos-routing.module";
import { ProveedoresService } from "../../services/proveedores.service";
import { DecimalsPipe } from "../../shared/pipes/decimals.pipe";
import { ThousandsSeparatorPipe } from "../../shared/pipes/thousands.separator.pipe";
import { MatTableModule, MatPaginatorModule, MatProgressSpinnerModule, MatPaginatorIntl } from "@angular/material";
import { ThousandsPipeModule } from "../../shared/pipes/thousands.module";
import { DecimalsPipeModule } from "../../shared/pipes/decimals.module";
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule,
        ArticuloRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        ThousandsPipeModule,
        DecimalsPipeModule
    ],
    declarations: [ 
        ArticulosComponent
    ],
    providers: [ 
        ArticulosService, 
        EmpresaService, 
        ProveedoresService,
        DecimalsPipe,
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class ArticuloModule { }