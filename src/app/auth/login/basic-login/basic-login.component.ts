import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../models/usuario';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { getErrorsForm } from '../../../shared/utils';
import { ToastData, ToastOptions, ToastyService } from 'ng2-toasty';
import swal from "sweetalert2";

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {

  tryLogin: boolean = false;
  tryChangePass: boolean = false;
  usuario: Usuario = new Usuario()
  loginForm: FormGroup;
  passChangeForm: FormGroup;
  passValidation: string;
  isLoggedIn = true;
  isLogged = true;

  constructor(    
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router,
    private toastyService: ToastyService
  ) { }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
    this.getAuthenticatedUser();
    this.buildLoginForm();
  }

  /* VALIDACIONES LOGIN */
  formErrorsLogin = {
    'username': '',
    'password': '',
  };

  validationMessagesLogin = {
    'username': {
      'required': 'Debe ingresar un Nombre de Usuario',
    },
    'password': {
      'required': 'Debe ingresar una Contraseña',
    }
  };

  /* VALIDACIONES CAMBIAR PASSWORD */
  formErrorsPassChange = {
    'username': '',
    'password': '',
    'passwordValidate': ''
  };

  validationMessagesPassChange = {
    'username': {
      'required': 'Debe ingresar un Nombre de Usuario.',
    },
    'password': {
      'required': 'Debe ingresar una Contraseña',
    },
    'passwordValidate': {
      'required': 'Debe repertir la Contraseña',
    }
  };
  /**
   * Construye el formulario y sus validaciones
   */
  buildLoginForm() {
    this.loginForm = this.fb.group({
      username: [this.usuario.username, Validators.compose([
        Validators.required
      ])],
      password: [this.usuario.password, Validators.compose([
        Validators.required
      ])]
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.loginForm.valueChanges.subscribe(
      data => this.onValueChangedLogin(data)
    );

    this.onValueChangedLogin();
  }

  buildChangePassForm() {
    this.passChangeForm = this.fb.group({
      username: [this.usuario.username, Validators.compose([
        Validators.required
      ])],
      password: [this.usuario.password, Validators.compose([
        Validators.required
      ])],
      passwordValidate: [this.passValidation, Validators.compose([
        Validators.required
      ])]
    });

    // Cada vez que hay un cambio en el form llama a onValueChanged
    this.passChangeForm.valueChanges.subscribe(
      data => this.onValueChangedPassChange(data)
    );

    this.onValueChangedPassChange();
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChangedLogin(data?: any) {
    if (!this.loginForm) {
      return;
    }

    const form = this.loginForm;
    for (const field in this.formErrorsLogin) {
      this.formErrorsLogin[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessagesLogin[field];
        for (const key in control.errors) {
          this.formErrorsLogin[field] += messages[key] + ' ';
        }
      }
    }
  }

  // Cada vez que los valores cambian en el Form, se validan.
  onValueChangedPassChange(data?: any) {
    if (!this.passChangeForm) {
      return;
    }

    const form = this.passChangeForm;
    for (const field in this.formErrorsPassChange) {
      this.formErrorsPassChange[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessagesPassChange[field];
        for (const key in control.errors) {
          this.formErrorsPassChange[field] += messages[key] + ' ';
        }
      }
    }
  }

  onLogin() {
    this.tryLogin = true;
    
    this.authService.login(this.usuario).subscribe(
      r => { 
        if (window.localStorage) {
          localStorage.setItem('bat', r.token);
        }
        
        this.isLoggedIn = true;
        this.router.navigate(['/dashboard/dashboard-default']);
      },
      err => {
        console.log(err);
        swal({
          title: 'Ups! Tenemos un problema',
          html: err.status == 422 ? getErrorsForm(err.error.errors) : err.error.error,
          type: err.status == 422 ? 'warning' : 'error'
        });

        this.tryLogin = false;
      });
  }

  onPassChange() {
    this.tryChangePass = true;

    if (this.usuario.password == this.passValidation) {
      
      this.usuario.password_repeat = this.passValidation;
      
      this.authService.changePassword(this.usuario).subscribe(
        res => {
          
          this.tryChangePass = false;

          // swal(
          //   '¡Contraseña Actualizada!',
          //   'La contraseña se actualizó correctamente.',
          //   'success'
          // ).then(
          //   jQuery("#changePassModal").modal("hide")
          // )
        },
        err => {
          this.tryChangePass = false;
          // swal({
          //   title: 'Ups! Tenemos un problema',
          //   text: err.error || getErrorsForm(err.errors),
          //   type: 'error'
          // });
        }
      )
    } else {
      this.tryChangePass = false;
      // swal(
      //   '¡Ups! Tenemos un problema',
      //   'Las contraseñas ingresadas no coinciden.',
      //   'error'
      // )
    }
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.isLoggedIn = true;
        this.isLogged = true;
      }, err => {
        localStorage.removeItem('bat');
        this.isLoggedIn = false;
        this.isLogged = false;
      });
  }
}
