import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from '@angular/forms';
import { DecimalsPipe } from "../../shared/pipes/decimals.pipe";
import { ClientesService } from "../../services/clientes.service";
import { ProveedoresService } from "../../services/proveedores.service";
import { FacturasService } from "../../services/facturas.service";
import { EmpresaService } from "../../services/empresas.service";
import { ArticulosService } from "../../services/articulos.service";
import { ReporteRoutingModule } from "./reporte-routing.module";
import { ReporteProveedoresComponent } from "./reportes_proveedores/reporte_proveedores.component";
import { ReporteArticulosComponent } from "./reportes_articulos/reporte_articulos.component";
import { ReporteClientesComponent } from "./reportes_clientes/reporte_clientes.component";
import { FacturasClientesComponent } from "./reportes_facturas/clientes/facturas_clientes.component";
import { FacturasProveedoresComponent } from "./reportes_facturas/proveedores/facturas_proveedores.component";
import { MatTableModule, MatPaginatorModule, MatProgressSpinnerModule, MatPaginatorIntl, MatCheckboxModule } from "@angular/material";
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";
import { ReporteArticulosPDFComponent } from "../../shared/reportes/reporte.articulos";
import { ReporteProveedoresPDFComponent } from "../../shared/reportes/reporte.proveedores";
import { ReporteDetalleFacturaPDFComponent } from "../../shared/reportes/reporte.detalle.factura";
import { ReporteFacturasClientePDFComponent } from "../../shared/reportes/reporte.facturas.cliente";
import { ReporteFacturasProveedorPDFComponent } from "../../shared/reportes/reporte.facturas.proveedor";
import { ReporteClientesPDFComponent } from "../../shared/reportes/reporte.clientes";
import { ModalDetalleFacturaComponent } from "../../shared/reportes/modal.detalle.factura";
import { ThousandsSeparatorPipe } from "../../shared/pipes/thousands.separator.pipe";
import { ExcelService } from "../../services/excel.service";
import { ReportesService } from "../../services/reportes.service";
import { ThousandsPipeModule } from "../../shared/pipes/thousands.module";
import { DecimalsPipeModule } from "../../shared/pipes/decimals.module";
import { ReporteComponent } from "./reportes.component";

@NgModule({
    declarations: [
        ReporteComponent,
        ReporteProveedoresComponent,
        ReporteArticulosComponent,
        ReporteClientesComponent,
        FacturasClientesComponent,
        FacturasProveedoresComponent,
        ReporteArticulosPDFComponent,
        ReporteProveedoresPDFComponent,
        ReporteDetalleFacturaPDFComponent,
        ReporteFacturasClientePDFComponent,
        ReporteFacturasProveedorPDFComponent,
        ReporteClientesPDFComponent,
        ModalDetalleFacturaComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule,
        MatTableModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        ReporteRoutingModule,
        ThousandsPipeModule,
        DecimalsPipeModule
    ],
    providers: [
        DatePipe,
        DecimalsPipe,
        ClientesService,
        ProveedoresService,
        FacturasService,
        EmpresaService,
        ArticulosService,
        ExcelService,
        ReportesService,
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class ReporteModule {}