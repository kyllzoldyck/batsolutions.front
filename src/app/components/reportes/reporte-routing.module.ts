import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReporteArticulosComponent } from "./reportes_articulos/reporte_articulos.component";
import { ReporteProveedoresComponent } from "./reportes_proveedores/reporte_proveedores.component";
import { ReporteClientesComponent } from "./reportes_clientes/reporte_clientes.component";
import { FacturasProveedoresComponent } from "./reportes_facturas/proveedores/facturas_proveedores.component";
import { FacturasClientesComponent } from "./reportes_facturas/clientes/facturas_clientes.component";

const routes: Routes = [
    {
        path: 'articulos',
        component: ReporteArticulosComponent,
        data: {
            breadcrumb: 'Reporte de Artículos',
            icon: 'fa fa-truck',
            status: false
        }
    },
    {
        path: 'proveedores',
        component: ReporteProveedoresComponent,
        data: {
            breadcrumb: 'Reporte de Proveedores',
            icon: 'fa fa-truck',
            status: false
        }
    },
    {
        path: 'clientes',
        component: ReporteClientesComponent,
        data: {
            breadcrumb: 'Reporte de Clientes',
            icon: 'fa fa-truck',
            status: false
        }
    },
    {
        path: 'facturas-proveedores',
        component: FacturasProveedoresComponent,
        data: {
            breadcrumb: 'Reporte de Facturas Proveedor',
            icon: 'fa fa-truck',
            status: false
        }
    },
    {
        path: 'facturas-clientes',
        component: FacturasClientesComponent,
        data: {
            breadcrumb: 'Reporte de Facturas Cliente',
            icon: 'fa fa-truck',
            status: false
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReporteRoutingModule { }