import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Cliente } from '../../../models/cliente';
import { Empresa } from '../../../models/empresa';
import { Usuario } from '../../../models/usuario';
import { AuthService } from '../../../services/auth.service';
import { ReportesService } from '../../../services/reportes.service';
import { EmpresaService } from '../../../services/empresas.service';
import { ExcelService } from '../../../services/excel.service';
import { ClientesService } from '../../../services/clientes.service';
import { ReporteClientesPDFComponent } from '../../../shared/reportes/reporte.clientes';
import * as $ from 'jquery';
import { saveAs } from 'file-saver/FileSaver';
import { getErrorsForm } from '../../../shared/utils';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { environment } from '../../../../environments/environment';
import swal from 'sweetalert2';

@Component({
  selector: 'app-reporte-clientes',
  templateUrl: './reporte_clientes.component.html',
  styleUrls: ['./reporte_clientes.component.css']
})
export class ReporteClientesComponent implements OnInit, AfterViewInit {

  public clientes: Cliente[];
  public clientesAux: Cliente[];
  public empresas: Empresa[];
  public usuario: Usuario;
  public empresa: Empresa;
  public isLoading: boolean = true;
  public hideReport: boolean = false;
  public showAlertNotFound = false;
  public nameFilePdf: string = '';
  public pathPdf: string = environment.PATH_DOCUMENTS_PDF;
  public showModalPdf: boolean = false;

  @ViewChild('btnExportExcel') btnExportExcel: ElementRef;
  @ViewChild('btnExportPdf') btnExportPdf: ElementRef;
  @ViewChild('reporte') reporte: ElementRef;
  @ViewChild(ReporteClientesPDFComponent, { read: ElementRef })
  private templateReporteClientes: ReporteClientesPDFComponent;

  @ViewChild('paginatorReporteClientes') paginatorReporteClientes: MatPaginator;
  public columnsClientes = ['rut', 'razon_social', 'nombre_contacto', 'direccion', 'telefono', 'email'];
  public dataSourceClientes = new MatTableDataSource<Cliente>();

  constructor(private excelService: ExcelService, private reportesService: ReportesService, private empresasService: EmpresaService, private authService: AuthService, private clientesService: ClientesService) {
    this.clientes = [];
    this.clientesAux = [];
    this.empresas = [];
    this.empresa = new Empresa();
    this.usuario = new Usuario();
  }

  ngOnInit() {

    this.templateReporteClientes = new ReporteClientesPDFComponent();

    this.btnExportExcel.nativeElement.disabled = true;
    this.btnExportPdf.nativeElement.disabled = true;

    this.getAuthenticatedUser();
    this.getEmpresas();
    this.getReporteClientes();
  }

  ngAfterViewInit() {
    this.dataSourceClientes.paginator = this.paginatorReporteClientes;
  }

  getAuthenticatedUser() {
    this.authService.getAuthenticatedUser()
      .subscribe(data => {
        this.usuario = data;

        if (this.usuario.id_rol != 1)
        {
          this.empresa = this.usuario.empresa;
        }
      }, err => {
        localStorage.removeItem('bat');
        location.reload();
      });
  }

  getReporteClientes() {

    this.dataSourceClientes.data = [];

    this.reportesService.getReporteClientes(this.empresa.id != null ? this.empresa.id : 0)
      .subscribe(
        data => {
          this.clientes = data;
          this.dataSourceClientes.data = data;
          this.isLoading = false;
          this.showAlertNotFound = false;

          if (this.dataSourceClientes.data.length == 0) {
            this.btnExportExcel.nativeElement.disabled = true;
            this.btnExportPdf.nativeElement.disabled = true;
            this.showAlertNotFound = true;
          }
          else {
            this.btnExportExcel.nativeElement.disabled = false;
            this.btnExportPdf.nativeElement.disabled = false;
          }
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un error',
            html: getErrorsForm(err.error.errors),
            type: 'error'
          })
        }
      );
  }

  getEmpresas() {
    this.empresasService.getAllEmpresas()
      .subscribe(
        data => {
          this.empresas = data;
        },
        err => {
          swal({
            title: 'Ups! Ha ocurrido un problema',
            html: getErrorsForm(err.error.errors),
            type: 'error'
          });
        }
      );
  }

  getEmpresa() {
    // this.source.reset();
    this.empresasService.getEmpresa(this.empresa.id)
      .subscribe(data => {
        this.empresa = data;
      }, err => {
        swal({
          title: 'Ups! Ha ocurrido un error',
          text: 'No se ha podido cargar la empresa',
          type: 'error'
        });
      }
      );
  }

  exportExcel() {
    let clientesAux = [];

    this.dataSourceClientes.data.forEach((d: Cliente) => {
      clientesAux.push({'rut': d.rut, 'razon_social': d.razon_social, 'nombre_contacto': d.nombre_contacto, 'direccion': d.direccion, 'telefono': d.telefono, 'email': d.email});
    });

    this.excelService.exportAsExcelFile(clientesAux, 'Informe Clientes');
  }

  fillArrayWithCliente(e) {
    this.clientesAux = [];

    e.selected.forEach(c => {
      this.clientesAux.push(c);
    });
  }

  searchClientesByEmpresa() {

    this.dataSourceClientes.data = [];
    this.isLoading = true;

    this.clientesService.getClientesByEmpresa(this.empresa.id)
      .subscribe(data => {
        this.isLoading = false;
        this.clientes = data;
        this.dataSourceClientes.data = data;

        if (this.dataSourceClientes.data.length == 0) {
          this.btnExportExcel.nativeElement.disabled = true;
          this.btnExportPdf.nativeElement.disabled = true;
        }
        else {
          this.btnExportExcel.nativeElement.disabled = false;
          this.btnExportPdf.nativeElement.disabled = false;
        }
      },
        err => {
          this.isLoading = false;
          swal({
            title: 'Ups! Ha ocurrido un error.',
            html: getErrorsForm(err.error.errors),
            type: 'warning'
          });
        }
      );
  }

  descargarPdf() {

    let idEmpresa = this.empresa.id != null ? this.empresa.id : 0;

    this.reportesService.makeReporteClientes(this.dataSourceClientes.data, idEmpresa).subscribe(data => {
      let blob = new Blob([data], { type: 'application/pdf' });
      saveAs(blob, 'reporte-clientes.pdf');
    },
    error => {
      swal({
        title: 'Ups! Ha ocurrido un error.',
        html: 'No se ha podido generar el documento. Intentelo más tarde.',
        type: 'error'
      });
    });
  }
}
