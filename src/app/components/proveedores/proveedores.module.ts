import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { ProveedoresComponent } from "./proveedores.component";
import { ProveedoresService } from "../../services/proveedores.service";
import { SharedModule } from "../../shared/shared.module";
import { EmpresaService } from "../../services/empresas.service";
import { ProveedoresRoutingModule } from "./proveedores-routing.module";
import { MatProgressSpinnerModule, MatPaginatorModule, MatTableModule, MatPaginatorIntl } from "@angular/material";
import { getSpanishPaginatorIntl } from "../../shared/spanish-datatable-intl";

@NgModule({
    imports: [ 
        CommonModule, 
        ReactiveFormsModule, 
        SharedModule,
        ProveedoresRoutingModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatPaginatorModule
    ],
    declarations: [ 
        ProveedoresComponent 
    ],
    providers: [ 
        ProveedoresService, 
        EmpresaService,
        { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() } 
    ]
})
export class ProveedorModule { }