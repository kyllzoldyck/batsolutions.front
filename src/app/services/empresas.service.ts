import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Empresa } from '../models/empresa';
import { environment } from '../../environments/environment';
import { getHeaders, extractData, handleError } from '../shared/utils';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class EmpresaService {

  public empresa: Empresa;
  private API_URI = environment.URI;

  constructor(public http: HttpClient) {}

  getAllEmpresas(): Observable<Empresa[]> {
    let URL = this.API_URI + 'entities';
    return this.http.get<Empresa[]>(URL, { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }

  getEmpresa(empresa: number): Observable<any> {
    let URL = this.API_URI + 'entities/' + empresa;
    return this.http.get(URL, { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }
}
