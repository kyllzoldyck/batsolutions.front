import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Impuesto } from '../models/impuesto';
import { getHeaders, extractData, handleError } from "../shared/utils";

@Injectable()
export class ImpuestosService {

  private API_URI = environment.URI;

  constructor(private http: HttpClient) {}

  public getImpuestoByNombre(nombre: string): Observable<any> {
    let URL = this.API_URI + 'impuestos/impuesto/' + nombre;
    return this.http.get(URL, { headers: getHeaders() });
      // .map(extractData)
      // .catch(handleError);
  }
}
