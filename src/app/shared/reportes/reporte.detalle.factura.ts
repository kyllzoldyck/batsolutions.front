import { Component, Input } from '@angular/core';
import { Factura } from '../../models/factura';

@Component({
  selector: 'app-template-detalle-factura',
  templateUrl: './reporte.detalle.factura.html',
  styleUrls: ['./reporte.detalle.factura.css']
})
export class ReporteDetalleFacturaPDFComponent {

  @Input('factura') factura: Factura;

  public date: number;
  public showDetalle = true;

  constructor() {
    this.date = Date.now();
  }

}
