import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Cliente } from '../models/cliente';
import { handleError, extractData, getHeaders } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class ClientesService {

    private API_URI = environment.URI;

    constructor(private http: HttpClient, public router: Router) {}

    /**
     * Funcion encargada de obtener todos los clientes desde el Servidor.
     */
    getAllClientes(): Observable<Cliente[]> {
        let URL = this.API_URI + 'clientes';
        return this.http
          .get(URL, { headers: getHeaders() })
        //   .map(extractData)
          .catch(handleError);
    }

    /**
     * Funcion encargada de obtener un cliente desde el servidor.
     */
    getClienteById(id: number): Observable<any> {
        let URL = this.API_URI + 'clientes/' + id;
        return this.http.get(URL, { headers: getHeaders() })
        .map(extractData)
        .catch(handleError);
    }

    /**
     * Agrega un cliente enviandolo al Servidor.
     * @param cliente
     */
    addCliente(cliente: Cliente): Observable<any> {
        let URL = this.API_URI + 'clientes';
        let body = {
            rut: cliente.rut,
            razon_social: cliente.razon_social,
            nombre_contacto: cliente.nombre_contacto,
            direccion: cliente.direccion,
            telefono: cliente.telefono,
            email: cliente.email,
            id_empresa: cliente.id_empresa,
            giro: cliente.giro
        }

        return this.http.post(URL, JSON.stringify(body), { headers: getHeaders() });
        // .map(extractData)
        // .catch(handleError);
    }

    /**
     * Actualiza un cliente enviandolo al servidor.
     * @param cliente
     */
    updateCliente(cliente: Cliente): Observable<any> {
        let URL = this.API_URI + 'clientes/' + cliente.id;

        let body = {
            razon_social: cliente.razon_social,
            nombre_contacto: cliente.nombre_contacto,
            direccion: cliente.direccion,
            telefono: cliente.telefono,
            email: cliente.email,
            id_empresa: cliente.id_empresa,
            rut: cliente.rut,
            giro: cliente.giro
        }

        return this.http.put(URL, JSON.stringify(body), { headers: getHeaders() });
        // .map(extractData)
        // .catch(handleError);
    }

    /**
     * Elimina un cliente dado su Id.
     * @param id Cliente
     */
    deleteCliente(id: Number): Observable<any> {
        let URL = this.API_URI + 'clientes/' + id;

        return this.http.delete(URL, { headers: getHeaders() });
        // .map(extractData)
        // .catch(handleError);
    }

    /**
     * Obtiene clientes según el ID de la empresa
     * @param empresa ID de la empresa
     */
    getClientesByEmpresa(empresa: number): Observable<any> {
      let URL = this.API_URI + 'clientes/empresas/' + empresa;
      return this.http.get(URL, { headers: getHeaders() });
    }

    getClienteByRut(rut: string): Observable<any> {
      let URL = this.API_URI + 'clientes/rut/' + rut;
      return this.http.get(URL, { headers: getHeaders() });
    }
}
